const Nightwatch = require('nightwatch');

(async function() {
    const runner = Nightwatch.CliRunner({
        config: './config/local/config.conf.js',
        env: 'chrome'
    });

    // here you can overwrite any nightwatch config setting
    const settings = {
        webdriver: {
            start_process: true,
            server_path: '/usr/bin/chromedriver',
            port: 9515
        }
    };
    runner.setup(settings);

    await runner.startWebDriver();

    try {
        await runner.runTests();
    } catch (err) {
        console.error('An error occurred:', err);
    }

    await runner.stopWebDriver();
}());

// const Nightwatch = require('nightwatch');
// // read the CLI arguments
//
// Nightwatch.cli(async function(argv) {
//     const settings = {
//         webdriver: {
//             start_process: true,
//             server_path: '/usr/bin/chromedriver',
//             port: 9515,
//             tag: 'login'
//         }
//     };
//     argv._source = argv['_'].slice(0);
//
//     const runner = await Nightwatch.CliRunner(argv);
//     await runner.setup(settings).startWebDriver();
//
//     try {
//         await runner.runTests();
//     } catch (err) {
//         console.error('An error occurred:', err);
//     }
//
//     await runner.stopWebDriver();
// });
