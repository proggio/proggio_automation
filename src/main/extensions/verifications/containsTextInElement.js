/**
 * Checks if the given element contains the specified text.
 *
 * ```
 *   this.demoTest = function (browser) {
 *     browser.assert.containsTextInElement('#main', 'The Night Watch');
 *   };
 * ```
 *
 * @method containsTextInElement
 * @param {string|object} element The selector (CSS/Xpath) used to locate the element. Can either be a string or an object which specifies [element properties](https://nightwatchjs.org/guide#element-properties).
 * @param {string} expectedText The text to look for.
 * @param {string} [msg] Optional log message to display in the output. If missing, one is displayed by default.
 // * @api verifications
 // */
exports.assertion = function(element, expectedText, msg) {
    this.options = {
        elementSelector: true
    };

    this.formatMessage = function() {
        const message = msg || `Testing if element %s ${this.negate ? 'does not contain text %s' : 'contains text %s'}`;

        return {
            message,
            args: [this.elementSelector, `'${expectedText}'`]
        };
    };

    this.expected = function() {
        return this.negate ? `does not contain text '${expectedText}'` : `contains text '${expectedText}'`;
    };

    this.evaluate = function(value) {
        if (typeof value !== 'string') {
            return false;
        }

        return value.includes(expectedText);
    };

    this.failure = function(result) {
        return result === false || result && result.status === -1;
    };

    this.value = function(result) {
        return result.value;
    };

    this.actual = function(passed) {
        return passed ? `contains '${expectedText}'` : `does not contain '${expectedText}'`;
    };

    this.command = function(callback) {
        const browser = this.api;
        browser.waitForElementVisible(element);
        browser.getText(element, callback);
    };
};
