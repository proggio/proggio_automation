module.exports = class switchToWindowByIndex {
    command(windowNum) {
        let browser = this.api;
        browser.windowHandles(function(result) {
            let handle = result.value[windowNum];
            browser.switchWindow(handle);
        });
    }
};
