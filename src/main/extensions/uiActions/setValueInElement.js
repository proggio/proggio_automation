module.exports = class setValueInElement {
    command(elem, text) {
        let browser = this.api;
        browser.waitForElementVisible(elem);
        browser.setValue(elem, text);
    }
};
