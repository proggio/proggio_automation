module.exports = class clickOnElement {
    async command(elem) {
        let browser = this.api;
        await browser.waitForElementVisible(elem);
        const result = await browser.click(elem);
        await console.log(`${elem}`, result.status === 0 ? 'clicked successfully.' : `clicked with no success - ${result.status}`);
    }
};
