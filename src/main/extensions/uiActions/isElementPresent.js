module.exports = class areElementsPresent {
    async command(using, selector, callback) {
        let browser = this.api;
        return browser.element(using, selector, results => {
            const isTrue = results.status === 0;
            if (isTrue) {
                console.log(`Element ${selector} is present.`);
                callback();
            } else {
                console.log(`Element ${selector} is not present.`);
            }
        });
    }
};
