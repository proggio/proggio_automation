module.exports = class areElementsPresent {
    async command(using, selector, callback) {
        let browser = this.api;
        return browser.elements(using, selector, results => {
            const isTrue = results.value.length > 0;
            console.log(results.value.length);
            if (isTrue) {
                console.log(`Element ${selector} is present.`);
                callback();
            } else {
                console.log(`Element ${selector} is not present.`);
            }
        });
    }
};
