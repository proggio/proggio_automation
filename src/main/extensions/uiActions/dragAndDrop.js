module.exports = class dragAndDrop {
    async command(using, dragElem, dropElem) {
        let browser = this.api;
        await browser.waitForElementVisible(dragElem);
        const draggable = await browser.moveToElement(using, dragElem, 1, 1);
        await browser.mouseButtonDown(0);
        const droppable = await browser.moveToElement(using, dropElem, 1, 1);
        await browser.mouseButtonUp(0);
        console.log(`${dragElem}`, draggable.status === 0 && droppable.status === 0 ? 'dragged and dropped successfully.' : 'with no success. error');
    }
};
