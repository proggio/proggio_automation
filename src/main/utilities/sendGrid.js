require('dotenv').config();
const { dateForAttachment } = require('./customDate');
const sgMail = require('@sendgrid/mail');
const apiKey = `${process.env.SENDGRID_API_KEY}`;
sgMail.setApiKey(apiKey);

const fs = require('fs');
let pathToAttachment, attachment;

function sendEmail(to, from, subject, text) {
    pathToAttachment = `${__dirname}/../../../nightwatch-reports/Tests Report ${dateForAttachment}.html`;
    attachment = fs.readFileSync(pathToAttachment).toString('base64');
    const msg = {
        to: `${to}`,
        from: `${from}`,
        subject: `${subject}`,
        text: `${text}`,
        attachments: [{
            content: attachment,
            filename: `Tests Report ${dateForAttachment}.html`,
            type: 'application/html',
            disposition: 'attachment'
        }]
    };
    sgMail
        .send(msg)
        .then(() => {
            console.log(`Email sent to ${to}`);
        })
        .catch((error) => {
            console.error(error);
        });
}

module.exports = { sendEmail };
