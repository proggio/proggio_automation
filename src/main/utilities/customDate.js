const moment = require('moment');
const randomNumber = moment().format('YYYYDDhhmmssSS');
const excelDateFormat = moment().format('MMMM DD YYYY');
const browserStackDate = moment().format('MM/DD/YYYY hh:mm:ss a');
const dateForAttachment = moment().format('MM_DD_YYYY_hh_mm');
const gmailDate = moment().format('MM/DD/YYYY hh:mm a');

module.exports = { gmailDate, dateForAttachment, excelDateFormat, browserStackDate, randomNumber };
