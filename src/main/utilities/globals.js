const axios = require('axios');
require('dotenv').config();
const { sendEmail } = require('./sendGrid');
const { createHtmlReporter } = require('./html-reporter');
const { gmailDate, randomNumber } = require('./customDate');
module.exports = {

    // Credentials
    proggio_username: 'vladi.b@progg.io',
    proggio_password: 'vladibProggio',
    dynamic_userName: `vladi.b+${randomNumber}@progg.io`,
    generic_password: 'qazQAZ123!@#',
    proggioGmail_password: '130819933zz',

    // Users
    list_view_account: `vladi.b+list_View@progg.io`,
    tiles_view_account: `vladi.b+tiles_View@progg.io`,
    kanban_view_account: `vladi.b+kanban_View@progg.io`,
    automation_testing_account: 'vladi.b+Automation_testing@progg.io',
    myUserProfile: 'vladi.b+My_User_Profile@progg.io',

    // Jira Domain
    jiraDomainLink: 'https://proggio.atlassian.net/',
    jira_account: `vladi.b+jira@progg.io`,

    // Permissions
    // Add User
    account_add_user: 'vladi.b+add_user@progg.io',
    account_permissions: 'vladi.b+permissions@progg.io',

    // Admin
    account_admin_project_manager: 'vladi.b+admin_project_manager@progg.io',
    account_admin_executive_manager: 'vladi.b+admin_executive_manager@progg.io',

    // Editor
    account_editor_guest: 'vladi.b+editor_guest@progg.io',
    account_editor_task_owner: 'vladi.b+editor_task_owner@progg.io',
    account_editor_project_manager: 'vladi.b+editor_project_manager@progg.io',
    account_editor_executive_manager: 'vladi.b+editor_executive_manager@progg.io',

    // Viewer
    account_viewer_guest: 'vladi.b+viewer_guest@progg.io',
    account_viewer_task_owner: 'vladi.b+viewer_task_owner@progg.io',
    account_viewer_project_manager: 'vladi.b+viewer_project_manager@progg.io',
    account_viewer_executive_manager: 'vladi.b+viewer_executive_manager@progg.io',

    // Testing Account
    testing_account: 'vladi.b+1612436396346@progg.io',

    // URLS
    base_url: 'https://app-staging.proggio.com',
    gmailLogin_url: 'https://accounts.google.com/signin',
    gmailMailBox_url: 'https://mail.google.com/mail/u/0/#inbox',

    // Task Management - Status Column
    taskManagement: {
        backLog: 'Backlog',
        inReview: 'In review',
        inProcess: 'In process',
        completed: 'Completed',
        closed: 'Closed'
    },

    // All Projects - Views
    allProjects: {
        views: {
            tilesView: 'tiles',
            listView: 'list',
            kanbanView: 'kanban',
            boardView: 'board'
        }
    },

    lifeCycleStages: {
        created: 'Created',
        prioritization: 'Prioritization',
        planning: 'Planning',
        approval: 'Approval',
        execution: 'Execution',
        done: 'Done',
        archived: 'Archived'
    },

    priorityOptions: {
        low: 'Low',
        medium: 'Medium',
        high: 'High',
        critical: 'Critical'
    },

    authentication: {
        none: 'None',
        email: 'Email',
        authenticator: 'Authenticator'
    },

    textSize: {
        auto: 'Auto',
        small: 'Small',
        medium: 'Medium',
        large: 'Large'
    },

    textColor: {
        light: 'Light',
        dark: 'Dark'
    },

    periodicEmailReport: {
        never: 'Never',
        daily: 'Daily',
        weekly: 'Weekly',
        monthly: 'Monthly'
    },

    contextMenu: {
        timeline: 'Timeline_project',
        settings: 'Settings_project',
        details: 'id_Details_activity',
        taskList: 'list_project',
        taskBoard: 'board_project',
        budget: 'Budget_project',
        notes: 'notes_project',
        duplication: 'id_Duplication_project',
        export: 'id_Export_project',
        archive: 'id_Archive_project',
        unArchive: 'id_Unarchive_archivedProject',
        createTemplate: 'Template_project',
        delete: 'id_Delete_project',
        deleteTemplate: 'id_Delete_teamTemplate'
    },

    // Activity IDs
    mainActivity: 'map_activity_id_338911',
    TasksActivity: 'map_activity_id_338910',
    bufferActivity: 'map_activity_id_338913',
    startActivity: 'map_activity_id_338920',
    endActivity: 'map_activity_id_338919',

    // Hooks Timeout
    timeOut: 15000,

    globals: {
        abortOnAssertionFailure: true,
        abortOnElementLocateError: false,
        waitForConditionPollInterval: 500,
        waitForConditionTimeout: 20000,
        throwOnMultipleElementsReturned: false,
        suppressWarningsOnMultipleElementsReturned: false,
        asyncHookTimeout: 10000,
        customReporterCallbackTimeout: 20000,
        retryAssertionTimeout: 5000,

        reporter: function(results, done) {
            const vladiBulanov = 'vladi.b@progg.io';
            // const emails = ['vladi.b', 'Alice.Goland'];
            const emails = ['vladi.b'];
            let flag = false;
            if (flag) {
                axios.get(`https://${process.env.BROWSERSTACK_USERNAME}:${process.env.BROWSERSTACK_ACCESS_KEY}@api.browserstack.com/automate/builds.json`)
                    .then(response => {
                        const buildId = response.data[0].automation_build.hashed_id;
                        const buildName = response.data[0].automation_build.name.slice(0, -23);
                        const browserUrl = `https://automate.browserstack.com/dashboard/v2/builds//${buildId}`;
                        const customDone = () => {
                            for (let i = 0; i < emails.length; i++) {
                                let currentEmail = `${emails[i]}@progg.io`;
                                sendEmail(
                                    `${currentEmail}`,
                                    `${vladiBulanov}`,
                                    `${buildName} ${gmailDate}`,
                                    `\nBrowserStack Build:\n ${browserUrl}`
                                );
                            }
                            done();
                        };
                        createHtmlReporter(results, customDone);
                    })
                    .catch(error => {
                        console.log(error);
                    });
            } else {
                done();
            }
        },

        // // External before hook is ran at the beginning of the tests run, before creating the Selenium session
        // before: function(done) {
        //     console.log("Setting up...");
        //     done();
        // },
        //
        // // External after hook is ran at the very end of the tests run, after closing the Selenium session
        // after: function(done) {
        //     console.log("Closing down...");
        //     done();
        // },

        // This will be run before each test suite is started
        beforeEach(browser, done) {
            console.log('Setting up...');
            browser.maximizeWindow();
            browser.deleteCookies();
            console.log('Navigating to: ' + this.base_url);
            browser.url(this.base_url);
            browser.clickOnElement('#hs-eu-confirmation-button');
            // browser.clickOnElement('div[data-auto = "button"]');
            done();
        },

        // This will be run after each test suite is finished
        afterEach(browser, done) {
            // browser.logout();
            console.log('Closing down...');
            browser.end();
            done();
        }
    }
};
