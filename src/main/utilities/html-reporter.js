const path = require('path');
const { dateForAttachment } = require('./customDate');
const HtmlReporter = require('nightwatch-html-reporter');
const createHtmlReporter = new HtmlReporter({
    openBrowser: false,
    reportsDirectory: path.join(__dirname, '/../../../nightwatch-reports'),
    themeName: 'custom-theme',
    reportFilename: `Tests Report ${dateForAttachment}.html`
});

module.exports = { createHtmlReporter: createHtmlReporter.fn };
