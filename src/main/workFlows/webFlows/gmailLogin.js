const globalConfig = require('../../utilities/globals');
module.exports = class gmailLogin {
    command(username, password) {
        console.log('Logging in to Gmail...');
        let browser = this.api;
        let gmailLoginPage = browser.page.gmailLoginPage();
        browser.url(globalConfig.gmailLogin_url);
        gmailLoginPage.setValueInElement('@emailNameInput', username);
        gmailLoginPage.clickOnElement('@emailContinueButton');
        gmailLoginPage.setValueInElement('@passwordInput', password);
        gmailLoginPage.clickOnElement('@passwordContinueButton');
        browser.useXpath();
        browser.waitForElementVisible('//h1[contains(text(), "Vladislav Bulanov")]');
    }
};
