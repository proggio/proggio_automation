module.exports = class logout {
    command() {
        let browser = this.api;
        let userProfilePage = browser.page.userProfilePage();
        browser.pause(1000);
        userProfilePage.clickOnElement('@userProfileButton');
        userProfilePage.clickOnElement('@signOutButton');
        console.log('Signing out from Proggio...');
    }
};
