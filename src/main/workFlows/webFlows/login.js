module.exports = class login {
    command(username, password) {
        console.log('Signing in to Proggio...');
        let browser = this.api;
        let proggioLoginPage = browser.page.proggioLoginPage();
        proggioLoginPage.setValueInElement('@emailInput', username);
        proggioLoginPage.setValueInElement('@passwordInput', password);
        proggioLoginPage.clickOnElement('@loginButton');
    }
};
