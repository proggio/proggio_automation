module.exports = {
    elements: {
        emailNameInput: {
            selector: 'input[id = "identifierId"]'
        },
        emailContinueButton: {
            selector: 'div[id= "identifierNext"]'
        },
        passwordInput: {
            selector: 'input[type= "password"]'
        },
        passwordContinueButton: {
            selector: 'div[id= "passwordNext"]'
        }
    }
};
