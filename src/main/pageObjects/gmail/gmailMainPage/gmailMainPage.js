const globalConfig = require('../../../utilities/globals');
let browser, elements;

module.exports = {
    elements: {
        composeEmailButton: {
            selector: 'div[id = ":io"] div[role = "button"]'
        },
        confirmEmailMessage: {
            selector: '//tbody//td[@role = "gridcell"][2]//span[contains (text(), "Proggio: Please confirm your email")]',
            locateStrategy: 'xpath'
        },
        confirmEmailButton: {
            selector: '//a[contains (text(), "Confirm your email address")]',
            locateStrategy: 'xpath'
        },
        confirmResetPasswordMessage: {
            selector: '//tbody//td[@role = "gridcell"][2]//span[contains (text(), "Proggio: Reset password")]',
            locateStrategy: 'xpath'
        },
        confirmResetPasswordButton: {
            selector: '//a[contains (text(), "Reset password")]',
            locateStrategy: 'xpath'
        },
        deleteEmailMessageButton: {
            selector: '//tbody//td[@role = "gridcell"][2]//span[contains (text(), "Proggio: Please confirm your email")]/../../../../../..//li[@data-tooltip = "Delete"]',
            locateStrategy: 'xpath'
        },
        deletePasswordMessageButton: {
            selector: '//tbody//td[@role = "gridcell"][2]//span[contains (text(), "Proggio: Reset password")]/../../../../../..//li[@data-tooltip = "Delete"]',
            locateStrategy: 'xpath'
        },
        inboxButton: {
            selector: '//div[@data-tooltip = "Inbox"]',
            locateStrategy: 'xpath'
        },

        // Gmail popUp - Enable desktop notifications
        closeGmailPopup: {
            selector: 'div[class = "vh"] div[role = "button"]'
        }

    },

    commands: [{

        deleteGmailMsg(moveToElement, clickElement) {
            browser = this.api;
            elements = this.elements;
            browser.clickOnElement(elements.inboxButton);
            browser.moveToElement('xpath', moveToElement, 1, 1, function() {
                browser.clickOnElement(clickElement);
                browser.pause(5000);
                browser.expect.element(moveToElement).to.not.be.present.before(globalConfig.timeOut);
            });
        },

        confirmPasswordResetMsg() {
            browser = this.api;
            elements = this.elements;
            browser.clickOnElement(elements.confirmResetPasswordMessage);
            browser.clickOnElement(elements.confirmResetPasswordButton);
        },

        confirmSignUpMsg() {
            browser = this.api;
            elements = this.elements;
            browser.clickOnElement(elements.confirmEmailMessage);
            browser.clickOnElement(elements.confirmEmailButton);
        },

        closePopup() {
            browser = this.api;
            elements = this.elements;
            browser.clickOnElement(elements.closeGmailPopup);
        }

    }]
};
