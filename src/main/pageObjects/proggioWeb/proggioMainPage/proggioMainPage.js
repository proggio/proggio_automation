let browser, elements;
module.exports = {
    elements: {
        alertDark: {
            selector: 'div[class*="alert dark"]'
        },
        alertDarkCloseButton: {
            selector: 'div[class*="alert dark"] div[class = "close-icon"]'
        },
        undoButton: {
            selector: 'span[id = "action-alert-link"]'
        },
        spinner: {
            selector: '//*[name() = "svg" and @class = "spinner"]',
            locateStrategy: 'xpath'
        },
        activitySpinner: {
            selector: '//*[name() = "circle" and @class = "loading-path"]',
            locateStrategy: 'xpath'
        },
        loadingText: {
            selector: 'div[class = "messages"] div[class = "message"]'
        },

        // Side Menu Options
        allProjects: {
            selector: 'td[id = "link-section-all-projects"] i'
        },
        sideMenu: {
            selector: 'div[id = "side-menu-container"] div[id = "style-1"]'
        },
        portfolio: {
            selector: 'td[id = "link-section-project-portfolio"]'
        },
        dashboard: {
            selector: 'td[id = "link-section-dashboard"]'
        },
        budget: {
            selector: 'td[id = "link-section-budget-management"]'
        },
        taskManagement: {
            selector: 'td[id = "link-section-task-management"]'
        },
        resourcesManagement: {
            selector: 'td[id = "link-section-team-loading"]'
        },
        scheduleDemo: {
            selector: 'td[id = "link-section-schedule-demo"]'
        },
        settings: {
            selector: 'td[id = "link-section-team-management"]'
        },
        contactSalesNow: {
            selector: 'div[class = "bottom-button ellipsis"]'
        },
        recentProject: {
            selector: 'td[id = "recent-project-0"]:first-child'
        },
        closeFeedBack: {
            selector: 'div[data-auto = "button"]'
        },

        // Tiles View
        tilesView: {
            selector: 'div[class*= "projects-toolbar"] li[id*= "Tiles"]'
        },
        // Kanban View
        kanbanView: {
            selector: 'div[class*= "projects-toolbar"] li[id*= "Kanban"]'
        },
        // List View
        listView: {
            selector: 'div[class*= "projects-toolbar"] li[id*= "List"]'
        }
    },

    commands: [{

        allProjectsViewPicker(viewName) {
            browser = this.api;
            elements = this.elements;
            browser.clickOnElement(elements.allProjects);
            switch (viewName) {
                case 'tiles':
                    browser.clickOnElement(elements.tilesView);
                    break;
                case 'list':
                    browser.clickOnElement(elements.listView);
                    break;
                case 'kanban':
                    browser.clickOnElement(elements.kanbanView);
                    break;
            }
        }

    }]
};
