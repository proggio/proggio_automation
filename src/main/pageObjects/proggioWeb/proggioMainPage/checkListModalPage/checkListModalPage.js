let browser, elements;
module.exports = {
    elements: {
        addItemInput: {
            selector: 'input[class*= "dqtGjB"]'
        },
        saveItemButton: {
            selector: 'div[id = "check-list-ok"]'
        },
        cancelItemButton: {
            selector: 'div[id = "check-list-cancel"]'
        }
    },

    commands: [{

        async getCheckListText(toDoName) {
            browser = this.api;
            elements = this.elements;
            const toDoElement = `div[title = "${toDoName}"]`;
            browser.useCss();
            return await browser.getText(toDoElement);
        },

        addItemToList(itemName) {
            browser = this.api;
            elements = this.elements;
            browser.setValueInElement(elements.addItemInput, itemName);
            browser.clickOnElement(elements.saveItemButton);
        },

        async toggleCheckItem(itemName) {
            browser = this.api;
            elements = this.elements;
            browser.useXpath();
            const item = `//div[contains(text(), "${itemName}")]/../../div[contains(@class, "check-div")]`;
            const itemChecked = `//div[contains(text(), "${itemName}")]/../../div[contains(@class, "check-div")]/i`;
            const result = await browser.getAttribute('xpath', itemChecked, 'class');
            const value = result.value;
            if (value.contains('checked')) {
                browser.clickOnElement(item);
                console.log(`${itemName} item unchecked`);
            } else {
                browser.clickOnElement(item);
                console.log(`${itemName} item checked`);
            }
        },

        deleteItemFromList(itemName) {
            browser = this.api;
            elements = this.elements;
            const item = `//div[contains(text(), "${itemName}")]/../..//i[@class = "fal fa-trash-alt"]`;
            browser.useXpath();
            browser.moveToElement('xpath', item, 1, 1);
            browser.mouseButtonClick(0);
        }
    }]
};
