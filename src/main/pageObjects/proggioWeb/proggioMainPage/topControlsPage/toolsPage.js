let browser, elements;
module.exports = {
    elements: {

        // Tools
        listViewToolsMenuButton: {
            selector: 'span[id = "tools_projects_submenu"]'
        },
        projectMapToolsMenuButton: {
            selector: 'span[id = "tools_map_submenu"]'
        },
        // Menu
        newProjectButton: {
            selector: 'li[id = "id_NEW_PROJECT_projects_mode"]'
        },
        importNewProjectButton: {
            selector: 'li[id = "id_Import new Project_projects_mode"]'
        },
        listViewExportProjectButton: {
            selector: 'li[id = "id_EXPORT_EXCEL_projects_mode"]'
        },
        projectMapExportProjectButton: {
            selector: 'li[id = "id_EXPORT_EXCEL_map"]'
        },
        inviteUserButton: {
            selector: 'li[id = "id_INVITE_USER_projects_mode"]'
        },

        viewMenuButton: {
            selector: 'span[id = "view_projects_submenu"]'
        },
        helpMenuButton: {
            selector: 'span[id = "help_submenu_projects_mode"]'
        }
    },

    commands: [{
        exportProjectToExcel(inAppLocation) {
            browser = this.api;
            elements = this.elements;
            switch (inAppLocation) {
                case 'all projects list view':
                    browser.clickOnElement(elements.listViewToolsMenuButton);
                    browser.clickOnElement(elements.listViewExportProjectButton);
                    break;
                case 'project map':
                    browser.clickOnElement(elements.projectMapToolsMenuButton);
                    browser.clickOnElement(elements.projectMapExportProjectButton);
                    break;
            }
        }
    }]
};
