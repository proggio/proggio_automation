let browser, elements;
module.exports = {
    elements: {
        addFileButton: {
            selector: 'div[class = "button positive"]'
        },
        addExternalLinkButton: {
            selector: 'div[id = "Add external link_button"]'
        },
        saveExternalLinkButton: {
            selector: 'div[id = "Save external link_button"]'
        },
        externalLinkInput: {
            selector: 'input[id = "linkfile"]'
        },
        doneButton: {
            selector: 'div[id = "Done_button"]'
        },
        confirmDelete: {
            selector: 'div[id = "popoverOkButton"]'
        },
        emptyAttachments: {
            selector: 'div[class = "empty-attachments"]'
        },
        lastFileUploaded: {
            selector: 'div[class = "files-container"] div[class = "file-container"]:last-child'
        }
    },

    commands: [{

        verifyFileVisible(fileName) {
            browser = this.api;
            elements = this.elements;
            browser.useXpath();
            const file = `//span[contains(text(), "${fileName}")]`;
            browser.assert.visible(file);
        },

        async getUploadedFileName(fileName) {
            browser = this.api;
            elements = this.elements;
            browser.useXpath();
            const fileElement = `//span[contains(text(), "${fileName}")]`;
            return await browser.getText(fileElement);
        },

        addExternalLink(link) {
            browser = this.api;
            elements = this.elements;
            browser.useCss();
            browser.clickOnElement(elements.addFileButton);
            browser.clickOnElement(elements.addExternalLinkButton);
            browser.setValueInElement(elements.externalLinkInput, link);
            browser.clickOnElement(elements.saveExternalLinkButton);
        },

        removeExternalLink(fileName) {
            browser = this.api;
            elements = this.elements;
            const fileDeleteIcon = `//span[contains(text(), "${fileName}")]/..//i[contains(@class, "delete-icon")]`;
            browser.clickOnElement(fileDeleteIcon);
            browser.clickOnElement(elements.confirmDelete);
        },

        closeModal() {
            browser = this.api;
            elements = this.elements;
            browser.useCss();
            browser.clickOnElement(elements.closeChatBoxButton);
        }
    }]
};
