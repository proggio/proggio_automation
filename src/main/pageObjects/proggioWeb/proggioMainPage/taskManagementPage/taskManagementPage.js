let browser, elements;
module.exports = {
    elements: {
        /** List View **/
        listView: {
            selector: 'li[id = "option-fa-list|List"]'
        },
        listViewSelected: {
            selector: 'li[id = "option-fa-list|List"][class*= "selected"]'
        },
        createNewTaskButton: {
            selector: 'div[id = "add-task-button"]'
        },
        myTasksTab: {
            selector: 'div[id = "tab_My Tasks"]'
        },
        allTasksTab: {
            selector: 'div[id = "tab_All Tasks"]'
        },
        toolsOption: {
            selector: 'span[id = "tools_user_submenu"]'
        },
        viewOption: {
            selector: 'span[id = "view_user_submenu"]'
        },

        /** Board View **/
        boardView: {
            selector: 'li[id = "option-fa-sliders-v|Board"]'
        },
        boardViewSelected: {
            selector: 'li[id = "option-fa-sliders-v|Board"][class*= "selected"]'
        },

        // Cell
        cellName: {
            selector: `div[class*= "Table__Grid"] div[class = "activity_name is_task"] div[class*= "label-div"]`
        },
        cellInput: {
            selector: 'div[class*= "Table__Grid"] div[class = "activity_name is_task"] input[class = "label-input"]'
        },
        cellActivityName: {
            selector: `div[class*= "Table__Grid"] div[class = "activity_name"] div[class = "label-div"]`
        },
        cellCheckBox: {
            selector: `div[class*= "Table__Grid"] div[class*= "Checkbox"]`
        },
        cellGoToProjectButton: {
            selector: 'div[class*= "Table__Grid"] i[id*= "external-link"]'
        },
        cellStatus: {
            selector: 'div[class = " css-1wvgej1-singleValue"]'
        },
        cellPriority: {
            selector: 'div[class = " css-1mvips1-singleValue"]'
        },
        cellStatusDropdownMain: {
            selector: 'div[class = " css-11unzgr"]'
        },

        /** Board View **/
        boardViewToggle: {
            selector: 'li[id = "option-fa-sliders-v|Board"]'
        },
        columnCell: {
            selector: 'div[class = "kanban__FlexContainer-rdmx6m-0 eifGEx"] div[id = "style-1"]'
        },
        rowItem: {
            selector: 'div[class = "activity-board-item-content"]:first-child'
        },

        backLogColumnList: {
            selector: `//div[contains(@title, "Backlog")]/../..//div[contains(@class, "List")]`,
            locateStrategy: 'xpath'
        },
        backLogColumnContainer: {
            selector: '//div[contains(@title, "Backlog")]/../..//div[contains(@class, "innerScrollContainer")]',
            locateStrategy: 'xpath'
        },
        inReviewColumnList: {
            selector: `//div[contains(@title, "In review")]/../..//div[contains(@class, "List")]`,
            locateStrategy: 'xpath'
        },
        inReviewColumnContainer: {
            selector: '//div[contains(@title, "In review")]/../..//div[contains(@class, "innerScrollContainer")]',
            locateStrategy: 'xpath'
        },

        // Top Menu
        exportProjectExcel: {
            selector: 'li[id = "id_EXPORT_EXCEL_user"]'
        }
    },

    commands: [{

        async getColumnsNameTableList() {
            browser = this.api;
            elements = this.elements;
            let columnsTableList = [];
            const tableList = 'div[class = "ReactVirtualized__Table__headerRow"] div[aria-label]';
            browser.useCss();
            browser.waitForElementVisible(tableList);
            await browser.elements('css selector', tableList, async(results) => {
                const list = results.value.length;
                for (let i = 0; i < list; i++) {
                    const column = `div[class = "ReactVirtualized__Table__headerRow"] div[aria-label]:nth-child(${i + 4})`;
                    if (i + 4 !== 5) {
                        const columnTableTitle = await browser.getAttribute('css selector', column, 'aria-label');
                        const columnTableTitleToUpper = columnTableTitle.value;
                        await columnsTableList.push(columnTableTitleToUpper.toUpperCase());
                    }
                }
            });
            return columnsTableList;
        },

        taskManagementViewPicker(viewName) {
            browser = this.api;
            elements = this.elements;
            switch (viewName) {
                case 'list':
                    browser.moveToElement('css selector', elements.listView, 1, 1);
                    break;
                case 'board':
                    browser.moveToElement('css selector', elements.boardView, 1, 1);
                    break;
            }
            browser.mouseButtonClick(1);
            browser.pause(1000);
        },

        taskManagementSubViewPicker(subViewName) {
            browser = this.api;
            elements = this.elements;
            switch (subViewName) {
                case 'my tasks':
                    browser.moveToElement('css selector', elements.myTasksTab, 1, 1);
                    break;
                case 'all tasks':
                    browser.moveToElement('css selector', elements.allTasksTab, 1, 1);
                    break;
            }
            browser.mouseButtonClick(1);
            browser.pause(1000);
        },

        openSettings(taskName) {
            browser = this.api;
            const cellSettings = `//div[contains(@class, "Table__Grid")]//div[contains(text(), "${taskName}")]/../../../../..//i[contains(@id, "new_task_list_icon_fal fa-cog")]`;
            browser.useXpath();
            browser.clickOnElement(cellSettings);
        },

        openStatus(taskName) {
            browser = this.api;
            const cellStatus = `//div[contains(@class, "Table__Grid")]//div[contains(text(), "${taskName}")]/../../../../..//div[@class = "ellipsis"]`;
            browser.useXpath();
            browser.moveToElement('xpath', cellStatus, 1, 1);
            browser.mouseButtonClick(0);
        },

        openPriority(taskName) {
            browser = this.api;
            const cellPriority = `//div[contains(@class, "Table__Grid")]//div[contains(text(), "${taskName}")]/../../../../..//div[@class = "ellipsis"]`;
            browser.useXpath();
            browser.moveToElement('xpath', cellPriority, 1, 1);
            browser.mouseButtonClick(0);
        },

        addTask(columnName) {
            browser = this.api;
            const plusIcon = `//div[contains(@title, "${columnName}")]/..//i[@class = "fa fa-plus"]`;
            browser.useXpath();
            browser.clickOnElement(plusIcon);
        },

        async dragAndDropItem(startingColumnName, dragTaskName, dropToColumnName) {
            browser = this.api;
            const rowItemDrag = `//div[contains(@title, "${startingColumnName}")]/../..//div[contains(@title, "${dragTaskName}")]`;
            const rowItemDrop = `//div[contains(@title, "${dropToColumnName}")]/../..//div[contains(@class, "List")]`;
            await browser.useXpath();
            await browser.waitForElementVisible(rowItemDrag);
            await browser.moveToElement('xpath', rowItemDrag, 1, 1);
            await browser.mouseButtonDown(0);
            await browser.moveToElement('xpath', rowItemDrag, 10, 10);
            await browser.moveToElement('xpath', rowItemDrop, 1, 1);
            await browser.mouseButtonUp(0);
            await browser.pause(1000);
        },

        verifyTaskPresent(cellName, expectedText) {
            browser = this.api;
            const myCell = `div[class*= "Table__Grid"] div[class = "activity_name is_task"] div[title*= "${cellName}"]`;
            browser.useCss();
            browser.expect.element(myCell).text.to.contain(expectedText);
        },

        exportProjectToExcel() {
            browser = this.api;
            elements = this.elements;
            browser.clickOnElement(elements.toolsOption);
            browser.clickOnElement(elements.exportProjectExcel);
        }
    }]
};
