let browser, elements;
module.exports = {
    elements: {
        // Settings - Side Menu
        // Jira
        jiraConnectButton: {
            selector: 'div[id = "jira_integration"]'
        },

        toolbarTitle: {
            selector: '//div[@id = "toolbar-title" and contains(text(), "SETTINGS")]',
            locateStrategy: 'xpath'
        },
        inviteNewUser: {
            selector: 'div[id = "inviteButton"]'
        },

        // Popup Confirm Deletion
        confirmRemoveUser: {
            selector: 'div[id = "popoverOkButton"]'
        },
        cancelRemoveUser: {
            selector: 'div[id = "popoverCancelButton"]'
        },

        // Access Level - Drop Down
        admin: {
            selector: 'div[id*= "option-0"]'
        },
        editor: {
            selector: 'div[id*= "option-1"]'
        },
        viewer: {
            selector: 'div[id*= "option-2"]'
        },

        // User Role - Drop Down
        executiveManager: {
            selector: 'div[id*= "option-0"]'
        },
        projectManager: {
            selector: 'div[id*= "option-1"]'
        },
        taskOwner: {
            selector: 'div[id*= "option-2"]'
        },
        guest: {
            selector: 'div[id*= "option-3"]'
        }

    },

    commands: [{
        removeUser(userName) {
            browser = this.api;
            elements = this.elements;
            const user = `//div[@title = "${userName}"]/../../../..//div[contains(@id, "removeUserButton")]`;
            browser.getLocationInView('xpath', user);
            browser.useXpath();
            browser.clickOnElement(user);
            browser.clickOnElement(elements.confirmRemoveUser);
            console.log(`Deleting user: "${userName}"`);
        },

        verifyUserAccessLevel(userName, expectedAccessLevel) {
            browser = this.api;
            const accessLevelElement = `//div[@title = "${userName}"]/../../../..//div[@aria-colindex = "3"]//div[@class = "ellipsis"]`;
            browser.useXpath();
            browser.waitForElementVisible(accessLevelElement);
            browser.expect.element(accessLevelElement).text.to.equal(expectedAccessLevel);
        },

        verifyUserRole(userName, expectedUserRole) {
            browser = this.api;
            const roleElement = `//div[@title = "${userName}"]/../../../..//div[@aria-colindex = "4"]//div[@class = "ellipsis"]`;
            browser.useXpath();
            browser.waitForElementVisible(roleElement);
            browser.expect.element(roleElement).text.to.equal(expectedUserRole);
        },

        changeAccessAndRole(myBrowser, userName, accessOption, roleOption) {
            browser = this.api;
            const accessDropdown = `//div[@title = "${userName}"]/../../../..//div[@aria-colindex = "3"]//div[@class = "ellipsis"]`;
            const roleDropdown = `//div[@title = "${userName}"]/../../../..//div[@aria-colindex = "4"]//div[@class = "ellipsis"]`;
            browser.useXpath();
            browser.clickOnElement(accessDropdown);
            myBrowser.clickOnElement(accessOption);
            browser.useXpath();
            browser.clickOnElement(roleDropdown);
            myBrowser.clickOnElement(roleOption);
        }

    }]
};
