let browser, elements;
module.exports = {
    elements: {
        emailInput: {
            selector: 'input[placeholder = "name@company.com"]'
        },

        // Access Levels
        admin: {
            selector: '//div[contains(@class, "modal__Card")]//div[contains(text(), "Admin")]/..',
            locateStrategy: 'xpath'
        },
        editor: {
            selector: '//div[contains(@class, "modal__Card")]//div[contains(text(), "Editor")]/..',
            locateStrategy: 'xpath'
        },
        viewer: {
            selector: '//div[contains(@class, "modal__Card")]//div[contains(text(), "Viewer")]/..',
            locateStrategy: 'xpath'
        },

        // User Roles
        executiveManager: {
            selector: '//div[contains(@class, "modal__Card")]//div[contains(text(), "Executive manager")]',
            locateStrategy: 'xpath'
        },
        projectManager: {
            selector: '//div[contains(@class, "modal__Card")]//div[contains(text(), "Project manager")]',
            locateStrategy: 'xpath'
        },
        taskOwner: {
            selector: '//div[contains(@class, "modal__Card")]//div[contains(text(), "Task owner")]',
            locateStrategy: 'xpath'
        },
        guest: {
            selector: '//div[contains(@class, "modal__Card")]//div[contains(text(), "Guest")]',
            locateStrategy: 'xpath'
        },

        withoutInvitationButton: {
            selector: 'div[id = "modal_button_no-title_Add user without sending an invitation"]'
        },
        sendInvitationButton: {
            selector: 'div[id = "Send Invitation_button"]'
        }
    },

    commands: [{
        inviteUser(myBrowser, email, accessLevel, userRole) {
            browser = this.api;
            elements = this.elements;
            browser.setValueInElement(elements.emailInput, email);
            myBrowser.clickOnElement(accessLevel);
            myBrowser.clickOnElement(userRole);
            browser.clickOnElement(elements.withoutInvitationButton);
        }

    }]
};
