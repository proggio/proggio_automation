let browser, elements;
module.exports = {
    elements: {
        domainLinkInput: {
            selector: 'input[id = "domain"]'
        },
        connectToJiraButton: {
            selector: 'div[id = "Connect JIRA_button"]'
        },

        // Jira Website
        allowButton: {
            selector: 'input[id = "approve"]'
        },
        denyButton: {
            selector: 'input[id = "deny"]'
        }
    },

    commands: [{
        connectToJira(domainLink) {
            browser = this.api;
            elements = this.elements;
            browser.setValueInElement(elements.domainLinkInput, domainLink);
            browser.clickOnElement(elements.connectToJiraButton);
            browser.clickOnElement(elements.allowButton);
        }
    }]
};
