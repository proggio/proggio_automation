module.exports = {
    elements: {
        newWidgetButton: {
            selector: 'div[id= "+ Add Widget_button"]'
        },
        selectDashboardDropdown: {
            selector: 'div[class = " css-1l6vcr0-control"]'
        }
    }
};
