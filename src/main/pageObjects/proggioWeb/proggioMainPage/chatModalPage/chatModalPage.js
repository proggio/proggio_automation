let browser, elements;
module.exports = {
    elements: {
        chatBoxInput: {
            selector: 'div[class = "message-editor"] input[type = "text"]'
        },
        closeChatBoxButton: {
            selector: 'i[class = "fa fa-times"]'
        },
        sendMessageButton: {
            selector: 'i[class*= "plane"]'
        },
        lastMessage: {
            selector: 'div[class = "inner"] div[class = "message-row"]:last-child div[class*= "message-content"]'
        },
        messageOptions: {
            selector: '//div[@class = "message-row"]//i[@class = "fa fa-chevron-down"]',
            locateStrategy: 'xpath'
        },
        deleteMessageButton: {
            selector: '//div[@class = "context-menu"]//div[contains(text(), "Delete message")]',
            locateStrategy: 'xpath'
        },
        replyMessageButton: {
            selector: '//div[@class = "context-menu"]//div[contains(text(), "Reply")]',
            locateStrategy: 'xpath'
        }
    },

    commands: [{
        zoomIntoProject() {
            browser = this.api;
            elements = this.elements;
            browser.clickOnElement(elements.closeChatBoxButton);
            browser.pause(2000);
        },

        async getLastMessageContent() {
            browser = this.api;
            elements = this.elements;
            browser.useCss();
            return await browser.getText(elements.lastMessage);
        },

        sendMessage(message) {
            browser = this.api;
            elements = this.elements;
            browser.useCss();
            browser.setValueInElement(elements.chatBoxInput, `${message}`);
            browser.clickOnElement(elements.sendMessageButton);
        },

        deleteMessage(text) {
            browser = this.api;
            elements = this.elements;
            browser.useXpath();
            const message = `//div[contains(text(), "${text}")]`;
            browser.moveToElement('xpath', message, 1, 1);
            browser.clickOnElement(elements.messageOptions);
            browser.clickOnElement(elements.deleteMessageButton);
        },

        closeChatBox() {
            browser = this.api;
            elements = this.elements;
            browser.useCss();
            browser.clickOnElement(elements.closeChatBoxButton);
        }
    }]
};
