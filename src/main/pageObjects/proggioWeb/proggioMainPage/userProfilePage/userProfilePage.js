let browser, elements;
const globalConfig = require('../../../../utilities/globals');
module.exports = {
    elements: {
        saveUserProfileSettingsButton: {
            selector: 'div[id*= "Profile_Save"]'
        },

        userProfileButton: {
            selector: 'div[id= "button-user-profile"] div'
        },
        profileEmailName: {
            selector: 'div[class = "profile-avatar"] div[class = "profile-email"]'
        },
        signOutButton: {
            selector: 'tr[id = "link-profile-sign-out"]'
        },
        notificationBell: {
            selector: 'div[class = "collaboration"] i[class = "far fa-bell"]'
        },

        myProfileModalOption: {
            selector: 'td[id = "user_menu_profile"]'
        },
        myProfileModalContainer: {
            selector: 'div[class = "modal__Card-sc-9strd2-3 kXQqfs"]'
        },

        // Two-Factor Authentication
        optionNone: {
            selector: 'li[id = "option-None"]'
        },
        optionEmail: {
            selector: 'li[id = "option-Email"]'
        },
        optionAuthenticator: {
            selector: 'li[id = "option-Authenticator"]'
        },

        // Text Size
        textSizeAuto: {
            selector: 'li[id = "option-Auto"]'
        },
        textSizeSmall: {
            selector: 'li[id = "option-Small"]'
        },
        textSizeMedium: {
            selector: 'li[id = "option-Medium"]'
        },
        textSizeLarge: {
            selector: 'li[id = "option-Large"]'
        },

        // Text Color
        textColorLight: {
            selector: 'li[id = "option-Light"]'
        },
        textColorDark: {
            selector: 'li[id = "option-Dark"]'
        },

        // Periodic Email Reporter
        reportDaily: {
            selector: 'li[id = "option-Daily"]'
        },
        reportWeekly: {
            selector: 'li[id = "option-Weekly"]'
        },
        reportMonthly: {
            selector: 'li[id = "option-Monthly"]'
        },
        reportNever: {
            selector: 'li[id = "option-Never"]'
        },

        // Tags
        tagContainer: {
            selector: 'div[id = "tags-input"]'
        },
        tagInput: {
            selector: 'div[id = "tags-input"] div[class = " css-17h5lee-control"] input'
        },

        // Group Tags
        groupTagContainer: {
            selector: 'div[id = "groups-input"]'
        },
        groupTagInput: {
            selector: 'div[id = "groups-input"] div[class = " css-17h5lee-control"] input'
        },

        // FullName
        fullNameInput: {
            selector: 'input[id = "fullname-input"]'
        },

        // Username
        userNameInput: {
            selector: 'input[id = "username-input"]'
        },

        // Color
        colorPickerButton: {
            selector: 'div[id*= "color"]'
        }

    },

    commands: [{

        saveUserProfile() {
            browser = this.api;
            elements = this.elements;
            browser.useXpath();
            browser.clickOnElement(elements.saveUserProfileSettingsButton);
        },

        openUserProfile() {
            browser = this.api;
            elements = this.elements;
            browser.useCss();
            browser.clickOnElement(elements.userProfileButton);
            browser.clickOnElement(elements.myProfileModalOption);
        },

        getAlertTextElement(authenticationOption) {
            browser = this.api;
            elements = this.elements;
            browser.useXpath();
            return `//div[@id = "alert"]//div[contains(text(), "${authenticationOption}")]`;
        },

        authenticationPicker(option) {
            browser = this.api;
            elements = this.elements;
            browser.useCss();
            switch (option) {
                case `${globalConfig.authentication.none}`:
                    browser.clickOnElement(elements.optionNone);
                    break;
                case `${globalConfig.authentication.email}`:
                    browser.clickOnElement(elements.optionEmail);
                    break;
                case `${globalConfig.authentication.authenticator}`:
                    browser.clickOnElement(elements.optionAuthenticator);
                    break;
            }
        },

        getTagElement(tagName) {
            browser = this.api;
            browser.useXpath();
            return `//div[contains(text(), "${tagName}")]/../div[@class = "css-xb97g8"]`;
        },

        addTag(tagName) {
            browser = this.api;
            elements = this.elements;
            browser.clickOnElement(elements.tagContainer);
            browser.setValueInElement(elements.tagInput, tagName);
            browser.sendKeys(
                'css selector',
                elements.tagInput,
                browser.Keys.ENTER);
        },

        removeTag(tagName) {
            browser = this.api;
            elements = this.elements;
            browser.useXpath();
            const deleteTagElement = `//div[@id = "tags-input"]//div[contains(text(), "${tagName}")]/../div[@class = "css-xb97g8"]`;
            browser.clickOnElement(deleteTagElement);
        },

        getGroupTagElement(tagName) {
            browser = this.api;
            browser.useXpath();
            return `//div[contains(text(), "${tagName}")]/../div[@class = "css-xb97g8"]`;
        },

        addGroupTag(groupTagName) {
            browser = this.api;
            elements = this.elements;
            browser.clickOnElement(elements.groupTagContainer);
            browser.setValueInElement(elements.groupTagInput, groupTagName);
            browser.sendKeys(
                'css selector',
                elements.groupTagInput,
                browser.Keys.ENTER);
        },

        removeGroupTag(groupTagName) {
            browser = this.api;
            elements = this.elements;
            browser.useXpath();
            const deleteTagElement = `//div[@id = "groups-input"]//div[contains(text(), "${groupTagName}")]/../div[@class = "css-xb97g8"]`;
            browser.clickOnElement(deleteTagElement);
        },

        textSizePicker(sizeOption) {
            browser = this.api;
            elements = this.elements;
            browser.useCss();
            switch (sizeOption) {
                case `${globalConfig.textSize.auto}`:
                    browser.clickOnElement(elements.textSizeAuto);
                    break;
                case `${globalConfig.textSize.small}`:
                    browser.clickOnElement(elements.textSizeSmall);
                    break;
                case `${globalConfig.textSize.medium}`:
                    browser.clickOnElement(elements.textSizeMedium);
                    break;
                case `${globalConfig.textSize.large}`:
                    browser.clickOnElement(elements.textSizeLarge);
                    break;
            }
        },

        textColorPicker(color) {
            browser = this.api;
            elements = this.elements;
            browser.useCss();
            switch (color) {
                case 'Light':
                    browser.clickOnElement(elements.textColorLight);
                    break;
                case 'Dark':
                    browser.clickOnElement(elements.textColorDark);
                    break;
            }
        },

        periodicEmailReportPicker(periodicOption) {
            browser = this.api;
            elements = this.elements;
            browser.useCss();
            switch (periodicOption) {
                case 'Daily':
                    browser.clickOnElement(elements.reportDaily);
                    break;
                case 'Weekly':
                    browser.clickOnElement(elements.reportWeekly);
                    break;
                case 'Monthly':
                    browser.clickOnElement(elements.reportMonthly);
                    break;
                case 'Never':
                    browser.clickOnElement(elements.reportNever);
                    break;
            }
        },

        getFullNameValue(value) {
            browser = this.api;
            elements = this.elements;
            browser.useCss();
            return `input[id = "fullname-input"][value = "${value}"]`;
        },

        changeFullName(fullName) {
            browser = this.api;
            elements = this.elements;
            browser.useCss();
            browser.sendKeys(
                'css selector',
                elements.fullNameInput,
                browser.Keys.CONTROL +
                'a' +
                browser.Keys.NULL +
                browser.Keys.DELETE);
            browser.setValueInElement(elements.fullNameInput, `${fullName}`);
        },

        getUserNameValue(value) {
            browser = this.api;
            elements = this.elements;
            browser.useCss();
            return `input[id = "username-input"][value = "${value}"]`;
        },

        changeUserName(username) {
            browser = this.api;
            elements = this.elements;
            browser.useCss();
            browser.sendKeys(
                'css selector',
                elements.userNameInput,
                browser.Keys.CONTROL +
                'a' +
                browser.Keys.NULL +
                browser.Keys.DELETE);
            browser.setValueInElement(elements.userNameInput, `${username}`);
        },

        getColorValueElement(color) {
            browser = this.api;
            return `div[id = "color-${color}"]`;
        },

        colorPicker(color) {
            browser = this.api;
            elements = this.elements;
            const colorId = `div[title = "${color}"]`;
            browser.useCss();
            browser.clickOnElement(elements.colorPickerButton);
            browser.clickOnElement(colorId);
        }

    }]

};
