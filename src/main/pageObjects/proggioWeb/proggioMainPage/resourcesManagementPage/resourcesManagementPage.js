module.exports = {
    elements: {
        toolbarTitle: {
            selector: '//div[@id = "toolbar-title" and contains(text(), "RESOURCES MANAGEMENT")]',
            locateStrategy: 'xpath'
        }
    }
};
