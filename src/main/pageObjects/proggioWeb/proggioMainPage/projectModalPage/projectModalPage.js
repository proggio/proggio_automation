let browser, elements;
const globalConfig = require('../../../../utilities/globals');
module.exports = {
    elements: {
        projectNameInput: {
            selector: 'input[id = "project_name"]'
        },
        closeProjectSettingsButton: {
            selector: '//div[contains(@class, "Tgncb")]/../i[@id = "closeModalButton"]',
            locateStrategy: 'xpath'
        },
        datePicker: {
            selector: 'div[id= "date_picker"] div[class = "DayPickerInput"]'
        },
        datePickerText: {
            selector: 'div[id= "date_picker"] div[class = "DayPickerInput"] div[class = "ellipsis"]'
        },
        addFileButton: {
            selector: 'div[class = "button positive"]'
        },
        descriptionText: {
            selector: 'div[id = "inline-edit-text-component"] div[class = "ql-editor ql-blank"]'
        },
        descriptionInput: {
            selector: '//input[@class = "label-input"]',
            locateStrategy: 'xpath'
        },
        confirmDescriptionButton: {
            selector: 'i[class = "fa fa-check"]'
        },
        lifeCycleDropDown: {
            selector: 'div[class = "modal__Card-sc-9strd2-3 kXQqfs"] div[class = "status-select"]'
        },
        activityAssignee: {
            selector: 'div[class = "flex vertical"] div[class*= "singleValue"]'
        },
        projectSettingsText: {
            selector: '//div[contains(text(), "Project Settings")]',
            locateStrategy: 'xpath'
        },

        projectTagContainer: {
            selector: 'div[class = " css-3rh5y4-container"] div[class = " css-q7i613-control"]'
        },
        projectTagInput: {
            selector: 'div[class = " css-17h5lee-control"] input'
        },
        moreProjectDataButton: {
            selector: 'div[id = "More Project data_button"]'
        },
        moreProjectDataModal: {
            selector: 'div[class = "modal__Card-sc-9strd2-3 kXQqfs"] '
        },
        assigneeContainerText: {
            selector: 'div[id= "assigneeList"] div[class*= "singleValue"]'
        }
    },

    commands: [{

        getProjectTitle() {
            browser = this.api;
            browser.useCss();
            return `input[id = "project_name"]`;
        },

        getProjectDescription(projectName) {
            browser = this.api;
            browser.useXpath();
            return `//div[@title = "${projectName}"]/../../..//div[@class = "label-div"]`;
        },

        addDescription(projectName, text, deleteDescription) {
            browser = this.api;
            elements = this.elements;
            const project = `//div[@title = "${projectName}"]/../../..//div[@class = "label-div"]`;
            browser.useXpath();
            browser.clickOnElement(project);
            browser.setValueInElement(elements.descriptionInput, text);
            if (deleteDescription === true) {
                browser.keys([browser.Keys.DELETE]);
                browser.keys([browser.Keys.ENTER]);
                browser.pause(500);
            } else {
                browser.keys([browser.Keys.ENTER]);
            }
        },

        closeProjectSettings() {
            browser = this.api;
            elements = this.elements;
            browser.useXpath();
            browser.clickOnElement(elements.closeProjectSettingsButton);
            browser.pause(500);
        },

        projectDatePicker(day) {
            browser = this.api;
            elements = this.elements;
            const dayNum = `//div[@class = "DayPicker-Month"]//div[contains(@aria-label, "${day}")]`;
            browser.pause(1000);
            browser.clickOnElement(elements.datePicker);
            browser.useXpath();
            browser.clickOnElement(dayNum);
        },

        changeProjectName(newProjectName) {
            browser = this.api;
            elements = this.elements;
            browser.clickOnElement(elements.projectNameInput);
            browser.sendKeys(
                'css selector',
                elements.projectNameInput,
                browser.Keys.CONTROL +
                'a' +
                browser.Keys.DELETE);
            browser.setValueInElement(elements.projectNameInput, newProjectName);
        },

        getLifeCycleElement(lifeCycleOption) {
            browser = this.api;
            browser.useXpath();
            return `//div[@class = "status-select"]//div[contains(text(), "${lifeCycleOption}")]`;
        },

        changeLifeCycleStage(lifeCycleOption) {
            browser = this.api;
            elements = this.elements;
            const lifeCycleElement = `//div[@class = "modal__Card-sc-9strd2-3 kXQqfs"]//div[@class = "status-select"]//div[contains(text(), "${lifeCycleOption}")]`;
            browser.useCss();
            browser.clickOnElement(elements.lifeCycleDropDown);
            browser.useXpath();
            switch (lifeCycleOption) {
                case `${globalConfig.lifeCycleStages.created}`:
                    browser.clickOnElement(lifeCycleElement);
                    break;
                case `${globalConfig.lifeCycleStages.prioritization}`:
                    browser.clickOnElement(lifeCycleElement);
                    break;
                case globalConfig.lifeCycleStages.planning:
                    browser.clickOnElement(lifeCycleElement);
                    break;
                case globalConfig.lifeCycleStages.approval:
                    browser.clickOnElement(lifeCycleElement);
                    break;
                case globalConfig.lifeCycleStages.execution:
                    browser.clickOnElement(lifeCycleElement);
                    break;
                case globalConfig.lifeCycleStages.done:
                    browser.clickOnElement(lifeCycleElement);
                    break;
                case globalConfig.lifeCycleStages.archived:
                    browser.clickOnElement(lifeCycleElement);
                    break;
            }
        },

        getProjectPriorityElement(priority) {
            browser = this.api;
            browser.useCss();
            return `li[id = "option-${priority}"]`;
        },

        changeProjectPriority(priority) {
            browser = this.api;
            elements = this.elements;
            const priorityElement = `li[id = "option-${priority}"]`;
            switch (priority) {
                case `${globalConfig.priorityOptions.low}`:
                    browser.clickOnElement(priorityElement);
                    break;
                case `${globalConfig.priorityOptions.medium}`:
                    browser.clickOnElement(priorityElement);
                    break;
                case `${globalConfig.priorityOptions.high}`:
                    browser.clickOnElement(priorityElement);
                    break;
                case `${globalConfig.priorityOptions.critical}`:
                    browser.clickOnElement(priorityElement);
                    break;
            }
        },

        getProjectTagElement(tagName) {
            browser = this.api;
            browser.useXpath();
            return `//div[contains(text(), "${tagName}")]/../div[@class = "css-xb97g8"]`;
        },

        addProjectTag(newTag) {
            browser = this.api;
            elements = this.elements;
            browser.clickOnElement(elements.projectTagContainer);
            browser.setValueInElement(elements.projectTagInput, newTag);
            browser.sendKeys(
                'css selector',
                elements.projectTagInput,
                browser.Keys.ENTER);
        },

        deleteProjectTag(projectTag) {
            browser = this.api;
            elements = this.elements;
            browser.useXpath();
            const deleteTagElement = `//div[contains(text(), "${projectTag}")]/../div[@class = "css-xb97g8"]`;
            browser.clickOnElement(deleteTagElement);
        },

        clickMoreProjectData() {
            browser = this.api;
            elements = this.elements;
            browser.useCss();
            browser.clickOnElement(elements.moreProjectDataButton);
        },

        assignProject(userName) {
            browser = this.api;
            elements = this.elements;
            const dropDownUsers = `//div[contains(@id, "react-select") and contains(text(), "${userName}")]`;
            browser.useCss();
            browser.clickOnElement(elements.assigneeContainerText);
            browser.useXpath();
            browser.clickOnElement(dropDownUsers);
        }
    }]
};
