const globalConfig = require('../../../../../utilities/globals');
let browser, elements;
module.exports = {
    elements: {
        createProjects: {
            selector: 'div[class*= "bocmvt"]:nth-child(1)'
        },
        createdProjectsList: {
            selector: 'div[class*= "innerScrollContainer"]'
        },
        lastProjectCard: {
            selector: 'div[class*= "hEEsTI"]:first-child'
        },
        lastProjectCardText: {
            selector: 'div[class*= "hEEsTI"]:first-child div[class = "activity-board-item-label"]:first-child'
        },
        duplicateButton: {
            selector: 'div[id = "modal_button_Duplicate_Duplicate"]'
        },
        confirmDelete: {
            selector: 'div[id = "modal_button_Delete_Delete"]'
        },
        alertDarkCloseButton: {
            selector: 'div[class*="alert dark"] div[class = "close-icon"]'
        },
        archivedProjectsCollapse: {
            selector: 'div[id = "projects_content_area_title"]'
        },
        // Project Template
        projectTemplate: {
            selector: 'div[id = "private_templates_area_title"]'
        },
        projectTemplateCollapse: {
            selector: 'div[id = "private_templates_area_title"] div[class = "project-collapse"] i'
        },
        projectTemplateCollapsed: {
            selector: 'div[id = "private_templates_area_title"] div[class = "project-collapse collapsed"] i'
        },

        /** Project Settings **/
        assigneeContainerText: {
            selector: 'div[id= "assigneeList"] div[class*= "singleValue"]'
        }
    },

    commands: [{

        getProjectName(projectName) {
            browser = this.api;
            browser.useCss();
            return `div[title = "${projectName}"]`;
        },

        openProjectSettingsByButton(projectName) {
            browser = this.api;
            const settingsButton = `//div[@title = "${projectName}"]/../../..//i[contains(@id, "fa-cog")]`;
            browser.useXpath();
            browser.clickOnElement(settingsButton);
        },

        openProjectSettings(projectName, option) {
            browser = this.api;
            elements = this.elements;
            let projectSettings = `div[title = "${projectName}"]`;
            const settingsOptions = `tr[id*= "${option}"]`;
            browser.useCss();
            browser.moveToElement('css selector', projectSettings, 1, 1);
            browser.pause(500);
            if (option === globalConfig.contextMenu.unArchive) {
                browser.useXpath();
                projectSettings = `//div[@title = "${projectName}"]/../../..//i[contains(@id, "new_task_list_icon_fal")]`;
                browser.clickOnElement(projectSettings);
            } else {
                browser.mouseButtonClick(2);
            }
            browser.useCss();
            browser.clickOnElement(settingsOptions);
        },

        projectMenuAction(projectName, option) {
            browser = this.api;
            elements = this.elements;
            switch (option) {
                case 'duplication':
                    this.openProjectSettings(`${projectName}`, `${globalConfig.contextMenu.duplication}`);
                    browser.clickOnElement(elements.openProjectCheckBox);
                    browser.clickOnElement(elements.duplicateButton);
                    break;
                case 'archive':
                    this.openProjectSettings(`${projectName}`, `${globalConfig.contextMenu.archive}`);
                    break;
                case 'unArchive':
                    this.openProjectSettings(`${projectName}`, `${globalConfig.contextMenu.unArchive}`);
                    break;
                case 'createTemplate':
                    this.openProjectSettings(`${projectName}`, `${globalConfig.contextMenu.createTemplate}`);
                    break;
                case 'delete':
                    this.openProjectSettings(`${projectName}`, `${globalConfig.contextMenu.delete}`);
                    browser.clickOnElement(elements.confirmDelete);
                    browser.clickOnElement(elements.alertDarkCloseButton);
                    break;
                case 'deleteTemplate':
                    this.openProjectTemplateSettings(`${projectName}`, `${globalConfig.contextMenu.deleteTemplate}`);
                    browser.clickOnElement(elements.confirmDelete);
                    browser.clickOnElement(elements.alertDarkCloseButton);
                    break;
            }
            browser.pause(1000);
        }
    }]
};
