let browser, elements;
const globalConfig = require('../../../../../../../utilities/globals');

module.exports = {
    elements: {

        /** Activity Details Modal - Info **/
        infoTab: {
            selector: 'div[id = "infoTab"]'
        },
        activitySettingsNameInput: {
            selector: 'div[class*= "activity-list-item"] div[class = "label-container"] input[class = "label-input"]'
        },
        activitySettingsPopup: {
            selector: 'div[class*= "activity-details-settings"]'
        },
        activityNameText: {
            selector: 'div[class*= "activity-name"] div[id = "inline-edit-text-component"]'
            // selector: 'div[class = "activity-name"]'
        },
        activityNameInput: {
            selector: 'input[id = "editTextInput"]'
            // selector: 'input[id = "editTextInput"]'
        },
        tasksList: {
            selector: 'div[class = "activity-details-tasks-list-container"]'
        },

        // Activity/Task Menu
        copy: {
            selector: 'tr[id = "id_Copy_rmtaskList"]'
        },
        cut: {
            selector: 'tr[id = "id_Cut_rmtaskList"]'
        },
        paste: {
            selector: 'tr[id = "id_Paste_rmtaskList"]'
        },
        reportAsOnTrack: {
            selector: 'tr[id = "id_Report as on Track_rmtaskList"]'
        },
        watch: {
            selector: 'tr[id = "id_Watch_rmtaskList"]'
        },
        addBudget: {
            selector: 'tr[id = "id_Add Budget_rmtaskList"]'
        },
        delete: {
            selector: 'tr[id = "id_Delete_rmtaskList"]'
        },

        // Date Picker
        activityStartDate: {
            selector: 'div[title = "Choose start date"]'
        },
        activityEndDate: {
            selector: 'div[title = "Choose end date"]'
        },
        activityEndDateText: {
            selector: 'div[title = "Choose end date"] div[class = "date-picker-container picker-container"]'
        },
        activityDatePickerDisabled: {
            selector: 'div[class = "info-tab-top-content"] div[id = "dates"]'
        },
        activityDatePickerPopup: {
            selector: 'div[class = "DayPickerInput-Overlay"]'
        },
        activityWorkingDays: {
            selector: 'div[class = "info-tab-top-container"] div[id= "inline-edit-text-component"]'
        },
        activityWorkingDaysInput: {
            selector: 'div[class = "info-tab-top-container"] div[id= "inline-edit-text-component"] input[id = "editTextInput"]'
        },

        // Description
        activityMainDescription: {
            selector: 'div[class*= "activity-name"] div[id = "inline-edit-text-component"]'
        },
        activityDescription: {
            selector: 'div[class = "description-section"] div[id = "inline-edit-text-component"]'
        },
        activityDescriptionInput: {
            selector: 'div[class = "quill "]'
        },
        activityDescriptionConfirm: {
            selector: 'div[class = "description-section"] i[class = "fa fa-check"]'
        },
        activityDescriptionCancel: {
            selector: 'div[class = "description-section"] i[class = "fa fa-times"]'
        },

        // Assignee
        activityAssignee: {
            selector: 'div[class = "flex vertical"] div[class*= "singleValue"]'
        },
        assigneeContainerText: {
            selector: 'div[id = "assigneeList"] div[class*= "singleValue"] div'
        },
        activityAssigneeDropdownMenu: {
            selector: 'div[class = " css-1gcltae-menu"]'
        },
        activityAddAssignee: {
            selector: 'div[class = "flex vertical"] div[title= "Add new assignee"]'
        },
        activityAssigneeLoad: {
            selector: 'input[id = "assigneePercentage"]'
        },
        activityAssigneeLoadDisabled: {
            selector: 'input[id = "assigneePercentage"][disabled]'
        },

        // Progress Bar
        activityProgressPercentage: {
            selector: 'div[class = "progress-section ui-form flex vertical"] div[class = "label form"] span'
        },
        activityProgressBar: {
            selector: 'div[class = "input-range__slider"]'
        },
        activityProgressMin: {
            selector: 'span[class = "input-range__label input-range__label--min"]'
        },
        activityProgressMax: {
            selector: 'span[class = "input-range__label input-range__label--max"]'
        },

        // Priority
        priorityLow: {
            selector: 'div[class = "priority-section ui-form flex"] li[id = "option-Low"]'
        },
        priorityMedium: {
            selector: 'div[class = "priority-section ui-form flex"] li[id = "option-Medium"]'
        },
        priorityHigh: {
            selector: 'div[class = "priority-section ui-form flex"] li[id = "option-High"]'
        },
        priorityCritical: {
            selector: 'div[class = "priority-section ui-form flex"] li[id = "option-Critical"]'
        },

        // Colors
        colorSection: {
            selector: 'div[class = "color-and-risk-section ui-form flex"] div[class = "color-section"]'
        },
        useWorkStreamColor: {
            selector: 'div[class = "color-and-risk-section ui-form flex"] span[class = "inline-link"]'
        },
        moreColorsButton: {
            selector: 'div[class = "color-and-risk-section ui-form flex"] span[class = "inline-link"]'
        },
        colorUseWorkStream: {
            selector: 'div[class = "color-and-risk-section ui-form flex"] span[class = "inline-link"]'
        },
        colorId: {
            selector: 'div[class = "items-container"] div[id*= "color"]:nth-child(2)'
        },
        colorRisk: {
            selector: 'div[class = "risk-section"] input[id = "risk"]'
        },
        colorRiskDisabled: {
            selector: 'div[class = "risk-section"] input[id = "risk"][disabled]'
        },

        // Tags
        activityTags: {
            selector: 'div[class = " css-z63xjp-container"]'
        },
        activityTagsDropdown: {
            selector: 'div[class = " css-11unzgr"]'
        },

        // Buttons
        addNewTaskButton: {
            selector: 'div[class = "activity-details-tasks-container"] div[class = "activity-details-tasks-list-add-task"]'
        },
        addTaskButton: {
            selector: 'div[class = "buttons-container"] div[id= "Add Task_button"]'
        },
        deleteButton: {
            selector: 'div[class = "buttons-container"] div[id= "deleteButton"]'
        },
        confirmDelete: {
            selector: 'div[id = "popoverOkButton"]'
        },
        saveButton: {
            selector: 'div[class = "buttons-container"] div[id= "saveButton"]'
        },
        closeSettings: {
            selector: 'div[class = "activity-details-close-button"] i[class = "fal fa-times"]'
        },

        /** Activity Details Modal - Comments **/
        commentsTab: {
            selector: 'div[id = "chatTab"]'
        },

        /** Activity Details Modal - Files **/
        fileTab: {
            selector: 'div[id = "filesTab"]'
        },

        /** Activity Details Modal - Checklist **/
        checkListTab: {
            selector: 'div[id = "checklistTab"]'
        },

        /** Activity Details Modal - Dependencies **/
        dependenciesTab: {
            selector: 'div[id = "linksTab"]'
        },
        activityConnectDropDown: {
            selector: '//div[@class = "selected-activity-settings-content"]//div[contains(text(), "Choose Activity")]',
            locateStrategy: 'xpath'
        },
        confirmActivityConnectButton: {
            selector: 'i[class*= "add"]'
        }

    },

    commands: [{

        assertActivityOrTask(activityOrTaskName, assertOption) {
            browser = this.api;
            elements = this.elements;
            const activityOrTask = `//div[contains(@class, "activity-list-item")]//div[@title = "${activityOrTaskName}"]`;
            if (assertOption === 'not') {
                browser.expect.element(activityOrTask).to.not.be.present.before(globalConfig.timeOut);
            } else {
                browser.expect.element(activityOrTask).to.be.present.before(globalConfig.timeOut);
            }
        },

        async getActivityOrTaskName(activityOrTaskName) {
            browser = this.api;
            elements = this.elements;
            const activityOrTask = `//div[contains(@class, "activity-list-item")]//div[@title = "${activityOrTaskName}"]`;
            browser.waitForElementVisible(activityOrTask);
            return browser.getText(activityOrTask);
        },

        async getActivityDateInModal() {
            browser = this.api;
            elements = this.elements;
            browser.waitForElementVisible(elements.activityEndDateText);
            return browser.getText(elements.activityEndDateText);
        },

        async getTaskDateInModal() {
            browser = this.api;
            elements = this.elements;
            browser.waitForElementVisible(elements.activityEndDateText);
            return browser.getText(elements.activityEndDateText);
        },

        openTaskOrActivitySettings(myBrowser, activityOrTaskName, option) {
            browser = this.api;
            elements = this.elements;
            browser.useXpath();
            const activityOrTask = `//div[contains(@class, "activity-list-item")]//div[@title = "${activityOrTaskName}"]/..//i[contains(@class, "settings-button")]`;
            browser.moveToElement('xpath', activityOrTask, 1, 1);
            browser.mouseButtonClick(0);
            myBrowser.clickOnElement(option);
        },

        renameActivityTask(activityName) {
            browser = this.api;
            elements = this.elements;
            browser.useCss();
            browser.clickOnElement(elements.activityNameText);
            browser.setValueInElement(elements.activityNameInput, activityName);
            browser.keys([browser.Keys.ENTER]);
            browser.pause(1000);
        },

        addNewTask(newTaskName) {
            browser = this.api;
            elements = this.elements;
            browser.clickOnElement(elements.addTaskButton);
            browser.setValueInElement(elements.activitySettingsNameInput, newTaskName);
            browser.keys([browser.Keys.ENTER]);
            browser.pause(1500);
        },

        deleteTask(taskName) {
            browser = this.api;
            elements = this.elements;
            const activityOrTask = `div[class*= "activity-list-item"] div[class = "label-container"] div[title = "${taskName}"]`;
            browser.useCss();
            browser.clickOnElement(activityOrTask);
            browser.clickOnElement(elements.deleteButton);
            browser.pause(1500);
        },

        renameTask(taskName) {
            browser = this.api;
            const activityOrTask = `div[class*= "activity-list-item"] div[class = "label-container"] div[title = "${taskName}"]`;
            browser.useCss();
            browser.clickOnElement(activityOrTask);
            browser.pause(1500);
        },

        verifyTaskActivityPresent(taskName, expectedText) {
            browser = this.api;
            const task = `div[class*= "activity-list-item"] div[class = "label-container"] div[title = "${taskName}"]`;
            browser.useCss();
            browser.expect.element(task).text.to.contain(expectedText);
        },

        assignActivityTask(activityName, assigneeName) {
            browser = this.api;
            elements = this.elements;
            const activityOrTaskTitle = `div[class*= "activity-list-item"] div[class = "label-container"] div[title = "${activityName}"]`;
            const dropDownUsers = `//div[contains(@id, "react-select")]/div[contains(text(), "${assigneeName}")]`;
            browser.useCss();
            browser.clickOnElement(activityOrTaskTitle);
            browser.clickOnElement(elements.activityAssignee);
            browser.useXpath();
            browser.clickOnElement(dropDownUsers);
        },

        async deleteActivityInModal() {
            browser = this.api;
            elements = this.elements;
            browser.elements('css selector', elements.tasksList, results => {
                const isTrue = results.value.length > 0;
                if (isTrue) {
                    browser.clickOnElement(elements.deleteButton);
                    browser.clickOnElement(elements.confirmDelete);
                    browser.pause(1000);
                } else {
                    browser.clickOnElement(elements.deleteButton);
                    browser.pause(1000);
                }
            });
        },

        selectTaskOrActivityInModal(taskName) {
            browser = this.api;
            elements = this.elements;
            const task = `//div[contains(@class, "activity-list-item")]//div[@title = "${taskName}"]/..//span[@class = "activity-list-icon"]`;
            browser.useXpath();
            browser.clickOnElement(task);
        },

        connectToActivity(activityName) {
            browser = this.api;
            elements = this.elements;
            const activity = `//div[contains(@class, "menu")]//span[contains(text(), "${activityName}")]`;
            browser.useXpath();
            browser.clickOnElement(elements.activityConnectDropDown);
            browser.clickOnElement(activity);
            browser.clickOnElement(elements.confirmActivityConnectButton);
        },

        verifyActivityConnected(activityName) {
            browser = this.api;
            const activityConnected = `span[title = "${activityName}"]`;
            browser.useCss();
            browser.waitForElementVisible(activityConnected);
            browser.expect.element(activityConnected).to.be.present.before(globalConfig.timeOut);
        },

        activityModalChooseTab(myBrowser, tabName) {
            myBrowser.clickOnElement(tabName);
        },

        deleteActivityConnection(activityName) {
            browser = this.api;
            const activityConnected = `//span[@title = "${activityName}"]/../i[contains(@class, "delete")]`;
            browser.useXpath();
            browser.clickOnElement(activityConnected);
        },

        activityModalDatePicker(datePickerOption, startDateDay, endDateDay) {
            browser = this.api;
            elements = this.elements;
            browser.useCss();
            if (datePickerOption === 'start date') {
                const start = `div[class = "DayPickerInput-Overlay"] div[aria-label*= "${startDateDay}"]`;
                const end = `div[class = "DayPickerInput-Overlay"] div[aria-label*= "${endDateDay}"]`;
                browser.clickOnElement(elements.activityStartDate);
                browser.clickOnElement(start);
                browser.clickOnElement(end);
            } else {
                const dates = `div[class = "DayPickerInput-Overlay"] div[aria-label*= "${endDateDay}"]`;
                browser.clickOnElement(elements.activityEndDate);
                browser.clickOnElement(dates);
            }
            browser.pause(1000);
        },

        closeActivitySettings() {
            browser = this.api;
            elements = this.elements;
            browser.clickOnElement(elements.closeSettings);
        }
    }]
};
