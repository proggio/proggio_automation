let browser, elements;
module.exports = {
    elements: {
        projectGirdLeftArrow: {
            selector: 'i[class*= "fa fa-chevron-left"]'
        },
        projectGridToday: {
            selector: '//*[name() = "text" and text() = "Today"]',
            locateStrategy: 'xpath'
        },
        activityPlaceHolder: {
            selector: '//*[name() = "g" and contains(@id, "activity-place-holder")]',
            locateStrategy: 'xpath'
        },
        activityCard: {
            selector: '//*[name() = "g" and contains(@id, "map_activity_id")]',
            locateStrategy: 'xpath'
        },
        activityCardText: {
            selector: '//*[name() = "g" and contains(@id, "map_activity_id")]//*[name() = "p"]',
            locateStrategy: 'xpath'
        },
        activityCardInput: {
            selector: '//*[name() = "g" and contains(@id, "map_activity_id")]//*[name() = "input"]',
            locateStrategy: 'xpath'
        },

        /** Activity Context Menu **/
        details: {
            selector: 'tr[id*= "id_Details_activity"]'
        },
        copy: {
            selector: 'tr[id*= "id_Copy_activity"]'
        },
        paste: {
            selector: 'tr[id = "id_Paste_grid"]'
        },
        cut: {
            selector: 'tr[id*= "id_Cut_activity"]'
        },
        reportOnTrack: {
            selector: 'tr[id*= "id_Report On Track_activity"]'
        },
        toBuffer: {
            selector: 'tr[id*= "id_To buffer_activity"]'
        },
        watch: {
            selector: 'tr[id*= "id_Watch_activity"]'
        },
        recurring: {
            selector: 'tr[id*= "id_Recurring_activity"]'
        },
        createSubProject: {
            selector: 'tr[id*= "id_Create Sub-Project_activity"]'
        },
        deleteSubProject: {
            selector: 'tr[id = "trash_button2"]'
        },
        addBudget: {
            selector: 'tr[id*= "id_Add Budget_activity"]'
        },
        delete: {
            selector: 'tr[id = "trash_button"]'
        },

        /** Activity Details Modal - Budget **/
        budgetTab: {
            selector: 'div[id = "budgetTab"]'
        },

        /** Project Controls **/
        // Zoom In/Out
        zoomInProject: {
            selector: 'span[id = "zoom_in_button"]'
        },
        zoomOutProject: {
            selector: 'span[id = "zoom_out_button"]'
        },
        zoomToProject: {
            selector: 'span[id = "zoom_to_project_button"]'
        },
        zoomToToday: {
            selector: 'span[id = "Go to Today"]'
        },
        // Undo/Redo
        undoButton: {
            selector: 'span[id = "undo_button"]'
        },
        redoButton: {
            selector: 'span[id = "redo_button"]'
        },

        collaborationButton: {
            selector: 'div[title = "Project Collaboration"]'
        },
        todayFlag: {
            selector: 'g[id = "today-header"]'
        },

        /** Activity Progress Bar **/
        progressBarSlider: {
            selector: 'div[class = "popover open"] div[class = "input-range__slider"]'
        },
        progressBarOnTrackButton: {
            selector: 'div[class = "popover open"] div[class = "status-popover-tag"]'
        },
        progressBarNumText: {
            selector: 'span[class*= "value"] span[class = "input-range__label-container"]'
        }
    },

    commands: [{

        getProjectTitleElement(projectTitle) {
            browser = this.api;
            elements = this.elements;
            browser.useXpath();
            return `//div[@id = "toolbar-title" and contains(text(), "${projectTitle.toUpperCase()}")]`;
        },

        async getActivityXAxisLocation(activityId) {
            browser = this.api;
            browser.useCss();
            const activity = `#${activityId}`;
            await browser.waitForElementVisible(activity);
            const result = await browser.getLocation('css selector', activity);
            return result.value.x;
        },

        async getActivityId(activityName) {
            browser = this.api;
            const activity = `//p[contains(text(), "${activityName}")]/../../../../../..`;
            browser.useXpath();
            const activityId = await browser.getAttribute('xpath', activity, 'id');
            return `${activityId.value}`;
        },

        async getActivityWidth(startActivityId) {
            browser = this.api;
            const activity = `#${startActivityId}`;
            const activitySize = await browser.getElementSize('css selector', activity);
            return Math.floor(activitySize.value.width);
        },

        async openActivitySettingsById(myBrowser, activityId, option) {
            browser = this.api;
            const activity = `#${activityId}`;
            const activitySize = await browser.getElementSize('css selector', activity);
            const activityWidth = Math.floor(activitySize.value.width / 2);
            await browser.moveToElement('css selector', activity, activityWidth, 1);
            await browser.mouseButtonClick(2);
            await myBrowser.clickOnElement(option);
        },

        async openActivitySettingsMouseClick(myBrowser, activityName, option) {
            browser = this.api;
            const activityCard = `//*[name() = "g" and contains(@id, "map_activity_id")]//p[contains(text(), "${activityName}")]`;
            const size = await browser.getElementSize('xpath', activityCard);
            const height = size.value.height;
            const width = size.value.width;
            await browser.moveToElement('xpath', activityCard, width / 2, height / 2);
            await browser.mouseButtonClick(2);
            await myBrowser.clickOnElement(option);
        },

        async openActivitySettingDoubleClick(activityName) {
            browser = this.api;
            const activityCard = `//*[name() = "g" and contains(@id, "map_activity_id")]//p[contains(text(), "${activityName}")]`;
            browser.useXpath();
            browser.pause(500);
            await browser.waitForElementVisible(activityCard);
            const size = await browser.getElementSize('xpath', activityCard);
            const height = size.value.height;
            const width = size.value.width;
            await browser.moveToElement('xpath', activityCard, width / 2, height / 2);
            await browser.doubleClick();
        },

        async createActivity() {
            browser = this.api;
            elements = this.elements;
            await browser.useCss();
            await browser.clickOnElement(elements.zoomToProject);
            await browser.moveToElement('xpath', elements.projectGridToday, 0, 35);
            await browser.waitForElementVisible(elements.activityPlaceHolder);
            await browser.doubleClick();
            await browser.pause(1000);
        },

        async createNewActivity(startActivityId) {
            browser = this.api;
            elements = this.elements;
            const mainActivity = `#${startActivityId}`;
            await browser.useCss();
            await browser.clickOnElement(elements.zoomToProject);
            await browser.waitForElementVisible(mainActivity);
            const activitySize = await browser.getElementSize('css selector', mainActivity);
            const activityWidth = Math.floor(activitySize.value.width);
            await browser.moveToElement('css selector', mainActivity, activityWidth + 15, 1);
            await browser.waitForElementVisible(elements.activityPlaceHolder);
            await browser.doubleClick();
            await browser.pause(1000);
        },

        async moveActivityById(startActivityId, endActivityId, dragOffsetX) {
            browser = this.api;
            await browser.useCss();
            const cssSelector = 'css selector';
            const dragActivity = `#${startActivityId}`;
            const dropActivity = `#${endActivityId}`;

            await browser.waitForElementVisible(dragActivity);

            const dragSize = await browser.getElementSize(cssSelector, dragActivity);
            const dragWidth = Math.floor(dragSize.value.width / 2);

            await browser.moveToElement(cssSelector, dragActivity, dragWidth, 1);
            await browser.mouseButtonDown(0);
            await browser.pause(500);
            // workaround for moving activities, needs to moveToElement() to the same element TWICE with different pixels
            await browser.moveToElement(cssSelector, dragActivity, dragWidth, 1);
            await browser.moveToElement(cssSelector, dropActivity, -dragWidth + dragOffsetX, 1);
            await browser.mouseButtonUp(0);
            await browser.pause(500);
        },

        async moveActivitiesByOffset(startActivityId, endActivityId, dragX) {
            browser = this.api;
            const cssSelector = 'css selector';
            const dragActivity = `#${startActivityId}`;
            const dropActivity = `#${endActivityId}`;
            await browser.waitForElementVisible(dragActivity);
            const dragSize = await browser.getElementSize(cssSelector, dragActivity);
            const dragWidth = Math.floor(dragSize.value.width / 2);
            await browser.keys(browser.Keys.CONTROL);
            await browser.moveToElement(cssSelector, dragActivity, dragWidth, 5);
            await browser.mouseButtonClick(0);
            await browser.pause(500);
            await browser.moveToElement(cssSelector, dropActivity, dragWidth, 5);
            await browser.mouseButtonClick(0);
            await browser.keys(browser.Keys.NULL);
            await browser.pause(500);
            await browser.moveToElement(cssSelector, dragActivity, dragWidth, 5);
            await browser.mouseButtonDown(0);
            // workaround for moving activities, needs to moveToElement() to the same element TWICE with different pixels
            await browser.moveToElement(cssSelector, dragActivity, dragWidth + 1, 1);
            await browser.moveToElement(cssSelector, dragActivity, dragWidth + dragX, 1);
            await browser.mouseButtonUp(0);
            await browser.pause(1000);
            await browser.keys(browser.Keys.CONTROL);
            await browser.moveToElement(cssSelector, dragActivity, dragWidth, 5);
            await browser.mouseButtonClick(0);
            await browser.pause(500);
            await browser.moveToElement(cssSelector, dropActivity, dragWidth, 5);
            await browser.mouseButtonClick(0);
            await browser.keys(browser.Keys.NULL);
            await browser.pause(1000);
        },

        async moveActivityByOffset(startActivityId, dragX) {
            browser = this.api;
            const cssSelector = 'css selector';
            const dragActivity = `#${startActivityId}`;
            await browser.waitForElementVisible(dragActivity);
            const dragSize = await browser.getElementSize(cssSelector, dragActivity);
            const dragWidth = Math.floor(dragSize.value.width / 2);
            await browser.pause(1000);
            await browser.moveToElement(cssSelector, dragActivity, dragWidth, 5);
            await browser.mouseButtonDown(0);
            await browser.pause(500);
            // workaround for moving activities, needs to moveToElement() to the same element TWICE with different pixels
            await browser.moveToElement(cssSelector, dragActivity, dragWidth + 1, 1);
            await browser.moveToElement(cssSelector, dragActivity, dragWidth + dragX, 1);
            await browser.mouseButtonUp(0);
            await browser.pause(1000);
        },

        async resizeActivity(startActivityId, resizeDirection, dragX) {
            browser = this.api;
            const dragActivity = `#${startActivityId}`;
            const cssSelector = 'css selector';
            await browser.waitForElementVisible(dragActivity);
            const dragWidth = await this.getActivityWidth(startActivityId);

            if (resizeDirection === 'right') {
                await browser.pause(1000);
                await browser.moveToElement(cssSelector, dragActivity, dragWidth, 5);
                await browser.mouseButtonDown(0);
                await browser.pause(500);
                // workaround for moving activities, needs to moveToElement() to the same element TWICE with different pixels
                await browser.moveToElement(cssSelector, dragActivity, dragWidth - 1, 1);
                await browser.moveToElement(cssSelector, dragActivity, dragWidth + dragX, 1);
                await browser.mouseButtonUp(0);
                await browser.pause(500);
            } else {
                await browser.pause(1000);
                await browser.moveToElement(cssSelector, dragActivity, 1, 5);
                await browser.mouseButtonDown(0);
                await browser.pause(500);
                // workaround for moving activities, needs to moveToElement() to the same element TWICE with different pixels
                await browser.moveToElement(cssSelector, dragActivity, 2, 1);
                await browser.moveToElement(cssSelector, dragActivity, 2 + dragX, 1);
                await browser.mouseButtonUp(0);
                await browser.pause(500);
            }
        },

        // For Copy/Paste Activities
        async pasteActivityAndOpen(activityId, dragX) {
            browser = this.api;
            elements = this.elements;
            browser.useCss();
            const cssSelector = 'css selector';
            const dragActivity = `#${activityId}`;
            await browser.waitForElementVisible(dragActivity);
            const dragSize = await browser.getElementSize(cssSelector, dragActivity);
            const dragWidth = Math.floor(dragSize.value.width);
            await browser.moveToElement(cssSelector, dragActivity, dragWidth + dragX, 5);
            await browser.mouseButtonClick(2);
            await browser.clickOnElement(elements.paste);
            await browser.pause(1500);
            await browser.moveToElement(cssSelector, dragActivity, dragWidth + dragX, 5);
            await browser.doubleClick();
            await browser.pause(1000);
        },

        // For Cut/Paste Activities
        async pasteActivity(activityId, pasteDirection, dragX) {
            browser = this.api;
            elements = this.elements;
            browser.useCss();
            const cssSelector = 'css selector';
            const dragActivity = `#${activityId}`;
            await browser.waitForElementVisible(dragActivity);
            const dragSize = await browser.getElementSize(cssSelector, dragActivity);
            const dragWidth = Math.floor(dragSize.value.width);
            if (pasteDirection === 'right') {
                await browser.moveToElement(cssSelector, dragActivity, dragWidth + dragX, 5);
                await browser.mouseButtonClick(2);
                await browser.clickOnElement(elements.paste);
                await browser.pause(1500);
            } else {
                await browser.moveToElement(cssSelector, dragActivity, 1 + dragX, 5);
                await browser.mouseButtonClick(2);
                await browser.clickOnElement(elements.paste);
                await browser.pause(1500);
            }
        },

        zoomIntoProject() {
            browser = this.api;
            elements = this.elements;
            browser.clickOnElement(elements.zoomToProject);
            browser.pause(2000);
        },

        zoomToTodayProject() {
            browser = this.api;
            elements = this.elements;
            browser.clickOnElement(elements.zoomToToday);
            browser.pause(2000);
        },

        zoomInMyProject(zoomNums) {
            browser = this.api;
            elements = this.elements;
            for (let i = 0; i < zoomNums; i++) {
                browser.clickOnElement(elements.zoomInProject);
            }
            browser.pause(2000);
        },

        zoomOutMyProject(zoomNums) {
            browser = this.api;
            elements = this.elements;
            for (let i = 0; i < zoomNums; i++) {
                browser.clickOnElement(elements.zoomOutProject);
            }
            browser.pause(2000);
        },

        openProjectChat() {
            browser = this.api;
            elements = this.elements;
            browser.clickOnElement(elements.collaborationButton);
        },

        openProjectSettings(projectName) {
            browser = this.api;
            elements = this.elements;
            const project = `div[id = "toolbar-title"][title*= "${projectName}"]`;
            browser.clickOnElement(project);
        },

        openActivityProgressBar(activityId) {
            browser = this.api;
            elements = this.elements;
            const progressId = `#${activityId}_progress`;
            browser.clickOnElement(progressId);
        },

        moveActivityProgressBar(dragNum) {
            browser = this.api;
            elements = this.elements;
            browser.moveToElement('css selector', elements.progressBarSlider, 1, 1);
            browser.mouseButtonDown(0);
            browser.moveToElement('css selector', elements.progressBarSlider, dragNum, 1);
            browser.mouseButtonUp(0);
            browser.pause(1500);
            browser.moveToElement('css selector', elements.zoomInProject, 1, 1);
        },

        undoAction() {
            browser = this.api;
            elements = this.elements;
            browser.clickOnElement(elements.undoButton);
        },

        redoAction() {
            browser = this.api;
            elements = this.elements;
            browser.clickOnElement(elements.redoButton);
        }
    }]
};
