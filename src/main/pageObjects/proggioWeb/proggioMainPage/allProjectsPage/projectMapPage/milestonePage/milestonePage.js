let browser, elements;
module.exports = {
    elements: {
        timeline: {
            selector: '//*[name() = "svg" and @class = "time-line-svg"]',
            locateStrategy: 'xpath'
        },
        timelineDays: {
            selector: '//*[name() = "svg" and @class = "time-line-svg"]//*[name() = "text" and contains(text(), "19")]',
            locateStrategy: 'xpath'
        },
        timelineMonths: {
            selector: '//*[name() = "svg" and @class = "time-line-svg"]//*[name() = "text" and contains(text(), "Feb / 2021")]',
            locateStrategy: 'xpath'
        },
        milestoneHoverText: {
            selector: '//*[name() = "g" and contains(@style, "transform")]//*[name() = "text" and @y = "-2"]',
            locateStrategy: 'xpath'
        },
        milestoneStrip: {
            selector: '//*[name() = "g" and @class = "milestone-strip"]',
            locateStrategy: 'xpath'
        },
        milestone: {
            selector: '//*[name() = "g" and contains(@style, "transform")]/*[name() = "g"]/*[name() = "g"]/*[name() ="g"]/*[name() = "rect"]',
            locateStrategy: 'xpath'
        },
        milestoneID: {
            selector: 'div[id*= "milestone"]'
        },

        // Milestone Settings
        milestoneSettingsPopup: {
            selector: 'div[class*= "hZhaGx"]'
        },
        milestoneInput: {
            selector: 'input[id = "milestone_name"]'
        },
        milestoneDescription: {
            selector: 'div[class*= "hZhaGx"] textarea'
        },
        milestoneDelete: {
            selector: 'div[id = "modal_button_Milestone Settings_Delete Milestone"]'
        },
        milestoneSaveButton: {
            selector: 'div[id = "modal_button_Milestone Settings_Save"]'
        },
        datePicker: {
            selector: 'div[id= "date_picker"] div[class = "DayPickerInput"]'
        },
        dateContainerText: {
            selector: 'div[class*= "date-picker-container"] div[class = "ellipsis"]'
        },

        // PopOver
        milestoneActualDelete: {
            selector: 'div[id = "popoverOkButton"]'
        },
        mileStoneCancelButton: {
            selector: 'div[id = "popoverCancelButton"]'
        }
    },

    commands: [{

        async getMilestoneId(milestoneName) {
            browser = this.api;
            const milestone = `div[title = "${milestoneName}"]`;
            const milestoneId = await browser.getAttribute('css selector', milestone, 'id');
            return `${milestoneId.value}`;
        },

        async openMilestoneSettings(milestoneName) {
            browser = this.api;
            const milestoneId = await this.getMilestoneId(milestoneName);
            await browser.moveToElement('css selector', `#${milestoneId}`, 1, 1);
            await browser.mouseButtonClick(0);
        },

        deleteMilestone(milestoneName) {
            browser = this.api;
            elements = this.elements;
            const milestoneTitle = `div[title = "${milestoneName}"]`;
            browser.useCss();
            browser.moveToElement('css selector', milestoneTitle, 1, 1, function() {
                browser.waitForElementVisible(milestoneTitle);
                browser.pause(1000);
                browser.click(milestoneTitle);
                browser.clickOnElement(elements.milestoneDelete);
                browser.clickOnElement(elements.milestoneActualDelete);
            });
        },

        verifyMilestoneText(milestoneName) {
            browser = this.api;
            const milestoneTitle = `div[title = "${milestoneName}"] div[class = "ellipsis"]`;
            browser.useCss();
            browser.waitForElementVisible(milestoneTitle);
            browser.expect.element(milestoneTitle).text.to.contain(milestoneName);
        },

        milestoneDatePicker(day) {
            browser = this.api;
            elements = this.elements;
            const dayNum = `//div[@class = "DayPicker-Month"]//div[contains(@aria-label, "${day}")]`;
            browser.pause(1000);
            browser.clickOnElement(elements.datePicker);
            browser.useXpath();
            browser.clickOnElement(dayNum);
            browser.pause(1000);
        }
    }]
};
