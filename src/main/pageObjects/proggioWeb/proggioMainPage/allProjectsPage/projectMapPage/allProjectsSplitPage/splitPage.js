let browser, elements;
module.exports = {
    elements: {
        // Popup Confirm Deletion
        confirmDeleteTask: {
            selector: 'div[id = "popoverOkButton"]'
        },
        cancelDeleteTask: {
            selector: 'div[id = "popoverCancelButton"]'
        },
        splitUp: {
            selector: 'i[class*= "circle-up"]'
        },
        splitDown: {
            selector: 'i[class*= "circle-down"]'
        },

        splitGripLines: {
            selector: 'div[id = "bottom-info-seperator"] i[class = "far fa-grip-lines"]'
        },

        // Right Icons
        expandSplitButton: {
            selector: 'div[id = "bottom-info-seperator"] i[class*= "chevron"]'
        },
        minimizeSplitButton: {
            selector: 'div[id = "bottom-info-seperator"] i[class*= "times"]'
        },
        lockSplitButton: {
            selector: 'div[id = "bottom-info-seperator"] i[class*= "lock"]'
        },

        // Split Tabs
        tasksTab: {
            selector: 'div[id= "tab_Tasks"]'
        },
        projectBacklogTab: {
            selector: 'div[id= "tab_Unscheduled"]'
        },
        // Import
        importTab: {
            selector: 'div[id= "tab_Import"]'
        },
        importFromDropDown: {
            selector: 'div[id = "bar"] div[class*= "singleValue"]'
        },
        importButton: {
            selector: 'div[id = "Import_button"]'
        },
        chooseBoardDropDown: {
            selector: '//div[contains(@class, "modal__Card")]//div[contains(@class, "indicatorContainer")]',
            locateStrategy: 'xpath'
        },
        selectTicketsDropDown: {
            selector: '//div[@class = "modal__CardBase-sc-9strd2-2 hurBpQ"]//div[contains(@class, "placeholder")]',
            locateStrategy: 'xpath'
        },
        // First Ticket
        firstJiraTicketDragHandler: {
            selector: 'div[class = "ReactVirtualized__Grid__innerScrollContainer"] div[aria-label]:first-child div[class = "drag-handler"]'
        },

        // Scope
        sprints: {
            selector: 'input[id = "sprints"]'
        },
        epics: {
            selector: 'input[id = "epics"]'
        },
        releaseVersions: {
            selector: 'input[id = "versions"]'
        },
        issue: {
            selector: 'input[id = "issue"]'
        },
        syncButton: {
            selector: 'div[id = "modal_button_Listen to JIRA_Sync"]'
        },

        subProjectTab: {
            selector: 'div[id= "tab_Sub Project"]'
        },

        addTaskButton: {
            selector: 'div[id = "Add Task To this Activity_button"]'
        },
        timelineOption: {
            selector: 'li[id = "option-Timeline"]'
        },
        detailsOption: {
            selector: 'li[id = "option-Details"]'
        },

        // Activity Cell
        activityCell: {
            selector: 'div[class*= "Table__Grid"] div[role = "row"]:first-child'
        },

        // Task Cell
        newTaskText: {
            selector: 'input[id = "new_task_label"]'
        },
        confirmTaskLabelButton: {
            selector: 'i[id = "new_task_list_icon_fas fa-check-circle"]'
        },
        lastTaskCell: {
            selector: 'div[class*= "Table__Grid"] div[role = "row"]:last-child'
        },
        lastTaskCheckBox: {
            selector: 'div[class*= "Table__Grid"] div[role = "row"]:last-child div[class*= "Checkbox"]'
        },
        lastTaskMenu: {
            selector: 'div[class*= "Table__Grid"] div[role = "row"]:last-child i[id*= "fa-ellipsis-v"]'
        },
        lastTaskText: {
            selector: 'div[class*= "Table__Grid"] div[role = "row"]:last-child div[class = "label-div"]'
        },
        selectAllCheckBox: {
            selector: 'div[title = "Select All"]'
        },

        // Task Options
        assignUser: {
            selector: 'i[id = "assign_user_bulk_icon"]'
        },
        changeStatus: {
            selector: 'i[id = "status_bulk_icon"]'
        },
        deleteMyTask: {
            selector: 'i[id = "delete_bulk_icon"]'
        },
        postMessage: {
            selector: 'i[id = "message_bulk_icon"]'
        },
        addTag: {
            selector: 'i[id = "add_tag_bulk_icon"]'
        }
    },

    commands: [{
        activitySplitDatePicker(cellName, datePickerOption, startDateDay, endDateDay) {
            browser = this.api;
            elements = this.elements;
            browser.useXpath();
            const cellStartDate = `//div[contains(@class, "Table__Grid")]//div[contains(@title, "${cellName}")]/../../../../..//div[@class = "timeline-inner"]/div[1]`;
            const start = `//div[@class = "DayPickerInput-Overlay"]//div[contains(@aria-label, "${startDateDay}"]`;
            const cellStartEnd = `//div[contains(@class, "Table__Grid")]//div[contains(@title, "${cellName}")]/../../../../..//div[@class = "timeline-inner"]/div[2]`;
            const end = `//div[@class = "DayPickerInput-Overlay"]//div[contains(@aria-label, "${endDateDay}")]`;
            if (datePickerOption === 'start end date') {
                browser.clickOnElement(cellStartDate);
                browser.clickOnElement(start);
                browser.clickOnElement(cellStartEnd);
                browser.clickOnElement(end);
            } else if (datePickerOption === 'start date') {
                browser.clickOnElement(cellStartDate);
                browser.clickOnElement(start);
            } else {
                browser.clickOnElement(cellStartEnd);
                browser.clickOnElement(end);
            }
            browser.pause(1000);
        },

        async getTaskEndDateInSplit(cellName) {
            browser = this.api;
            elements = this.elements;
            browser.useXpath();
            const cellStartDate = `//div[contains(@class, "Table__Grid")]//div[contains(@title, "${cellName}")]/../../../../..//div[@class = "timeline-inner"]/div[2]//div[@class = "date-picker-container picker-container"]`;
            browser.waitForElementVisible(cellStartDate);
            return browser.getText(cellStartDate);
        },

        openSplit() {
            browser = this.api;
            elements = this.elements;
            browser.pause(1500);
            browser.waitForElementVisible(elements.splitUp);
            browser.moveToElement('css selector', elements.splitUp, 1, 1);
            browser.mouseButtonClick(0);
            browser.pause(1500);
        },

        closeSplit() {
            browser = this.api;
            browser.pause(1500);
            elements = this.elements;
            browser.waitForElementVisible(elements.splitDown);
            browser.moveToElement('css selector', elements.splitDown, 1, 1);
            browser.mouseButtonClick(0);
            browser.pause(1500);
        },

        addTask(taskName) {
            browser = this.api;
            elements = this.elements;
            browser.clickOnElement(elements.addTaskButton);
            browser.setValueInElement(elements.newTaskText, taskName);
            browser.clickOnElement(elements.confirmTaskLabelButton);
        },

        deleteTaskByName: async function(taskName) {
            browser = this.api;
            elements = this.elements;
            let option = `//div[contains(@class, "Table__Grid")]//div[contains(@title, "${taskName}")]/../../../../..//div[contains(@class, "Checkbox")]`;
            await browser.useXpath();
            await browser.waitForElementVisible(option);
            await browser.pause(1500);
            await browser.moveToElement('xpath', option, 5, 5);
            await browser.mouseButtonClick(0);
            await browser.clickOnElement(elements.deleteMyTask);
            await browser.clickOnElement(elements.confirmDeleteTask);
        },

        goToImportTab() {
            browser = this.api;
            elements = this.elements;
            browser.clickOnElement(elements.importTab);
        },

        importProjectBoard(myBrowser, projectType, boardName, scope, ticketName) {
            browser = this.api;
            elements = this.elements;
            const spinner = '//*[name() = "svg" and @class = "spinner"]';
            let option;
            option = `//div[contains(@class, "menu")] //div[contains(text(), "${projectType}")]`;
            browser.clickOnElement(elements.importFromDropDown);
            browser.useXpath();
            browser.clickOnElement(option);
            browser.useCss();
            browser.clickOnElement(elements.importButton);
            browser.clickOnElement(elements.chooseBoardDropDown);
            option = `//div[contains(@class, "menu")] //div[contains(text(), "${boardName}")]`;
            browser.useXpath();
            browser.clickOnElement(option);
            browser.pause(1000);
            myBrowser.clickOnElement(scope);
            browser.pause(1000);
            option = `//div[contains(@class, "menu")] //div[contains(text(), "${ticketName}")]`;
            browser.clickOnElement(elements.selectTicketsDropDown);
            browser.clickOnElement(option);
            browser.useCss();
            browser.pause(1000);
            browser.clickOnElement(elements.syncButton);
            browser.useXpath();
            browser.expect.element(spinner).to.not.be.present.before(10000);
            browser.pause(1000);
        },

        async dragJiraTicketToMap(ticketName, endActivityId, dragOffsetX, dragFirstTicket) {
            browser = this.api;
            elements = this.elements;
            const dragHandler = `//div[contains(text(), "${ticketName}")]/../../../../..//div[@class = "drag-handler"]`;
            const dropActivity = `#${endActivityId}`;
            const activityPlaceHolder = '//*[name() = "g" and contains(@id, "activity-place-holder")]';
            const activitySize = await browser.getElementSize('css selector', dropActivity);
            const width = Math.floor(activitySize.value.width);
            if (dragFirstTicket === true) {
                await browser.useCss();
                await browser.waitForElementVisible(elements.firstJiraTicketDragHandler);
                await browser.pause(1000);
                await browser.moveToElement('css selector', elements.firstJiraTicketDragHandler, 1, 1);
            } else {
                await browser.useXpath();
                await browser.waitForElementVisible(dragHandler);
                await browser.pause(1000);
                await browser.moveToElement('xpath', dragHandler, 1, 1);
            }
            await browser.mouseButtonDown(0);
            await browser.pause(500);
            await browser.useCss();
            // workaround for moving activities, needs to moveToElement() to the same element TWICE with different pixels
            await browser.moveToElement('css selector', dropActivity, width, 2);
            await browser.moveToElement('css selector', dropActivity, width + dragOffsetX, 3);
            await browser.useXpath();
            await browser.expect.element(activityPlaceHolder).to.be.present.before(10000);
            await browser.useCss();
            await browser.mouseButtonUp(0);
        }
    }]
};
