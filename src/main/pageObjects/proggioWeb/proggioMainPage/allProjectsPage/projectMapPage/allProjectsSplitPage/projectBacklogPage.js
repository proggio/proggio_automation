module.exports = {
    elements: {
        // Blank
        addTaskButton: {
            selector: 'div[id = "Add Task To this Project_button"]'
        },
        newTaskText: {
            selector: 'input[id = "new_task_label"]'
        },
        confirmTaskLabelButton: {
            selector: 'i[id = "new_task_list_icon_fas fa-check-circle"]'
        },
        lastTaskText: {
            selector: 'div[class*= "Table__Grid"] div[role = "row"]:last-child div[class = "label-div"]'
        },
        selectAllCheckBox: {
            selector: 'div[title = "Select All"]'
        },
        deleteMyTask: {
            selector: 'i[id = "delete_bulk_icon"]'
        },
        lastTaskCell: {
            selector: 'div[class*= "Table__Grid"] div[role = "row"]:last-child'
        },

        // Popup Confirm Deletion
        confirmDeleteTask: {
            selector: 'div[id = "popoverOkButton"]'
        },
        cancelDeleteTask: {
            selector: 'div[id = "popoverCancelButton"]'
        }
    },

    commands: [{}]
};
