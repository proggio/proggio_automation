let browser, elements;
module.exports = {
    elements: {
        // Blank
        blankButton: {
            selector: 'div[id = "Blank_button"]'
        },
        blankInput: {
            selector: 'div[class = "ui-form"] input[type = "text"]'
        },
        createProject: {
            selector: 'div[id = "Create Sub-Project_button"]'
        },

        // From Existing Project
        fromExistingProjectButton: {
            selector: 'div[id = "From Existing Project_button"]'
        },
        selectProjectDropDown: {
            selector: 'div[class = " css-dyr6gj-container"]'
        },

        // From Template
        fromTemplateButton: {
            selector: 'div[id = "From Template_button"]'
        },

        gridSplitProject: {
            selector: '//*[name() = "svg" and @id = "grid_split_project"]',
            locateStrategy: 'xpath'
        }
    },

    commands: [{
        selectProjectFromExisting: async function(projectName) {
            browser = this.api;
            elements = this.elements;
            let options = `//div[contains(@class, "menu")]//div[contains(text(), "${projectName}")]`;
            await browser.clickOnElement(elements.selectProjectDropDown);
            await browser.useXpath();
            await browser.clickOnElement(options);
            await browser.clickOnElement(elements.createProject);
        }
    }]
};
