let browser, elements;
module.exports = {
    elements: {
        buffer: {
            selector: '//*[name() = "g" and contains(@id, "map_activity_id")]//*[name() = "rect" and contains(@style, "stripes")]',
            locateStrategy: 'xpath'
        },
        bufferFocused: {
            selector: '//*[name() = "g" and contains(@id, "map_activity_id")]//*[name() = "rect" and contains(@style, "selected-stripes")]',
            locateStrategy: 'xpath'
        },

        // Buffer Menu
        info: {
            selector: 'tr[id*= "id_Info_buffer"]'
        },
        copy: {
            selector: 'tr[id*= "id_Copy_buffer"]'
        },
        cut: {
            selector: 'tr[id*= "id_Cut_buffer"]'
        },
        watch: {
            selector: 'tr[id*= "id_Watch_buffer"]'
        },
        recurring: {
            selector: 'tr[id*= "id_Recurring_buffer"]'
        },
        delete: {
            selector: 'tr[id = "trash_button"]'
        },

        bufferSaveButton: {
            selector: 'div[id = "modal_button_Buffer Settings_Save"]'
        },

        // Buffer Settings
        bufferNameInput: {
            selector: 'input[id = "buffer_name"]'
        },
        deleteBufferButton: {
            selector: 'div[id = "modal_button_Buffer Settings_Delete Buffer"]'
        },
        confirmBufferDelete: {
            selector: 'div[id = "popoverOkButton"]'
        },

        // Date Picker
        activityStartDate: {
            selector: 'div[title = "Choose start date"]'
        },
        activityEndDate: {
            selector: 'div[title = "Choose end date"]'
        },
        startDateContainerText: {
            selector: 'div[title= "Choose start date"] div[class = "ellipsis"]'
        },
        endDateContainerText: {
            selector: 'div[title= "Choose end date"] div[class = "ellipsis"]'
        }

    },
    commands: [{
        saveBufferSettings() {
            browser = this.api;
            elements = this.elements;
            browser.clickOnElement(elements.bufferSaveButton);
        },

        deleteBuffer() {
            browser = this.api;
            elements = this.elements;
            browser.clickOnElement(elements.deleteBufferButton);
            browser.clickOnElement(elements.confirmBufferDelete);
        },

        async openBufferSettings(myBrowser, activityId, option) {
            browser = this.api;
            elements = this.elements;
            const activity = `#${activityId}`;
            const activitySize = await browser.getElementSize('css selector', activity);
            const activityWidth = Math.floor(activitySize.value.width / 2);
            await browser.moveToElement('css selector', activity, activityWidth, 1);
            await browser.mouseButtonClick(2);
            await myBrowser.clickOnElement(option);
        },

        bufferModalDatePicker(datePickerOption, year, startDateDay, endDateDay) {
            browser = this.api;
            elements = this.elements;
            browser.useCss();
            if (datePickerOption === 'start date') {
                const start = `div[class = "DayPickerInput-Overlay"] div[aria-label*= "${startDateDay} ${year}"]`;
                const end = `div[class = "DayPickerInput-Overlay"] div[aria-label*= "${endDateDay} ${year}"]`;
                browser.clickOnElement(elements.activityStartDate);
                browser.clickOnElement(start);
                browser.clickOnElement(end);
            } else {
                const dates = `div[class = "DayPickerInput-Overlay"] div[aria-label*= "${endDateDay} ${year}"]`;
                browser.clickOnElement(elements.activityEndDate);
                browser.clickOnElement(dates);
            }
        }
    }]
};
