let browser, elements;
module.exports = {
    elements: {
        addNewWorkStreamButton: {
            selector: 'i[id = "add_item"]'
        },
        addNewWorkStreamHidden: {
            selector: 'div[id = "add_item"][style*= "hidden"]'
        },
        workStreamList: {
            selector: 'div[style^= "z-index"] div[class^= "list-item"]'
        },
        lastWorkStreamCard: {
            selector: 'div[style^= "z-index"]:last-child div[class = "text"]'
        },
        firstWorkStreamCard: {
            selector: 'div[style^= "z-index"]:first-child div[class = "text"]'
        },
        lastWorkStreamCardText: {
            selector: 'div[style^= "z-index"]:last-child input'
        },
        workStreamMenuButton: {
            selector: 'div[style^= "z-index"]:last-child i[id^= "menu_item"]'
        },
        addNewWorkStreamBottom: {
            selector: 'div[id = "bottomAddWorkstream"]'
        },
        defaultWorkStreamCard: {
            selector: '//div[contains(@style, "z-index")]//input[contains(@value, "Workstream")]',
            locateStrategy: 'xpath'
        },
        copyWorkStreamButton: {
            selector: 'div[id = "modal_button_Copy workstream_Copy"]'
        },
        duplicateWorkStreamButton: {
            selector: 'div[id = "modal_button_Duplicate workstream_Duplicate"]'
        },

        // WorkStream Menu
        settings: {
            selector: 'tr[id = "id_Settings_workstream"]'
        },
        add: {
            selector: 'tr[id*= "id_Add"]'
        },
        duplicate: {
            selector: 'tr[id = "id_Duplicate_workstream"]'
        },
        copy: {
            selector: 'tr[id = "id_Copy_workstream"]'
        },
        paste: {
            selector: 'tr[id = "id_Paste_workstream"]'
        },
        collaboration: {
            selector: 'tr[id = "id_Collaboration_workstream"]'
        },
        hideMe: {
            selector: 'tr[id = "id_Hide Me_workstream"]'
        },
        createSeparator: {
            selector: 'tr[id = "id_Create Separator_workstream"]'
        },
        delete: {
            selector: 'tr[id = "id_Delete_workstream"]'
        },

        // WorkStream Settings
        assigneeContainerText: {
            selector: 'div[id= "assigneeList"] div[class*= "singleValue"]'
        },
        closeSettings: {
            selector: 'div[class*= "fesXi"] i[id = "closeModalButton"]'
        },

        // PopOver Top
        confirmDeleteButton: {
            selector: 'div[id = "popoverOkButton"]'
        },
        cancelButton: {
            selector: 'div[id = "popoverCancelButton"]'
        }
    },

    commands: [{

        getWorkStreamTitle(workStreamName) {
            return `//div[contains(@style, "z-index")]//div[contains(@title, "${workStreamName}")]`;
        },

        async getWorkStreamYAxisByName(workStreamTitle) {
            browser = this.api;
            browser.useXpath();
            const workStream = `//div[contains(@title, "${workStreamTitle}")]/../../../div[contains(@class, "list-item")]`;
            await browser.waitForElementVisible(workStream);
            const result = await browser.getLocation('xpath', workStream);
            return result.value.y;
        },

        openWorkStreamSettings(myBrowser, workStreamName, option) {
            browser = this.api;
            const workStreamElem = `div[class*= "list-item"] div[title = "${workStreamName}"]`;
            browser.moveToElement('css selector', workStreamElem, 1, 1);
            browser.mouseButtonClick(2);
            myBrowser.clickOnElement(option);
        },

        addNewWorkStream(workStreamName) {
            browser = this.api;
            elements = this.elements;
            browser.clickOnElement(elements.addNewWorkStreamButton);
            browser.setValueInElement(elements.lastWorkStreamCardText, workStreamName);
            browser.keys([browser.Keys.ENTER]);
        },

        deleteWorkStream(workStreamName) {
            browser = this.api;
            elements = this.elements;
            const workStream = `//div[contains(@style, "z-index")]//div[contains(@title, "${workStreamName}")]`;
            browser.moveToElement('xpath', workStream, 1, 1);
            browser.mouseButtonClick(2);
            browser.clickOnElement(elements.delete);
            browser.clickOnElement(elements.confirmDeleteButton);
            browser.pause(1000);
        },

        async moveMousePointer(dropWorkStreamTitle) {
            browser = this.api;
            await browser.useXpath();
            const dropWorkStream = `//div[contains(@title, "${dropWorkStreamTitle}")]/../../../div[contains(@class, "list-item")]`;
            await browser.waitForElementVisible(dropWorkStream);
            await browser.moveToElement('xpath', dropWorkStream, 1, 1);
            await browser.pause(500);
        },

        async moveWorkStreamByName(dragWorkStreamTitle, dropWorkStreamTitle) {
            browser = this.api;
            await browser.useXpath();
            const dragWorkStream = `//div[contains(@title, "${dragWorkStreamTitle}")]/../../../div[contains(@class, "list-item")]`;
            const dropWorkStream = `//div[contains(@title, "${dropWorkStreamTitle}")]/../../../div[contains(@class, "list-item")]`;
            await browser.waitForElementVisible(dragWorkStream);
            await browser.moveToElement('xpath', dragWorkStream, 1, 1);
            await browser.mouseButtonDown(0);
            await browser.moveToElement('xpath', dropWorkStream, 1, 1);
            await browser.mouseButtonUp(0);
            await browser.pause(1000);
            // Move Mouse Pointer
            await this.moveMousePointer(dropWorkStreamTitle);
        },

        // TODO - Find a way to scroll mouse while holding a button( .execute() - possibly )
        async resizeWorkStream(workStreamName, resizePixel) {
            browser = this.api;
            elements = this.elements;
            const workStream = await this.getWorkStreamTitle(workStreamName);
            browser.useXpath();
            browser.moveToElement('xpath', workStream, 1, 1);
            browser.keys(browser.Keys.CONTROL);
            browser.mouseButtonDown(1);
            browser.moveToElement('xpath', workStream, 1, resizePixel);
            browser.mouseButtonUp(1);
            browser.keys(browser.Keys.NULL);
        },

        assignWorkStream(userName) {
            browser = this.api;
            elements = this.elements;
            const dropDownUsers = `//div[contains(@id, "react-select") and contains(text(), "${userName}")]`;
            browser.useCss();
            browser.clickOnElement(elements.assigneeContainerText);
            browser.useXpath();
            browser.clickOnElement(dropDownUsers);
        },

        closeWorkStreamSettings() {
            browser = this.api;
            elements = this.elements;
            browser.clickOnElement(elements.closeSettings);
        }
    }]
};
