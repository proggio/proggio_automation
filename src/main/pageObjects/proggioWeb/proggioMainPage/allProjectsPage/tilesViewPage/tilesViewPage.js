const globalConfig = require('../../../../../utilities/globals');
let elements, browser;
let projectName = globalConfig.automation_testing_account.split('@', 1);
module.exports = {
    elements: {
        allProjects: {
            selector: 'td[id = "link-section-all-projects"] i'
        },
        // Project Settings Options
        projectsList: {
            selector: 'div[class^= "projects"]'
        },
        projectCardList: {
            selector: 'div[class^= "projects"] div[class = "box normal"]'
        },
        lastProjectCard: {
            selector: 'div[class^= "projects"] div[class = "box normal"]:nth-child(2)'
        },
        lastProjectCardText: {
            selector: 'div[class^= "projects"] div[class = "box normal"] div[class = "label-project"]'
        },
        lastProjectSettingsButton: {
            selector: 'div[class^= "projects"] div[class = "box normal"] div[class = "project-info-reveal-on-hover"]'
        },
        newProjectButton: {
            selector: 'div[id = "My Projects"] div[id = "+ New project_button"]'
        },
        projectCardByName: {
            selector: `div[class = "box normal"][id = "${projectName}"]`
        },
        projectCardByNameSettings: {
            selector: `div[class = "box normal"][id = "${projectName}"] div[class = "project-info-reveal-on-hover"]`
        },
        projectCardByNameArchivedText: {
            selector: `div[class = "box normal"][id = "${projectName}"] span span`
        },

        /** Create from template or import **/
        createTemplateCollapse: {
            selector: 'div[id = "CREATE_OR_IMPORT"]'
        },
        archivedProjectsCollapse: {
            selector: 'div[id = "projects_content_area_title"]'
        },
        // Template - Options
        applicationDevelopment: {
            selector: 'div[id = "Application Development"]'
        },
        electricDevice: {
            selector: 'div[id = "Electronic Device"]'
        },
        eventManagement: {
            selector: 'div[id = "Event Management"]'
        },
        hrCompanyEvent: {
            selector: 'div[id = "HR: Company event"]'
        },
        manufacturing: {
            selector: 'div[id = "Manufacturing"]'
        },
        phase1Biotech: {
            selector: 'div[id = "Phase 1 Biotech Template"]'
        },
        theRoyalWedding: {
            selector: 'div[id = "The Royal Wedding"]'
        },
        vendorSelection: {
            selector: 'div[id = "Vendor selection"]'
        },
        websiteDevelopment: {
            selector: 'div[id = "Website development"]'
        },
        importProjectFromFile: {
            selector: 'div[id = "Import Project from File"]'
        },

        /** Project Menu **/
        settings: {
            selector: 'tr[id*="Settings_project"]'
        },
        taskList: {
            selector: 'tr[id*="list_project"]'
        },
        taskBoard: {
            selector: 'tr[id*="board_project"]'
        },
        budget: {
            selector: 'tr[id*="Budget_project"]'
        },
        duplication: {
            selector: 'tr[id = "id_Duplication_project"]'
        },
        export: {
            selector: 'tr[id = "id_Export_project"]'
        },
        archive: {
            selector: 'tr[id = "id_Archive_project"]'
        },
        unArchive: {
            selector: 'tr[id = "id_Unarchive_archivedProject"]'
        },
        createTemplate: {
            selector: 'tr[id*="Template_project"]'
        },
        delete: {
            selector: 'tr[id = "id_Delete_project"]'
        },
        confirmDelete: {
            selector: 'div[id = "modal_button_Delete_Delete"]'
        },

        /** Project Settings **/
        assigneeContainerText: {
            selector: 'div[id= "assigneeList"] div[class*= "singleValue"]'
        },
        closeSettings: {
            selector: 'div[class*= "fesXi"] i[id = "closeModalButton"]'
        },

        /** Create Project - Popup Card **/
        textInput: {
            selector: 'div[class*= "modal__Card"] input[type = "text"]'
        },
        openProjectCheckBox: {
            selector: 'input[id = "ProjectMap"]'
        },
        cancelButton: {
            selector: 'div[id = "Cancel_button"]'
        },
        duplicateButton: {
            selector: 'div[id = "modal_button_Duplicate_Duplicate"]'
        },
        closeButton: {
            selector: 'div[class*= "hZhaGx"] i[class*= "CloseIcon"]'
        },
        dragButton: {
            selector: 'div[class*= "hZhaGx"] i[class*= "DragIcon"]'
        },
        projectStart: {
            selector: 'input[id= "start"]'
        },
        projectEnd: {
            selector: 'input[id= "end"]'
        },
        datePicker: {
            selector: 'div[id= "date_picker"]'
        },
        createProject: {
            selector: 'div[id = "modal_button_Create Project _Create Project"]'
        },
        createProjectTemplate: {
            selector: 'div[id = "modal_button_Create Project from Template_Create Project"]'
        },
        projectNameInput: {
            selector: 'div[class*= "modal__Card"] input[type = "text"]'
        },
        openProjectMapCheckBox: {
            selector: 'input[id= "ProjectMap"]'
        },
        deleteProject: {
            selector: 'div[id = "modal_button_Delete_Delete"]'
        },
        alertDarkCloseButton: {
            selector: 'div[class*="alert dark"] div[class = "close-icon"]'
        },
        createProjectProgress: {
            selector: 'path[class = "create-progress-path"]'
        },
        tilesView: {
            selector: 'div[class*= "projects-toolbar"] li[id*= "Tiles"]'
        }
    },
    commands: [{

        getProjectName(projectName) {
            return `div[id = "${projectName}"] div[class*= "label-project"]`;
        },

        getProjectId(projectId) {
            return `div[id = "${projectId}"]`;
        },

        createNewProject(projectName, openProjectMap) {
            browser = this.api;
            elements = this.elements;
            browser.clickOnElement(elements.newProjectButton);
            browser.clearValue('css selector', elements.projectNameInput);
            browser.setValueInElement(elements.projectNameInput, projectName);
            // TODO - No indication if checkbox has been selected
            if (openProjectMap === true) {
                browser.clickOnElement(elements.openProjectCheckBox);
                browser.clickOnElement(elements.createProject);
                browser.expect.element(elements.createProjectProgress).to.be.present.before(globalConfig.timeOut);
                browser.expect.element(elements.createProjectProgress).to.not.be.present.before(globalConfig.timeOut);
            } else {
                browser.clickOnElement(elements.createProject);
            }
        },

        async deleteMyProject(projectName) {
            browser = this.api;
            elements = this.elements;
            const project = `div[id = "${projectName}"]`;
            browser.useCss();
            await browser.waitForElementVisible(elements.projectsList);
            browser.isElementPresent('css selector', project, async() => {
                const size = await browser.getElementSize('css selector', project);
                const height = size.value.height;
                const width = size.value.width;
                browser.moveToElement('css selector', project, width / 2, height / 2);
                browser.mouseButtonClick(2);
                browser.clickOnElement(elements.delete);
                browser.clickOnElement(elements.confirmDelete);
                browser.clickOnElement('div[class*="alert dark"] div[class = "close-icon"]');
                browser.pause(1000);
            });
        },

        async deleteAllProjects() {
            browser = this.api;
            elements = this.elements;
            await browser.clickOnElement(elements.allProjects);
            await browser.clickOnElement(elements.tilesView);
            await browser.waitForElementVisible(elements.projectsList);
            const result = await browser.elements('css selector', elements.projectCardList);
            if (result.status !== -1) {
                for (let i = 1; i <= result.value.length; i++) {
                    await browser.clickOnElement(elements.lastProjectSettingsButton);
                    await browser.pause(500);
                    await browser.clickOnElement(elements.delete);
                    await browser.clickOnElement(elements.deleteProject);
                    await browser.clickOnElement(elements.alertDarkCloseButton);
                    await browser.pause(1500);
                }
            }
        },

        openProjectMenu(projectName) {
            browser = this.api;
            const project = `div[id = "${projectName}"]`;
            browser.useCss();
            browser.moveToElement(`css selector`, project, 1, 1);
            browser.mouseButtonClick(2);
        },

        openProjectSettings(projectName, option) {
            browser = this.api;
            elements = this.elements;
            browser.useCss();
            const projectSettings = `div[id = "${projectName}"] div[class = "project-info-reveal-on-hover"]`;
            const settingsOptions = `tr[id*= "${option}"]`;
            browser.pause(500);
            browser.clickOnElement(projectSettings);
            browser.clickOnElement(settingsOptions);
        },

        openProject(projectName) {
            browser = this.api;
            const project = `div[class = "box normal"][id = "${projectName}"]`;
            browser.useCss();
            browser.clickOnElement(project);
        },

        verifyProjectPresent(projectName) {
            browser = this.api;
            const project = `div[class = "box normal"][id = "${projectName}"]`;
            browser.useCss();
            browser.waitForElementVisible(project);
            browser.expect.element(project).to.be.present.before(globalConfig.timeOut);
        },

        enterToProject(projectName) {
            browser = this.api;
            const project = `div[id = "${projectName}"]`;
            browser.useCss();
            browser.clickOnElement(project);
            console.log(`Entering to project: "${project}"`);
        },

        assignProject(userName) {
            browser = this.api;
            elements = this.elements;
            const dropDownUsers = `//div[contains(@id, "react-select") and contains(text(), "${userName}")]`;
            browser.useCss();
            browser.clickOnElement(elements.assigneeContainerText);
            browser.useXpath();
            browser.clickOnElement(dropDownUsers);
        },

        closeProjectSettings() {
            browser = this.api;
            elements = this.elements;
            browser.clickOnElement(elements.closeSettings);
        }
    }]
};
