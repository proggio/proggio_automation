let browser, elements;
const globalConfig = require('../../../../utilities/globals');
module.exports = {
    elements: {
        projectsTableGrid: {
            selector: 'div[class*= "Table__Grid"]'
        },
        projectsList: {
            selector: 'div[class*= "innerScrollContainer"]'
        },
        lastProjectCard: {
            selector: 'div[class*= "innerScrollContainer"] div[role = "row"]:first-child'
        },
        lastProjectText: {
            selector: 'div[class*= "row"]:first-child div[class*= "rowColumn"]:nth-child(2) div[class = "ellipsis"]'
        },
        columnButton: {
            selector: 'div[id = "columns-button"]'
        },
        columnBody: {
            selector: 'div[class = "popover open bottom"]'
        },
        okColumnButton: {
            selector: 'div[id = "sort-and-filter-menu-ok-button"]'
        },
        projectHealthColumn: {
            selector: 'div[role = "columnheader"] div[title*= "Health"]'
        },

        // All Project
        listViewAllProjectsView: {
            selector: 'div[id = "tab_All Projects"]'
        },

        // Create Project Modal
        duplicateButton: {
            selector: 'div[id = "modal_button_Duplicate_Duplicate"]'
        },
        openProjectCheckBox: {
            selector: 'input[id = "ProjectMap"]'
        },
        confirmDelete: {
            selector: 'div[id = "modal_button_Delete_Delete"]'
        },
        deleteTemplateButton: {
            selector: 'div[id = "id_Delete_teamTemplate"]'
        },
        alertDarkCloseButton: {
            selector: 'div[class*="alert dark"] div[class = "close-icon"]'
        },

        archivedProjectsCollapse: {
            selector: 'div[id = "projects_content_area_title"]'
        },
        templateProjectsCollapse: {
            selector: 'div[id = "private_templates_area_title"]'
        },

        // Project Template
        projectTemplate: {
            selector: 'div[id = "private_templates_area_title"]'
        },
        projectTemplateCollapse: {
            selector: 'div[id = "private_templates_area_title"] div[class = "project-collapse"] i'
        },
        projectTemplateCollapsed: {
            selector: 'div[id = "private_templates_area_title"] div[class = "project-collapse collapsed"] i'
        },

        /** Project Settings **/
        assigneeContainerText: {
            selector: 'div[id= "assigneeList"] div[class*= "singleValue"]'
        }
    },

    commands: [{

        getProjectLifeCycleStatusElement(projectName) {
            browser = this.api;
            return `//div[@title = "${projectName}"]/../../..//div[contains(@class, "flex horizontal")]/div[@class = "ellipsis"]`;
        },

        getProjectName(projectName) {
            browser = this.api;
            return `//div[@title = "${projectName}"]`;
        },

        async getColumnsNameList() {
            browser = this.api;
            elements = this.elements;
            let columnsList = [];
            const list = 'div[class = "ui-form flex vertical full height"] div[style*= "overflow"] div[style*= "z-index"]';
            browser.useCss();
            browser.clickOnElement(elements.columnButton);
            browser.waitForElementVisible(elements.columnBody);
            await browser.elements('css selector', list, async(results) => {
                const list = results.value.length;
                for (let i = 0; i < list; i++) {
                    const column = `div[style*= "z-index"]:nth-child(${i + 1}) div[class*= "label-div"]`;
                    const columnTitle = await browser.getAttribute('css selector', column, 'title');
                    await columnsList.push(columnTitle.value);
                }
            });
            return columnsList;
        },

        async getColumnsNameTableList() {
            browser = this.api;
            elements = this.elements;
            let columnsTableList = [];
            const tableList = 'div[class = "ReactVirtualized__Table__headerRow"] div[aria-label]';
            browser.useCss();
            browser.waitForElementVisible(tableList);
            await browser.elements('css selector', tableList, async(results) => {
                const list = results.value.length;
                for (let i = 0; i < list; i++) {
                    const column = `div[class = "ReactVirtualized__Table__headerRow"] div[aria-label]:nth-child(${i + 2})`;
                    const columnTableTitle = await browser.getAttribute('css selector', column, 'aria-label');
                    const columnTableTitleToUpper = columnTableTitle.value;
                    await columnsTableList.push(columnTableTitleToUpper.toUpperCase());
                }
            });
            return columnsTableList;
        },

        getColumnCheckBoxStatus(columnName, checkBoxStatus) {
            if (checkBoxStatus === true) {
                return `//div[@title = "${columnName}"]/../../..//div[@id = "done"]//div[contains(@class , "check-div")]`;
            } else {
                return `//div[@title = "${columnName}"]/../../..//div[@id = "not-done"]//div[contains(@class , "check-div")]`;
            }
        },

        toggleAddAllColumns(addColumn) {
            browser = this.api;
            elements = this.elements;
            const list = 'div[class = "ui-form flex vertical full height"] div[style*= "overflow"] div[style*= "z-index"]';
            let columnCheckBox = `div[id = "not-done"] div[class*= "check-div"]`;
            browser.useCss();
            browser.clickOnElement(elements.columnButton);
            browser.waitForElementVisible(elements.columnBody);
            browser.elements('css selector', list, results => {
                const list = results.value.length;
                for (let i = 0; i < list; i++) {
                    browser.element('css selector', columnCheckBox, results => {
                        const isTrue = results.status === 0;
                        if (isTrue && addColumn === 'check') {
                            browser.clickOnElement(columnCheckBox);
                        } else {
                            columnCheckBox = `div[style*= "z-index"]:nth-child(${i + 1}) div[id = "done"] div[class*= "check-div"]`;
                            browser.element('css selector', columnCheckBox, results => {
                                const isTrue = results.status === 0;
                                if (isTrue && addColumn === 'uncheck') {
                                    browser.clickOnElement(columnCheckBox);
                                }
                            });
                        }
                    });
                }
            });
            browser.useCss();
            browser.clickOnElement(elements.okColumnButton);
        },

        toggleAddColumn(columnName, checkColumn = 'none') {
            browser = this.api;
            elements = this.elements;
            let columnCheckBox = `//div[@title = "${columnName}"]/../../..//div[@id = "not-done"]//div[contains(@class , "check-div")]`;
            browser.useCss();
            browser.clickOnElement(elements.columnButton);
            browser.waitForElementVisible(elements.columnBody);
            browser.useXpath();
            browser.element('xpath', columnCheckBox, results => {
                const isTrue = results.status === 0;
                if (isTrue && checkColumn === 'check') {
                    browser.clickOnElement(columnCheckBox);
                } else if (isTrue && checkColumn === 'uncheck') {
                    columnCheckBox = `//div[@title = "${columnName}"]/../../..//div[@id = "done"]//div[contains(@class , "check-div")]`;
                    browser.clickOnElement(columnCheckBox);
                }
            });
            browser.useCss();
            browser.clickOnElement(elements.okColumnButton);
        },

        openProjectByName(projectName) {
            browser = this.api;
            elements = this.elements;
            const projectTitle = `div[class*= "Grid"] div[title = "${projectName}"]`;
            browser.useCss();
            browser.clickOnElement(projectTitle);
        },

        openProjectByButton(projectName) {
            browser = this.api;
            elements = this.elements;
            const projectButton = `//div[@title = "${projectName}"]/../../..//i[contains(@id, "external-link")]`;
            browser.useXpath();
            browser.clickOnElement(projectButton);
        },

        openProjectSettings(projectName, option) {
            browser = this.api;
            elements = this.elements;
            const projectSettings = `//div[@title = "${projectName}"]/../../..//i[contains(@id, "new_task_list_icon_fal")]`;
            const settingsOptions = `tr[id*= "${option}"]`;
            browser.useXpath();
            browser.pause(500);
            browser.clickOnElement(projectSettings);
            browser.useCss();
            browser.clickOnElement(settingsOptions);
        },

        openProjectTemplateSettings(projectName, option) {
            browser = this.api;
            elements = this.elements;
            const projectSettings = `//div[@title = "${projectName}"]/../../..//div[contains(@class, "project-info-reveal-on-hover")]`;
            const settingsOptions = `tr[id*= "${option}"]`;
            browser.useXpath();
            browser.clickOnElement(projectSettings);
            browser.useCss();
            browser.clickOnElement(settingsOptions);
        },

        openProjectSettingsByButton(projectName) {
            browser = this.api;
            const settingsButton = `//div[@title = "${projectName}"]/../../..//i[contains(@id, "fa-cog")]`;
            browser.useXpath();
            browser.clickOnElement(settingsButton);
        },

        changeLifeCycleStage(projectName, stage) {
            browser = this.api;
            const lifeCycle = `//div[@title = "${projectName}"]/../../..//div[contains(@class, "flex horizontal")]/div[@class = "ellipsis"]`;
            const stages = `//div[@class =" css-11unzgr"]//div[contains(text(), "${stage}")]`;
            browser.useXpath();
            browser.clickOnElement(lifeCycle);
            browser.clickOnElement(stages);
        },

        changeView(viewName) {
            browser = this.api;
            elements = this.elements;
            const view = `div[id = "tab_${viewName}"]`;
            browser.clickOnElement(view);
        },

        duplicateProject(projectName, openProjectOnDupe) {
            browser = this.api;
            elements = this.elements;
            browser.useXpath();
            this.openProjectSettings(`${projectName}`, `${globalConfig.contextMenu.duplication}`);
            if (openProjectOnDupe === true) {
                browser.clickOnElement(elements.openProjectCheckBox);
                browser.clickOnElement(elements.duplicateButton);
            } else {
                browser.clickOnElement(elements.duplicateButton);
            }
        },

        deleteProject(projectName) {
            browser = this.api;
            elements = this.elements;
            const projectSettings = `//div[@title = "${projectName}"]/../../..//i[contains(@id, "new_task_list_icon_fal")]`;
            browser.useXpath();
            browser.clickOnElement(projectSettings);
            browser.useCss();
            this.openProjectSettings(`${projectName}`, `${globalConfig.contextMenu.delete}`);
            browser.clickOnElement(elements.confirmDelete);
            browser.clickOnElement(elements.alertDarkCloseButton);
            browser.pause(1000);
        },

        deleteProjectTemplate(projectName) {
            browser = this.api;
            elements = this.elements;
            browser.useXpath();
            browser.waitForElementVisible(elements.projectTemplate);
            browser.isElementPresent('xpath', elements.projectTemplateCollapsed, () => {
                browser.clickOnElement(elements.projectTemplateCollapsed);
            });
            browser.useCss();
            this.projectMenuAction(`${projectName}`, `deleteTemplate`);
            browser.pause(1000);
        },

        projectMenuAction(projectName, option) {
            browser = this.api;
            elements = this.elements;
            switch (option) {
                case 'duplication':
                    this.openProjectSettings(`${projectName}`, `${globalConfig.contextMenu.duplication}`);
                    browser.clickOnElement(elements.openProjectCheckBox);
                    browser.clickOnElement(elements.duplicateButton);
                    break;
                case 'archive':
                    this.openProjectSettings(`${projectName}`, `${globalConfig.contextMenu.archive}`);
                    break;
                case 'unArchive':
                    this.openProjectSettings(`${projectName}`, `${globalConfig.contextMenu.unArchive}`);
                    break;
                case 'createTemplate':
                    this.openProjectSettings(`${projectName}`, `${globalConfig.contextMenu.createTemplate}`);
                    break;
                case 'delete':
                    this.openProjectSettings(`${projectName}`, `${globalConfig.contextMenu.delete}`);
                    browser.clickOnElement(elements.confirmDelete);
                    browser.clickOnElement(elements.alertDarkCloseButton);
                    break;
                case 'deleteTemplate':
                    this.openProjectTemplateSettings(`${projectName}`, `${globalConfig.contextMenu.deleteTemplate}`);
                    browser.clickOnElement(elements.confirmDelete);
                    browser.clickOnElement(elements.alertDarkCloseButton);
                    break;
            }
            browser.pause(1000);
        }

    }]
};
