module.exports = {
    elements: {
        // PopOver
        deleteButton: {
            selector: 'div[id = "popoverOkButton"]'
        },
        cancelButton: {
            selector: 'div[id = "popoverCancelButton"]'
        }
    }
};
