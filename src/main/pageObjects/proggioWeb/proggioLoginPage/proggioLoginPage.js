
module.exports = {
    elements: {
        emailInput: {
            selector: '#input-email'
        },
        passwordInput: {
            selector: '#passwordInput'
        },
        conformationPopup: {
            selector: '#hs-eu-confirmation-button'
        },
        loginButton: {
            selector: '#button-login'
        },
        signInWithGoogleButton: {
            selector: 'div[id = "google_signin_button"]'
        },
        signUpHyperLink: {
            selector: 'a[href = "/signup"]'
        },
        signInWithSSOHyperLink: {
            selector: 'a[id = "button-login-sso"]'
        },
        resetPasswordHyperLink: {
            selector: 'a[href = "/reset-request"]'
        },
        signInHyperLink: {
            selector: 'a[href = "/login"]'
        }
    },

    commands: [{

    }]

};
