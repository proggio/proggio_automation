module.exports = {
    elements: {
        emailToResetInput: {
            selector: 'input[id = "emailResetRequest"]'
        },
        resetPasswordButton: {
            selector: 'button[id = "resetPasswordButton"]'
        },
        passwordInput: {
            selector: '#passwordInput'
        },
        newPasswordInput: {
            selector: '#input-password-confirm'
        },
        setNewPasswordButton: {
            selector: '#setNewPasswordButton'
        }
    },

    commands: [{
        setNewPassword(browser, newPassword) {
            browser.setValueInElement('@passwordInput', newPassword);
            browser.setValueInElement('@newPasswordInput', newPassword);
            browser.clickOnElement('@setNewPasswordButton');
        }
    }]
};
