module.exports = {
    elements: {
        linkSentText: {
            selector: '//div[contains(text(), "you a link")]',
            locateStrategy: 'xpath'
        }
    }
};
