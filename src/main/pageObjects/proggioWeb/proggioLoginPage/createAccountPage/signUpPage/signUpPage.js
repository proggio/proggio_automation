let browser, elements;
const globalConfig = require('../../../../../utilities/globals');
module.exports = {
    elements: {
        fullNameInput: {
            selector: 'input[id = "nameRegister"]'
        },
        flagDropDown: {
            selector: 'div[class= " flag-dropdown"]'
        },
        flagDropDownOptions: {
            selector: '//ul[@class = " country-list"]//li',
            locateStrategy: 'xpath'
        },
        mobilePhoneInput: {
            selector: 'input[class = " form-control"]'
        },
        setPasswordInput: {
            selector: 'input[id = "passwordRegister"]'
        },
        workspaceNameInput: {
            selector: 'input[id = "teamRegister"]'
        },
        sampleProjectDropDown: {
            selector: 'div[class = "status-select"]'
        },
        termsOfServiceHyperLink: {
            selector: '//label[@id = "TermsOfService"]/span[contains(text(), "Terms of Service")]',
            locateStrategy: 'xpath'
        },
        privacyPolicyHyperLink: {
            selector: '//label[@id = "TermsOfService"]/span[contains(text(), "Privacy Policy")]',
            locateStrategy: 'xpath'
        },
        startUsingProggioButton: {
            selector: 'button[type = "button"]'
        },
        hrOption: {
            selector: '//div[contains(@class, "css")]/div/div/div[@id = "react-select-2-option-1"]',
            locateStrategy: 'xpath'
        }
    },

    commands: [{
        fillForm(mobileNumber, password, workspaceName) {
            browser = this.api;
            elements = this.elements;
            browser.clickOnElement(elements.fullNameInput);
            browser.sendKeys(
                'css selector',
                'input[id = "nameRegister"]',
                browser.Keys.CONTROL +
                'a' +
                browser.Keys.NULL +
                browser.Keys.DELETE);
            browser.setValueInElement(elements.fullNameInput, globalConfig.dynamic_userName.split('@', 1));
            browser.setValueInElement(elements.mobilePhoneInput, mobileNumber);
            browser.setValueInElement(elements.setPasswordInput, password);
            browser.setValueInElement(elements.workspaceNameInput, workspaceName.split('@', 1));
            browser.clickOnElement(elements.sampleProjectDropDown);
            browser.clickOnElement(elements.hrOption);
            browser.clickOnElement(elements.startUsingProggioButton);
        }
    }]
};
