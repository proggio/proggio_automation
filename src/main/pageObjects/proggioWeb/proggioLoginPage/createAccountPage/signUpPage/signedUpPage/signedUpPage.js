module.exports = {
    elements: {
        signedUpEmailText: {
            selector: '//div[@class = "box"]//div[contains(text(), "@progg.io")]',
            locateStrategy: 'xpath'
        },
        changeEmailHyperLink: {
            selector: '//div[@class = "box"]//a[contains(text(), "Change email")]',
            locateStrategy: 'xpath'
        },
        resendEmailHyperLink: {
            selector: '//div[@class = "box"]//a[contains(text(), "re-send email")]',
            locateStrategy: 'xpath'
        },
        chatHyperLink: {
            selector: '//div[@class = "box"]//span[contains(text(), "chat")]',
            locateStrategy: 'xpath'
        },
        supportHyperLink: {
            selector: '//div[@class = "box"]//a[contains(text(), "support")]',
            locateStrategy: 'xpath'
        }
    }
};
