module.exports = {
    elements: {
        emailInput: {
            selector: 'input[id = "emailRegister"]'
        },
        createAccountButton: {
            selector: 'button[type = "button"]'
        }
    }
};
