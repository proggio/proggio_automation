let proggioMainPage, settingsPage, inviteUserPage;
const globalConfig = require('../../main/utilities/globals');
module.exports = {
    '@tags': ['allProjectsTests', 'settingsTests', 'fullRun', 'addUser'],
    before: function(browser) {
        console.log('Loading element pages...');
        proggioMainPage = browser.page.proggioMainPage();
        settingsPage = browser.page.settingsPage();
        inviteUserPage = browser.page.inviteUserPage();
        browser.login(globalConfig.proggio_username, globalConfig.proggio_password);
    },

    'Step 1: Click Settings': () => {
        proggioMainPage.clickOnElement('@settings');
    },

    'Step 2: Verify User Not Present': async(browser) => {
        const userElement = `//div[@title = "${globalConfig.account_add_user}"]`;
        await browser.areElementsPresent('xpath', userElement, function() {
            settingsPage.removeUser(`${globalConfig.account_add_user}`
            );
        });
    },

    "Step 3: Click 'Invite New User'": async() => {
        settingsPage.clickOnElement('@inviteNewUser');
    },

    'Step 4: Add User': () => {
        inviteUserPage.inviteUser(inviteUserPage,
            `${globalConfig.account_add_user}`,
            '@viewer',
            '@guest');
    },

    'Step 5: Verify User Access Level': (browser) => {
        browser.refresh();
        settingsPage.verifyUserAccessLevel(
            `${globalConfig.account_add_user}`,
            'Viewer'
        );
    },

    'Step 6: Verify User Role': () => {
        settingsPage.verifyUserRole(
            `${globalConfig.account_add_user}`,
            'Guest'
        );
    }
};
