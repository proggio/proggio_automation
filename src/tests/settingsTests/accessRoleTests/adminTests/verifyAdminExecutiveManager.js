let proggioMainPage, settingsPage,
    userProfilePage, taskManagementPage, activityPage,
    tilesViewPage, dashboardPage,
    workStreamPage;
const globalConfig = require('../../../../main/utilities/globals');
const assert = require('assert');
const adminExecutiveManager = 'Admin Executive Manager';
const taskOwnerProject = 'TaskOwner';
const activityName = 'Edit Me!';
const workStreamName = 'New workStream';
let activityId, xStartLocation, xEndLocation, workStreamTitle, projectId;
module.exports = {
    '@tags': ['allProjectsTests', 'settingsTests', 'accessRoleTests', 'adminTests', 'fullRun'],
    before: function(browser) {
        console.log('Loading element pages...');
        proggioMainPage = browser.page.proggioMainPage();
        settingsPage = browser.page.settingsPage();
        userProfilePage = browser.page.userProfilePage();
        taskManagementPage = browser.page.taskManagementPage();
        activityPage = browser.page.activityPage();
        tilesViewPage = browser.page.tilesViewPage();
        dashboardPage = browser.page.dashboardPage();
        workStreamPage = browser.page.workStreamPage();
        browser.login(globalConfig.proggio_username, globalConfig.proggio_password);
        workStreamTitle = workStreamPage.getWorkStreamTitle(workStreamName);
        projectId = tilesViewPage.getProjectId(adminExecutiveManager);
    },

    'Step 1: Click Settings': () => {
        proggioMainPage.clickOnElement('@settings');
    },

    "Step 2: Change User Permission To: 'Admin + Executive Manager'": () => {
        settingsPage.changeAccessAndRole(settingsPage,
            `${globalConfig.account_admin_executive_manager}`,
            `@admin`,
            `@executiveManager`);
    },

    "Step 3: Login With 'Editor + Project Manager' User": (browser) => {
        browser.logout();
        browser.login(
            globalConfig.account_admin_executive_manager,
            globalConfig.generic_password
        );
    },

    'Step 3.1: Delete Previous Project': async(browser) => {
        proggioMainPage.clickOnElement('@allProjects');
        await tilesViewPage.deleteMyProject(adminExecutiveManager);
        browser.expect.element(projectId).to.not.be.present.before(globalConfig.timeOut);
    },

    'Step 4: Verify All Projects Page - Should Work': () => {
        proggioMainPage.clickOnElement('@allProjects');
        proggioMainPage.expect.element('@tilesView').to.be.visible;
    },

    'Step 5: Verify Dashboard Page - Should Work': async(browser) => {
        proggioMainPage.clickOnElement('@dashboard');
        const closePopup = proggioMainPage.elements.closeFeedBack;
        const result = await browser.element('css selector', closePopup);
        if (result.status !== -1) {
            await proggioMainPage.clickOnElement('@closeFeedBack');
        }
        dashboardPage.expect.element('@selectDashboardDropdown').to.be.visible;
    },

    'Step 6: Verify Task Management Page - Should Work': () => {
        proggioMainPage.clickOnElement('@taskManagement');
        taskManagementPage.expect.element('@listView').to.be.visible;
    },

    'Step 7: Verify Settings Page - Should Work': () => {
        proggioMainPage.clickOnElement('@settings');
        settingsPage.expect.element('@inviteNewUser').to.be.visible;
    },

    'Step 8: Verify Portfolio Page - Should Work': () => {
        proggioMainPage.expect.element('@portfolio').to.be.visible;
    },

    'Step 9: Verify Budget Page - Should Work': () => {
        proggioMainPage.expect.element('@budget').to.be.visible;
    },

    'Step 10: Verify Resources Management Page - Should Work': () => {
        proggioMainPage.expect.element('@resourcesManagement').to.be.visible;
    },

    'Step 11: Check Previous Project and Delete': async(browser) => {
        proggioMainPage.clickOnElement('@allProjects');
        const project = `div[class = "box normal"][id = "${adminExecutiveManager}"]`;
        const result = await browser.element('css selector', project);
        if (result.status !== -1) {
            await tilesViewPage.createNewProject(adminExecutiveManager);
        }
    },

    'Step 11.1: Create New Project - Should Work': async() => {
        tilesViewPage.createNewProject(`${adminExecutiveManager}`, true);
    },

    'Step 12: Create Activity - Should Work': async() => {
        await activityPage.createActivity();
    },

    'Step 13: Move Activity - Should Work': async() => {
        const xOffsetNum = 20;
        activityId = await activityPage.getActivityId(activityName);
        console.log(activityId);
        xStartLocation = await activityPage.getActivityXAxisLocation(activityId);
        await activityPage.moveActivityByOffset(activityId, xOffsetNum);
        xEndLocation = await activityPage.getActivityXAxisLocation(activityId);
        assert.strict.notEqual(xStartLocation, xEndLocation);
        // Move Activity Back
        await activityPage.moveActivityByOffset(activityId, -xOffsetNum);
    },

    'Step 14: Delete Activity - Should Work': async() => {
        activityId = await activityPage.getActivityId(activityName);
        await activityPage.openActivitySettingsById(activityPage,
            `${activityId}`,
            '@delete');
    },

    'Step 15: Create WorkStream - Should Work': (browser) => {
        workStreamPage.addNewWorkStream(workStreamName);
        browser.expect.element(workStreamTitle).to.be.present.before(globalConfig.timeOut);
    },

    'Step 16: Delete WorkStream - Should Work': (browser) => {
        workStreamPage.deleteWorkStream(workStreamName);
        browser.expect.element(workStreamTitle).to.not.be.present.before(globalConfig.timeOut);
        proggioMainPage.expect.element('@spinner').to.not.be.present.before(globalConfig.timeOut);
    },

    'Step 17: Delete Others Project - Should work': () => {
        proggioMainPage.allProjectsViewPicker(globalConfig.allProjects.views.tilesView);
        tilesViewPage.openProjectMenu(taskOwnerProject);
        tilesViewPage.expect.element('@delete').to.be.present.before(globalConfig.timeOut);
        userProfilePage.clickOnElement('@userProfileButton');
    }
};
