let settingsPage,
    userProfilePage, taskManagementPage,
    proggioMainPage, activityModalPage;
const globalConfig = require('../../../../main/utilities/globals');
let expectedTask = 'Simple Task';
module.exports = {
    '@tags': ['allProjectsTests', 'settingsTests', 'accessRoleTests', 'viewerTests', 'fullRun'],
    before: function(browser) {
        console.log('Loading element pages...');
        proggioMainPage = browser.page.proggioMainPage();
        settingsPage = browser.page.settingsPage();
        userProfilePage = browser.page.userProfilePage();
        taskManagementPage = browser.page.taskManagementPage();
        activityModalPage = browser.page.activityModalPage();
        browser.login(globalConfig.proggio_username, globalConfig.proggio_password);
    },

    'Step 1: Click Settings': () => {
        proggioMainPage.clickOnElement('@settings');
    },

    "Step 2: Change User Permission To: 'Viewer + Guest'": () => {
        settingsPage.changeAccessAndRole(settingsPage,
            `${globalConfig.account_viewer_guest}`,
            `@viewer`,
            `@guest`
        );
    },

    "Step 3: Login With 'Viewer + Guest' User": (browser) => {
        browser.logout();
        browser.login(
            globalConfig.account_viewer_guest,
            globalConfig.generic_password
        );
    },

    // Should Work Visible/Available
    'Step 4: Verify User Owned Tasks': () => {
        proggioMainPage.expect.element('@spinner').to.not.be.present.before(globalConfig.timeOut);
        taskManagementPage.clickOnElement('@listView');
        taskManagementPage.expect.element('@cellName').text.to.contain(expectedTask);
    },

    // Should NOT Work Be Visible/Available
    'Step 5: Side Menu': () => {
        proggioMainPage.expect.element('@sideMenu').to.not.be.present.before(globalConfig.timeOut);
    },

    'Step 6: Top Right Bell Notification Icon': () => {
        userProfilePage.expect.element('@notificationBell').to.not.be.present.before(globalConfig.timeOut);
    },

    "Step 7: '+ Create Task' Button": () => {
        taskManagementPage.expect.element('@createNewTaskButton').to.not.be.present.before(globalConfig.timeOut);
    },

    "Step 8: 'All Tasks' Tab Option": () => {
        taskManagementPage.expect.element('@allTasksTab').to.not.be.present.before(globalConfig.timeOut);
    },

    'Step 9: Task CheckBox': () => {
        taskManagementPage.expect.element('@cellCheckBox').to.not.be.present.before(globalConfig.timeOut);
    },

    "Step 10: 'Go To Project' Button": () => {
        taskManagementPage.expect.element('@cellGoToProjectButton').to.not.be.present.before(globalConfig.timeOut);
    },

    "Step 11: 'Tools' Top Option": () => {
        taskManagementPage.expect.element('@toolsOption').to.not.be.present.before(globalConfig.timeOut);
    },

    "Step 12: 'View' Top Option": () => {
        taskManagementPage.expect.element('@viewOption').to.not.be.present.before(globalConfig.timeOut);
    },

    'Step 13: Edit Activity Name': () => {
        taskManagementPage.clickOnElement('@cellName');
        taskManagementPage.expect.element('@cellInput').to.not.be.present.before(globalConfig.timeOut);
    },

    'Step 14: Backlog Dropdown Settings': () => {
        taskManagementPage.openStatus(`${expectedTask}`);
        taskManagementPage.expect.element('@cellStatusDropdownMain').to.not.be.present.before(globalConfig.timeOut);
    },

    'Step 15: Priority Dropdown Settings': () => {
        taskManagementPage.expect.element('@cellPriority').to.not.be.present.before(globalConfig.timeOut);
    },

    // Activity/Task Settings - Info
    'Step 16: Open Task Settings': () => {
        let taskName = 'Simple Task';
        taskManagementPage.openSettings(`${taskName}`);
    },

    'Step 17: Edit Task Name From Info Tab': () => {
        activityModalPage.clickOnElement('@activityNameText');
        activityModalPage.expect.element('@activityNameInput').to.not.be.present.before(globalConfig.timeOut);
    },

    'Step 18: Edit Date': () => {
        activityModalPage.clickOnElement('@activityDatePickerDisabled');
        activityModalPage.expect.element('@activityDatePickerPopup').to.not.be.present.before(globalConfig.timeOut);
    },

    'Step 19: Edit Number of Days': () => {
        activityModalPage.clickOnElement('@activityWorkingDays');
        activityModalPage.expect.element('@activityWorkingDaysInput').to.not.be.present.before(globalConfig.timeOut);
    },

    "Step 20: Enter 'Description'": () => {
        activityModalPage.clickOnElement('@activityDescription');
        activityModalPage.expect.element('@activityDescriptionInput').to.not.be.present.before(globalConfig.timeOut);
    },

    "Step 21: Change 'Assignee'": (browser) => {
        const activityAssignee = activityModalPage.elements.activityAssignee;
        browser.moveToElement('css selector', activityAssignee, 1, 1);
        browser.mouseButtonClick(0);
        activityModalPage.expect.element('@activityAssigneeDropdownMenu').to.not.be.present.before(globalConfig.timeOut);
    },

    "Step 22: Add 'Assignee'": () => {
        activityModalPage.expect.element('@activityAddAssignee').to.not.be.visible.before(globalConfig.timeOut);
    },

    "Step 23: Change 'Load'": () => {
        activityModalPage.expect.element('@activityAssigneeLoadDisabled').to.be.present.before(globalConfig.timeOut);
    },

    'Step 24: Change Color Option': () => {
        activityModalPage.expect.element('@colorSection').to.not.be.present.before(globalConfig.timeOut);
    },

    "Step 25: Change 'Risk'": () => {
        activityModalPage.expect.element('@colorRiskDisabled').to.be.present.before(globalConfig.timeOut);
    },

    'Step 26: Add Tags': () => {
        activityModalPage.expect.element('@activityTags').to.not.be.present.before(globalConfig.timeOut);
    },

    "Step 27: '+ Add Task' Button": () => {
        activityModalPage.expect.element('@addTaskButton').to.not.be.present.before(globalConfig.timeOut);
    },

    "Step 28: '+ Add New Task' Highlighted Text": () => {
        activityModalPage.expect.element('@addNewTaskButton').to.not.be.present.before(globalConfig.timeOut);
    },

    "Step 29: 'Delete' Button": () => {
        activityModalPage.expect.element('@deleteButton').to.not.be.present.before(globalConfig.timeOut);
    },

    "Step 30: 'Save' Button": () => {
        activityModalPage.expect.element('@saveButton').to.not.be.present.before(globalConfig.timeOut);
    },

    'Step 31: Close Settings': () => {
        activityModalPage.closeActivitySettings();
    },

    'Step 32: Move To Board View': () => {
        taskManagementPage.clickOnElement('@boardViewToggle');
    },

    'Step 33: Drag Item To Other Column': async(browser) => {
        const taskName = 'Simple Task';
        await taskManagementPage.dragAndDropItem(
            `${globalConfig.taskManagement.backLog}`,
            `${taskName}`,
            `${globalConfig.taskManagement.inReview}`
        );

        const result = await browser.elements('xpath', taskManagementPage.elements.inReviewColumnContainer);
        if (result.value.length === 0) {
            await taskManagementPage.expect.element('@inReviewColumnContainer').to.not.be.present.before(globalConfig.timeOut);
        } else {
            throw new Error('Whoops! Column contains an item');
        }
    }
};
