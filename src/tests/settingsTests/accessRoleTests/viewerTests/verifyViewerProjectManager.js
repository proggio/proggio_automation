let settingsPage, taskManagementPage, activityPage, dashboardPage,
    tilesViewPage, milestonePage, proggioMainPage, activityModalPage;
let activityOriginalX, activityMovedX;
const myActivityID = 'map_activity_id_339024';
const notMyActivityID = 'map_activity_id_339025';
let projectName = 'TaskOwner';
const globalConfig = require('../../../../main/utilities/globals');
module.exports = {
    '@tags': ['allProjectsTests', 'settingsTests', 'accessRoleTests', 'viewerTests', 'fullRun'],
    before: function(browser) {
        console.log('Loading element pages...');

        settingsPage = browser.page.settingsPage();
        proggioMainPage = browser.page.proggioMainPage();
        taskManagementPage = browser.page.taskManagementPage();
        activityPage = browser.page.activityPage();
        milestonePage = browser.page.milestonePage();
        tilesViewPage = browser.page.tilesViewPage();
        dashboardPage = browser.page.dashboardPage();
        activityModalPage = browser.page.activityModalPage();
        browser.login(globalConfig.proggio_username, globalConfig.proggio_password);
    },

    'Step 1: Click Settings': () => {
        proggioMainPage.clickOnElement('@settings');
    },

    "Step 2: Change User Permission To: 'Viewer + Project Manager'": () => {
        settingsPage.changeAccessAndRole(settingsPage,
            `${globalConfig.account_viewer_project_manager}`,
            `@viewer`,
            `@projectManager`);
    },

    "Step 3: Login With 'Viewer + Project Manager' User": (browser) => {
        browser.logout();
        browser.login(
            globalConfig.account_viewer_project_manager,
            globalConfig.generic_password
        );
    },

    'Step 4: Verify All Projects Page - Should Work': () => {
        proggioMainPage.clickOnElement('@allProjects');
        proggioMainPage.expect.element('@tilesView').to.be.visible;
    },

    'Step 5: Verify Dashboard Page - Should Work': async(browser) => {
        proggioMainPage.clickOnElement('@dashboard');
        const closePopup = proggioMainPage.elements.closeFeedBack;
        const result = await browser.element('css selector', closePopup);
        if (result.status !== -1) {
            await proggioMainPage.clickOnElement('@closeFeedBack');
        }
        dashboardPage.expect.element('@selectDashboardDropdown').to.be.visible;
    },

    'Step 6: Verify Task Management Page - Should Work': () => {
        proggioMainPage.clickOnElement('@taskManagement');
        taskManagementPage.expect.element('@listView').to.be.visible;
    },

    'Step 7: Verify Settings Page - Should Work': () => {
        proggioMainPage.clickOnElement('@settings');
        settingsPage.expect.element('@inviteNewUser').to.be.visible;
    },

    'Step 8: Verify Portfolio Page - Should NOT Work': () => {
        proggioMainPage.expect.element('@portfolio').to.not.be.present;
    },

    'Step 9: Verify Budget Page - Should NOT Work': () => {
        proggioMainPage.expect.element('@budget').to.not.be.present;
    },

    'Step 10: Verify Resources Management Page - Should NOT Work': () => {
        proggioMainPage.expect.element('@resourcesManagement').to.not.be.present;
    },

    'Step 11: Verify Project - Should Work': () => {
        proggioMainPage.clickOnElement('@allProjects');
        tilesViewPage.verifyProjectPresent(projectName);
    },

    'Step 12: Enter Project': () => {
        proggioMainPage.clickOnElement('@tilesView');
        tilesViewPage.openProject(projectName);
    },

    'Step 13: His WorkStream - Add Activity - Should NOT Work ': (browser) => {
        activityPage.zoomIntoProject();
        browser.moveToElement('xpath', activityPage.elements.projectGridToday, 0, 35);
        activityPage.expect.element('@activityPlaceHolder').to.not.be.present;
    },

    'Step 14: His WorkStream - Move Activity - Should NOT Work ': async() => {
        activityOriginalX = await activityPage.getActivityXAxisLocation(myActivityID);
        await activityPage.moveActivityById(
            `${myActivityID}`,
            `${notMyActivityID}`,
            1
        );
        activityMovedX = await activityPage.getActivityXAxisLocation(myActivityID);
        if (activityOriginalX === activityMovedX) {
            await console.log(`Activity: ${myActivityID} didn't move successfully`);
        } else {
            throw new Error(`Whoops! Activity: ${myActivityID} moved!`);
        }
    },

    'Step 15: His WorkStream - Add Milestone - Should NOT Work ': (browser) => {
        browser.moveToElement('xpath', activityPage.elements.projectGridToday, 96, 0, function() {
            browser.doubleClick();
        });
        milestonePage.expect.element('@milestoneStrip').to.not.be.present;
    },

    'Step 16: His WorkStream - Change Dates - Should NOT Work ': async() => {
        await activityPage.openActivitySettingDoubleClick('Main Activity');
        await activityModalPage.clickOnElement('@activityDatePickerDisabled');
        await activityModalPage.expect.element('@activityDatePickerPopup').to.not.be.present.before(globalConfig.timeOut);
        await activityModalPage.closeActivitySettings();
    },

    'Step 17: NOT His WorkStream - Add Activity - Should NOT Work ': (browser) => {
        activityPage.zoomIntoProject();
        browser.moveToElement('xpath', activityPage.elements.projectGridToday, 0, 90);
        activityPage.expect.element('@activityPlaceHolder').to.not.be.present;
    },

    'Step 18: NOT His WorkStream - Move Activity - Should NOT Work ': async() => {
        activityOriginalX = await activityPage.getActivityXAxisLocation(notMyActivityID);
        await activityPage.moveActivityById(
            `${notMyActivityID}`,
            `${myActivityID}`,
            1
        );
        activityMovedX = await activityPage.getActivityXAxisLocation(notMyActivityID);
        if (activityOriginalX === activityMovedX) {
            await console.log(`Activity: ${myActivityID} didn't move successfully`);
        } else {
            throw new Error(`Whoops! Activity: ${myActivityID} moved!`);
        }
    },

    'Step 19: NOT His WorkStream - Change Dates - Should NOT Work ': async() => {
        await activityPage.openActivitySettingDoubleClick('Not My Activity');
        await activityModalPage.clickOnElement('@activityDatePickerDisabled');
        await activityModalPage.expect.element('@activityDatePickerPopup').to.not.be.present.before(globalConfig.timeOut);
        await activityModalPage.closeActivitySettings();
    }
};
