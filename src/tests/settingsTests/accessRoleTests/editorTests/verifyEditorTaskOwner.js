const globalConfig = require('../../../../main/utilities/globals');
const assert = require('assert');
let proggioMainPage, settingsPage, taskManagementPage, activityPage, tilesViewPage,
    milestonePage, dashboardPage, workStreamPage, activityModalPage;
let xStartLocation, xEndLocation;
const activity2Id = 'map_activity_id_341613';
const activity3Id = 'map_activity_id_341073';
const activity4Id = 'map_activity_id_341220';
const activity5Id = 'map_activity_id_341223';
const activity3Name = 'Activity3';
const activity4Name = 'Activity4';
const anchorA2StartId = 'map_activity_id_341222';
const anchorA3StartId = 'map_activity_id_341435';
const taskName = 'My Task';
const activityName = 'Check';
const activityConnectedName = 'Activity3 (Workstream 2) -> Activity2 (Workstream 3)';
const projectName = 'Editor Permissions';

module.exports = {
    '@tags': ['allProjectsTests', 'settingsTests', 'accessRoleTests', 'editorTests', 'fullRun', 'verifyEditorTaskOwner'],
    before: function(browser) {
        console.log('Loading element pages...');
        proggioMainPage = browser.page.proggioMainPage();
        settingsPage = browser.page.settingsPage();
        tilesViewPage = browser.page.tilesViewPage();
        taskManagementPage = browser.page.taskManagementPage();
        activityPage = browser.page.activityPage();
        milestonePage = browser.page.milestonePage();
        dashboardPage = browser.page.dashboardPage();
        workStreamPage = browser.page.workStreamPage();
        activityModalPage = browser.page.activityModalPage();
        browser.login(globalConfig.proggio_username, globalConfig.proggio_password);
    },

    'Step 1: Click Settings': () => {
        proggioMainPage.clickOnElement('@settings');
    },

    "Step 2: Change User Permission To: 'Editor + Task Owner'": () => {
        settingsPage.changeAccessAndRole(settingsPage,
            `${globalConfig.account_editor_task_owner}`,
            `@editor`,
            `@taskOwner`
        );
    },

    "Step 3: Login With 'Editor + Task Owner' User": (browser) => {
        browser.logout();
        browser.login(
            globalConfig.account_editor_task_owner,
            globalConfig.generic_password
        );
    },

    'Step 4: Verify All Projects Page - Should Work': () => {
        proggioMainPage.clickOnElement('@allProjects');
        proggioMainPage.expect.element('@tilesView').to.be.visible;
    },

    'Step 5: Verify Dashboard Page - Should Work': async(browser) => {
        proggioMainPage.clickOnElement('@dashboard');
        const closePopup = proggioMainPage.elements.closeFeedBack;
        const result = await browser.element('css selector', closePopup);
        if (result.status !== -1) {
            await proggioMainPage.clickOnElement('@closeFeedBack');
        }
        dashboardPage.expect.element('@selectDashboardDropdown').to.be.visible;
    },

    'Step 6: Verify Task Management Page - Should Work': () => {
        proggioMainPage.clickOnElement('@taskManagement');
        taskManagementPage.expect.element('@listView').to.be.visible;
    },

    'Step 7: Verify Settings Page - Should Work': () => {
        proggioMainPage.clickOnElement('@settings');
        settingsPage.expect.element('@inviteNewUser').to.be.visible;
    },

    'Step 8: Verify Portfolio Page - Should Work': () => {
        proggioMainPage.expect.element('@portfolio').to.be.visible;
    },

    'Step 9: Verify Budget Page - Should Work': () => {
        proggioMainPage.expect.element('@budget').to.be.visible;
    },

    'Step 10: Verify Resources Management Page - Should Work': () => {
        proggioMainPage.expect.element('@resourcesManagement').to.be.visible;
    },

    'Step 11: Verify Project - Should Work': () => {
        proggioMainPage.clickOnElement('@allProjects');
        tilesViewPage.verifyProjectPresent(projectName);
    },

    'Step 12: Enter Project': () => {
        proggioMainPage.clickOnElement('@tilesView');
        tilesViewPage.openProject(projectName);
    },

    'Step 13: Create Milestone - Should NOT Work': async function(browser) {
        activityPage.zoomIntoProject();
        await browser.moveToElement('xpath', activityPage.elements.projectGridToday, 96, 0);
        await browser.doubleClick();
        milestonePage.expect.element('@milestoneID').to.not.be.present.before(globalConfig.timeOut);
    },

    'Step 14: His WorkStream - Create Activity - Should Work': async function(browser) {
        await activityPage.createNewActivity(`${activity2Id}`);
        await browser.mouseButtonClick(2);
        await activityPage.clickOnElement('@details');
        await activityModalPage.renameActivityTask(activityName);
    },

    'Step 15: His WorkStream - Add Tasks - Should Work': () => {
        activityModalPage.addNewTask(`${taskName}`);
        activityModalPage.verifyTaskActivityPresent(taskName, taskName);
    },

    'Step 16: Delete Activity - Should Work': async() => {
        activityModalPage.selectTaskOrActivityInModal(activityName);
        await activityModalPage.deleteActivityInModal();
    },

    'Step 17: His WorkStream - Move Activity - Should Work': async() => {
        const xOffsetNum = 30;
        xStartLocation = await activityPage.getActivityXAxisLocation(activity2Id);
        await activityPage.moveActivityByOffset(
            `${activity2Id}`, xOffsetNum);
        xEndLocation = await activityPage.getActivityXAxisLocation(activity2Id);
        assert.strict.notEqual(xStartLocation, xEndLocation);
        await activityPage.moveActivityByOffset(
            `${activity2Id}`, -xOffsetNum);
    },

    'Step 18: His Activity NOT His WorkStream - Change Dates - Should NOT Work': async() => {
        await activityPage.openActivitySettingsById(activityPage, activity2Id, '@details');
        activityModalPage.clickOnElement('@activityStartDate');
        activityModalPage.expect.element('@activityDatePickerPopup').to.be.present.before(globalConfig.timeOut);
        activityModalPage.clickOnElement('@activityStartDate');
    },

    'Step 19: Connect Activities - Should Work': async() => {
        activityModalPage.activityModalChooseTab(activityModalPage, '@dependenciesTab');
        activityModalPage.connectToActivity(`${activity3Name}`);
    },

    'Step 20: Verify - Connected Activities': () => {
        activityModalPage.verifyActivityConnected(activityConnectedName);
        activityModalPage.closeActivitySettings();
    },

    'Step 21: Move Both Activities - Should Work': async() => {
        const xOffsetNum = 20;
        xStartLocation = await activityPage.getActivityXAxisLocation(activity3Id);
        await activityPage.moveActivityById(activity2Id, activity3Id, xOffsetNum);
        xEndLocation = await activityPage.getActivityXAxisLocation(activity3Id);
        assert.strict.notEqual(xStartLocation, xEndLocation);
    },

    'Step 22: Move Both Activities - Move Back To Original Place': async function() {
        await activityPage.moveActivityById(activity2Id, anchorA2StartId, 1);
        await activityPage.moveActivityById(activity3Id, anchorA3StartId, 1);
    },

    'Step 23: Move Both Activities - Remove Connection': async function() {
        await activityPage.openActivitySettingsById(activityPage, activity2Id, '@details');
        activityModalPage.activityModalChooseTab(activityModalPage, '@dependenciesTab');
        activityModalPage.deleteActivityConnection(activityConnectedName);
        activityModalPage.closeActivitySettings();
    },

    'Step 24: Delete WorkSteam - Should NOT Work': () => {
        activityPage.clickOnElement('@projectGirdLeftArrow');
        workStreamPage.expect.element('@addNewWorkStreamHidden').to.be.present;
    },

    'Step 25: His Activity NOT His WorkStream - Move Activity - Should NOT Work': async() => {
        const xOffsetNum = 30;
        xStartLocation = await activityPage.getActivityXAxisLocation(activity4Id);
        await activityPage.moveActivityByOffset(
            `${activity4Id}`, xOffsetNum);
        xEndLocation = await activityPage.getActivityXAxisLocation(activity4Id);
        assert.strict.equal(xStartLocation, xEndLocation);
        await activityPage.moveActivityByOffset(
            `${activity4Id}`, -xOffsetNum);
    },

    'Step 26: His Activity NOT His WorkStream - Rename Activity - Should Work': async function() {
        await activityPage.openActivitySettingsById(activityPage, activity4Id, '@details');
        await activityModalPage.renameActivityTask(activity4Name);
        activityModalPage.expect.element('@activityNameText').text.to.contain(activity4Name);
    },

    'Step 27: His Activity NOT His WorkStream - Add Tasks - Should Work': () => {
        activityModalPage.addNewTask(taskName);
        activityModalPage.deleteTask(taskName);
    },

    'Step 28: His Activity NOT His WorkStream - Delete Activity - Should NOT Work': () => {
        activityModalPage.expect.element('@deleteButton').to.not.be.present;
        activityModalPage.closeActivitySettings();
    },

    'Step 29: His Connected Activity NOT His WorkStream - Move Activity - Should NOT Work': async function() {
        const xOffsetNum = 20;
        xStartLocation = await activityPage.getActivityXAxisLocation(activity5Id);
        await activityPage.moveActivityById(activity2Id, activity5Id, xOffsetNum);
        xEndLocation = await activityPage.getActivityXAxisLocation(activity5Id);
        assert.strict.equal(xStartLocation, xEndLocation);
    },

    'Step 30: His Connected Activity - Move Back To Original Place': async function() {
        await activityPage.moveActivityById(activity2Id, anchorA2StartId, 1);
    }
};
