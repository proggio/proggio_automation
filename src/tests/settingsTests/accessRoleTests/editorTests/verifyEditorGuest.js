let proggioMainPage, settingsPage, taskManagementPage, activityPage,
    tilesViewPage, activityModalPage;
const myTasks = {
    task1: 'My 1st task',
    task2: 'My 2nd task',
    task3: 'My 3rd task'
};
const activity1 = 'Activity1';
const globalConfig = require('../../../../main/utilities/globals');
module.exports = {
    '@tags': ['allProjectsTests', 'settingsTests', 'accessRoleTests', 'editorTests', 'fullRun', 'verifyEditorGuest'],
    before: function(browser) {
        console.log('Loading element pages...');
        proggioMainPage = browser.page.proggioMainPage();
        settingsPage = browser.page.settingsPage();
        tilesViewPage = browser.page.tilesViewPage();
        taskManagementPage = browser.page.taskManagementPage();
        activityPage = browser.page.activityPage();
        activityModalPage = browser.page.activityModalPage();
        browser.login(globalConfig.proggio_username, globalConfig.proggio_password);
    },

    'Step 1: Click Settings': () => {
        proggioMainPage.clickOnElement('@settings');
    },

    "Step 2: Change User Permission To: 'Editor + Guest'": () => {
        settingsPage.changeAccessAndRole(settingsPage,
            `${globalConfig.account_editor_guest}`,
            `@editor`,
            `@guest`);
    },

    "Step 3: Enter 'Editor Permissions' Project": async() => {
        const projectName = 'Editor Permissions';
        proggioMainPage.allProjectsViewPicker(globalConfig.allProjects.views.tilesView);

        tilesViewPage.openProject(projectName);
        await activityPage.zoomIntoProject();
    },

    'Step 4: Open Activity Settings': async() => {
        const activityName = 'Activity1';
        await activityPage.openActivitySettingsMouseClick(activityPage,
            `${activityName}`,
            `@details`
        );
    },

    "Step 5: Reassign Activity To 'Editor + Guest'": () => {
        const assigneeName = 'Editor+Guest';
        activityModalPage.assignActivityTask(activity1, assigneeName);
        activityModalPage.closeActivitySettings();
    },

    "Step 6: Login With 'Editor + Guest' User": (browser) => {
        browser.logout();
        browser.login(
            globalConfig.account_editor_guest,
            globalConfig.generic_password
        );
    },

    'Step 7: Verify User Owned Tasks - Should Work': () => {
        proggioMainPage.expect.element('@spinner').to.not.be.present.before(globalConfig.timeOut);
        taskManagementPage.clickOnElement('@listView');
        for (let x in myTasks) {
            taskManagementPage.verifyTaskPresent(myTasks[x], myTasks[x]);
        }
    },

    'Step 8: Verify User Owned Activities - Should Work': () => {
        taskManagementPage.expect.element('@cellActivityName').text.to.contain(activity1);
    },

    'Step 9: Side Menu - Should NOT Work': () => {
        proggioMainPage.expect.element('@sideMenu').to.not.be.present.before(globalConfig.timeOut);
    },

    'Step 10: Open Activity Settings': () => {
        taskManagementPage.openSettings(`${activity1}`);
    },

    'Step 11: Edit Activity Names - Should Work': () => {
        activityModalPage.renameActivityTask(activity1);
    },

    'Step 12: Edit Tasks Names - Should Work': () => {
        activityModalPage.renameTask(myTasks.task3);
    },

    'Step 13: Edit Task Dates - Should NOT Work': () => {
        activityModalPage.clickOnElement('@activityDatePickerDisabled');
        activityModalPage.expect.element('@activityDatePickerPopup').to.not.be.present.before(globalConfig.timeOut);
    },

    'Step 14: Add Tasks - Should Work': () => {
        const newTask = 'My new task';
        activityModalPage.addNewTask(newTask);
        activityModalPage.verifyTaskActivityPresent(newTask, newTask);
        activityModalPage.deleteTask(newTask);
    },

    'Step 15: Assign Activity to another User - Should Work': () => {
        let assigneeName = 'Vladi';
        activityModalPage.assignActivityTask(activity1, assigneeName);
    },

    'Step 16: Close Settings': () => {
        activityModalPage.closeActivitySettings();
    },

    'Step 17: Verify Activity Not Present': () => {
        const myTask = `div[class*= "Table__Grid"] div[class = "activity_name"] div[title*= "${activity1}"]`;
        taskManagementPage.expect.element(myTask).to.not.be.present.before(globalConfig.timeOut);
    },

    'Step 18: Go To Board View': () => {
        taskManagementPage.clickOnElement('@boardViewToggle');
    },

    'Step 19: Drag Item To Other Column - Should Work': async() => {
        const statusList = `//div[contains(@title, "${globalConfig.taskManagement.inReview}")]/../..//div[contains(@class, "List")]/div[contains(@class, "Grid")]`;
        await taskManagementPage.dragAndDropItem(
            `${globalConfig.taskManagement.backLog}`,
            `${myTasks.task1}`,
            `${globalConfig.taskManagement.inReview}`
        );
        taskManagementPage.expect.elements(statusList).count.to.equal(1);
    },

    "Step 20: Drag Item To 'Backlog' Column": async() => {
        await taskManagementPage.dragAndDropItem(
            `${globalConfig.taskManagement.inReview}`,
            `${myTasks.task1}`,
            `${globalConfig.taskManagement.backLog}`
        );
    }
};
