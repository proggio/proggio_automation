const globalConfig = require('../../main/utilities/globals');
let createAccountPage, signUpPage, signedUpPage, userProfilePage, gmailMainPage, gmailLoginPage;
module.exports = {
    '@tags': ['allProjectsTests', 'registerTests', 'excludedTests'],
    before: function(browser) {
        console.log('Loading element pages...');
        createAccountPage = browser.page.createAccountPage();
        signUpPage = browser.page.signUpPage();
        signedUpPage = browser.page.signedUpPage();
        gmailLoginPage = browser.page.gmailLoginPage();
        gmailMainPage = browser.page.gmailMainPage();
        userProfilePage = browser.page.userProfilePage();
        browser.login(globalConfig.proggio_username, globalConfig.proggio_password);
    },

    'Step 1: Navigate To Gmail': (browser) => {
        browser.url(globalConfig.gmailLogin_url);
    },

    'Step 2: Login To Gmail': () => {
        gmailLoginPage.gmailLogin(
            globalConfig.proggio_username,
            globalConfig.proggioGmail_password);
    },

    'Step 3: Delete Previous Emails': (browser) => {
        browser.url(globalConfig.gmailMailBox_url);
        gmailMainPage.closePopup();
        let gmailMessageElement = gmailMainPage.elements.confirmEmailMessage;
        let emailMessageButton = gmailMainPage.elements.deleteEmailMessageButton;
        browser.element('xpath', gmailMessageElement, function(result) {
            if (result.status !== -1) {
                gmailMainPage.deleteGmailMsg(gmailMessageElement, emailMessageButton);
            }
        });
    },

    'Step 4: Navigate To Proggio Sign Up Page': (browser) => {
        browser.url('https://app-staging.proggio.com');
        browser.logout();
    },

    'Step 5: Input New Work Email': (browser) => {
        browser.url('https://app-staging.proggio.com/signup');
        createAccountPage.setValueInElement('@emailInput', globalConfig.dynamic_userName);
        createAccountPage.clickOnElement('@createAccountButton');
    },

    'Step 6: Fill Sign Up Form': () => {
        let genericMobileNumber = '1231231233';
        signUpPage.fillForm(
            `${genericMobileNumber}`,
            `${globalConfig.generic_password}`,
            `${globalConfig.dynamic_userName}`
        );
    },

    'Step 7: Verify Email Text in Proggio': () => {
        signedUpPage.assert.containsTextInElement('@signedUpEmailText', globalConfig.dynamic_userName);
    },

    'Step 8: Navigate To Gmail': (browser) => {
        browser.url(globalConfig.gmailMailBox_url);
    },

    'Step 9: Verify Registered Email ': (browser) => {
        if (gmailMainPage.expect.element('@confirmEmailMessage').to.not.be.present) {
            browser.pause(3000);
            browser.refresh();
            gmailMainPage.confirmSignUpMsg();
        } else {
            gmailMainPage.confirmSignUpMsg();
        }
    },

    'Step 10: Verify Account Name': (browser) => {
        browser.switchToWindowByIndex(1);
        userProfilePage.clickOnElement('@userProfileButton');
        userProfilePage.assert.containsTextInElement('@profileEmailName', globalConfig.dynamic_userName);
        userProfilePage.clickOnElement('@userProfileButton');
    },

    'Step 11: Receive Username and Password': () => {
        console.log(
            'Username: ' + globalConfig.dynamic_userName + '\n' +
            'Password: ' + globalConfig.generic_password
        );
    }
};
