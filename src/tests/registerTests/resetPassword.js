const globalConfig = require('../../main/utilities/globals');
let gmailLoginPage, resetPasswordPage, resetSubmittedPage, userProfilePage, gmailMainPage;
let proggioEmail = 'vladi.b+1611836790852@progg.io';

module.exports = {
    '@tags': ['allProjectsTests', 'registerTests', 'excludedTests'],
    before: function(browser) {
        console.log('Loading element pages...');
        resetPasswordPage = browser.page.resetPasswordPage();
        resetSubmittedPage = browser.page.resetSubmittedPage();
        gmailMainPage = browser.page.gmailMainPage();
        gmailLoginPage = browser.page.gmailLoginPage();
        userProfilePage = browser.page.userProfilePage();
        browser.login(globalConfig.proggio_username, globalConfig.proggio_password);
    },

    'Step 1: Navigate To Gmail': (browser) => {
        browser.url(globalConfig.gmailLogin_url);
    },

    'Step 2: Login To Gmail': () => {
        gmailLoginPage.gmailLogin(
            globalConfig.proggio_username,
            globalConfig.proggioGmail_password);
    },

    'Step 3: Delete Previous Emails': async function(browser) {
        browser.url(globalConfig.gmailMailBox_url);
        gmailMainPage.closePopup();
        let gmailMessageElement = gmailMainPage.elements.confirmResetPasswordMessage;
        let passwordMessageButton = gmailMainPage.elements.deletePasswordMessageButton;
        const result = await browser.element('xpath', gmailMessageElement);
        if (result.status !== -1) {
            await gmailMainPage.deleteGmailMsg(gmailMessageElement, passwordMessageButton);
        }
    },

    'Step 4: Navigate To Proggio Reset Password Page': (browser) => {
        browser.url('https://app-staging.proggio.com');
        browser.logout();
        browser.url('https://app-staging.proggio.com/reset-request');
    },

    'Step 5: Enter Your Email': () => {
        resetPasswordPage.setValueInElement('@emailToResetInput', proggioEmail);
        resetPasswordPage.clickOnElement('@resetPasswordButton');
    },

    'Step 6: Verify Email Sent': () => {
        let expected = 'We just emailed you a link';
        resetSubmittedPage.assert.containsTextInElement('@linkSentText', expected);
    },

    'Step 7: Verify Password Reset Email': (browser) => {
        browser.url(globalConfig.gmailMailBox_url);
        if (gmailMainPage.expect.element('@confirmResetPasswordMessage').to.not.be.present) {
            browser.pause(3000);
            browser.refresh();
            gmailMainPage.confirmPasswordResetMsg();
        } else {
            gmailMainPage.confirmPasswordResetMsg();
        }
    },

    'Step 8: Set New Password': (browser) => {
        browser.switchToWindowByIndex(1);
        let newPassword = globalConfig.generic_password;
        resetPasswordPage.setNewPassword(resetPasswordPage, newPassword);
    },

    'Step 9: Verify Account Name': () => {
        userProfilePage.clickOnElement('@userProfileButton');
        userProfilePage.assert.containsTextInElement('@profileEmailName', proggioEmail);
        userProfilePage.clickOnElement('@userProfileButton');
    },

    'Step 10: Receive Username and Password': () => {
        console.log(
            'Username: ' + proggioEmail + '\n' +
            'Password: ' + globalConfig.generic_password
        );
    }
};
