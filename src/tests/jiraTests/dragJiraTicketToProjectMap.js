let tilesViewPage, proggioMainPage, activityPage,
    workStreamPage, splitPage, activityModalPage;
const globalConfig = require('../../main/utilities/globals');
const projectTitle = 'FROG board Map';
const activity1 = 'map_activity_id_350735';
const ticketName = 'Duplicate';
const testTicket = `PRG-3199 (Yaniv's Epic for Test)`;
const boardName = `PRG board scrum`;
let activityTicketId;
module.exports = {
    '@tags': ['jiraTests', 'sanity', 'dragJiraTicketToProjectMap'],
    before: function(browser) {
        console.log('Loading element pages...');
        tilesViewPage = browser.page.tilesViewPage();
        splitPage = browser.page.splitPage();
        activityPage = browser.page.activityPage();
        activityModalPage = browser.page.activityModalPage();
        proggioMainPage = browser.page.proggioMainPage();
        workStreamPage = browser.page.workStreamPage();
        browser.login(globalConfig.jira_account, globalConfig.generic_password);
    },

    'Step 1: Click On Project': () => {
        proggioMainPage.allProjectsViewPicker('tiles');
        tilesViewPage.enterToProject(`${projectTitle}`);
    },

    'Step 2: Open Split': () => {
        splitPage.openSplit();
    },

    'Step 3: Go To "Import" Tab': () => {
        splitPage.goToImportTab();
    },

    'Step 4: Import Jira Board': () => {
        splitPage.importProjectBoard(splitPage,
            `JIRA`,
            `${boardName}`,
            `@epics`,
            `${testTicket}`
        );
    },

    'Step 5: Drag and Drop Jira Ticket on Project Map': async(browser) => {
        let splitGripLines = splitPage.elements.splitGripLines;
        let workStreamBottom = workStreamPage.elements.addNewWorkStreamBottom;
        await browser.dragAndDrop('css selector', splitGripLines, workStreamBottom);
        await splitPage.dragJiraTicketToMap(
            `${ticketName}`,
            `${activity1}`,
            50,
            true
        );
        await proggioMainPage.expect.element('@activitySpinner').to.not.be.present.before(globalConfig.timeOut);
    },

    'Step 6: Get Ticket Activity ID': async() => {
        activityTicketId = await activityPage.getActivityId(ticketName);
    },

    'Step 7: Open Ticket Activity Settings': async() => {
        await activityPage.openActivitySettingsById(activityPage,
            `${activityTicketId}`,
            '@details'
        );
    },

    'Step 8: Verify Ticket on Project Map': async() => {
        activityModalPage.expect.element('@activityMainDescription').text.to.contain(ticketName);
    },

    'Step 9: Delete Ticket From Project Map': async(browser) => {
        await activityModalPage.deleteActivityInModal();
        browser.expect.element(activityTicketId).to.not.be.present.after(globalConfig.timeOut);
    }
};
