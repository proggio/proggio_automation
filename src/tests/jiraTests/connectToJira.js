let proggioMainPage, settingsPage,
    jiraConnectPage;
const globalConfig = require('../../main/utilities/globals');
module.exports = {
    '@disabled': true,
    '@tags': ['jiraTests', 'sanity'],
    before: function(browser) {
        console.log('Loading element pages...');
        proggioMainPage = browser.page.proggioMainPage();
        settingsPage = browser.page.settingsPage();
        jiraConnectPage = browser.page.jiraConnectPage();
        browser.login(globalConfig.jira_account, globalConfig.proggio_password);
    },

    'Step 1: Go To Settings': () => {
        proggioMainPage.clickOnElement('@settings');
    },

    "Step 2: Click On 'Connect' Jira Option": () => {
        settingsPage.clickOnElement('@jiraConnectButton');
    },

    'Step 3: Connect To Jira': () => {
        jiraConnectPage.connectToJira(globalConfig.jiraDomainLink);
    },

    'Step 4: Verify Jira Connected': () => {
        // TODO - connectToJira - Currently not available
    },

    'Step 5: Disconnect From Jira': () => {}

};
