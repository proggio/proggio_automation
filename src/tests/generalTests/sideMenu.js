const globalConfig = require('../../main/utilities/globals');
let tilesViewPage, proggioMainPage, budgetPage, dashboardPage, portfolioPage, resourcesManagementPage, settingsPage,
    taskManagementPage;
module.exports = {
    '@tags': ['sideMenu', 'fullRun', 'generalTests'],
    before(browser) {
        console.log('Loading element pages...');
        tilesViewPage = browser.page.tilesViewPage();
        proggioMainPage = browser.page.proggioMainPage();
        portfolioPage = browser.page.portfolioPage();
        dashboardPage = browser.page.dashboardPage();
        budgetPage = browser.page.budgetPage();
        taskManagementPage = browser.page.taskManagementPage();
        resourcesManagementPage = browser.page.resourcesManagementPage();
        settingsPage = browser.page.settingsPage();
        browser.login(globalConfig.automation_testing_account, globalConfig.generic_password);
    },

    'Step 1: Verify All Projects Page': () => {
        proggioMainPage.allProjectsViewPicker('tiles');
        tilesViewPage.expect.element('@newProjectButton').to.be.visible;
    },

    'Step 2: Verify Portfolio Page': () => {
        proggioMainPage.clickOnElement('@portfolio');
        portfolioPage.expect.element('@newProjectButton').to.be.visible;
    },

    'Step 3: Verify Dashboard Page': async(browser) => {
        proggioMainPage.clickOnElement('@dashboard');
        const closePopup = proggioMainPage.elements.closeFeedBack;
        const result = await browser.element('css selector', closePopup);
        if (result.status !== -1) {
            await proggioMainPage.clickOnElement('@closeFeedBack');
        }
        dashboardPage.expect.element('@selectDashboardDropdown').to.be.visible;
    },

    'Step 4: Verify Budget Page': () => {
        proggioMainPage.clickOnElement('@budget');
        budgetPage.expect.element('@projectBudgetButton').to.be.visible;
    },

    'Step 5: Verify Task Management Page': () => {
        proggioMainPage.clickOnElement('@taskManagement');
        taskManagementPage.expect.element('@createNewTaskButton').to.be.visible;
    },

    'Step 6: Verify Resources Management Page': () => {
        proggioMainPage.clickOnElement('@resourcesManagement');
        resourcesManagementPage.expect.element('@toolbarTitle').to.be.visible;
    },

    'Step 7: Verify Settings Page': () => {
        proggioMainPage.clickOnElement('@settings');
        settingsPage.expect.element('@toolbarTitle').to.be.visible;
    }
};
