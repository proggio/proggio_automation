const globalConfig = require('../../../main/utilities/globals');
let userProfilePage, tilesViewPage, proggioMainPage;
const fullName = 'vladi.b+My_User_Profile';
const eventManagementProject = 'Event Management';
const noneOptionText = 'Two factor authentication is now disabled';
const emailOptionText = 'Email Two-Factor Authentication has been enabled';
const activityIdElement = '#map_activity_id_356574';
const activityStyleElement = 'g[id = "map_activity_id_356574"] div[style*= "font-size"]';
const lightColor = '255, 255, 255';
const darkColor = '0, 0, 0';
const genericName = 'Test';
let alertElement;
module.exports = {
    '@tags': ['userProfileTests', 'fullRun', 'userProfileSettingsModal'],
    before(browser) {
        console.log('Loading element pages...');
        userProfilePage = browser.page.userProfilePage();
        tilesViewPage = browser.page.tilesViewPage();
        proggioMainPage = browser.page.proggioMainPage();
        browser.login(globalConfig.myUserProfile, globalConfig.generic_password);
    },

    'Step 1: [Test - My User Profile Modal] - Open My Profile': (browser) => {
        proggioMainPage.allProjectsViewPicker('tiles');
        tilesViewPage.openProject(`${eventManagementProject}`);
        browser.isVisible('css selector', activityIdElement);
    },

    'Step 1.1: [Test - My User Profile Modal] - Open My Profile': () => {
        userProfilePage.openUserProfile();
    },

    'Step 2: [Verify - My User Profile Modal]': () => {
        userProfilePage.expect.element('@myProfileModalContainer').to.be.present.before(globalConfig.timeOut);
    },

    'Step 3: [Test - Change Authentication To "None" Option]': () => {
        alertElement = userProfilePage.getAlertTextElement(`${noneOptionText}`);
        userProfilePage.authenticationPicker(`${globalConfig.authentication.none}`);
    },

    'Step 4: [Verify - Change Authentication To "None" Option]': (browser) => {
        browser.useXpath();
        userProfilePage.expect.element(alertElement).to.be.present.before(globalConfig.timeOut);
    },

    'Step 5: [Test - Change Authentication To "Email" Option]': () => {
        alertElement = userProfilePage.getAlertTextElement(`${emailOptionText}`);
        userProfilePage.authenticationPicker(`${globalConfig.authentication.email}`);
    },

    'Step 6: [Verify - Change Authentication To "Email" Option]': (browser) => {
        browser.useXpath();
        browser.expect.element(alertElement).to.be.present.before(globalConfig.timeOut);
    },

    'Step 7: [Test - Change Authentication To "Authenticator" Option]': () => {
        userProfilePage.authenticationPicker(`${globalConfig.authentication.authenticator}`);
    },

    'Step 8: [Verify - Change Authentication To "Authenticator" Option]': (browser) => {
        let authenticationContainerElement = '//div[contains(text(), "Set up Two-Factor Authentication")]/../../..//div[@id = "modal_button_Set up Two-Factor Authentication_Cancel"]';
        browser.useXpath();
        browser.expect.element(authenticationContainerElement).to.be.present.before(globalConfig.timeOut);
        browser.clickOnElement(authenticationContainerElement);
        userProfilePage.authenticationPicker(`${globalConfig.authentication.none}`);
    },

    'Step 9: [Test - Text Size - Small]': () => {
        userProfilePage.textSizePicker(`${globalConfig.textSize.small}`);
        userProfilePage.saveUserProfile();
    },

    'Step 10: [Verify - Text Size - Small]': (browser) => {
        browser.useCss();
        browser.assert.attributeContains(activityStyleElement, 'style', 'font-size: 14px');
    },

    'Step 11: [Test - Text Size - Medium]': () => {
        userProfilePage.openUserProfile();
        userProfilePage.textSizePicker(`${globalConfig.textSize.medium}`);
        userProfilePage.saveUserProfile();
    },
    'Step 12: [Verify - Text Size - Medium]': (browser) => {
        browser.useCss();
        browser.assert.attributeContains(activityStyleElement, 'style', 'font-size: 16px');
    },

    'Step 13: [Test - Text Size - Large]': () => {
        userProfilePage.openUserProfile();
        userProfilePage.textSizePicker(`${globalConfig.textSize.large}`);
        userProfilePage.saveUserProfile();
    },

    'Step 14: [Verify - Text Size - Large]': (browser) => {
        browser.useCss();
        browser.assert.attributeContains(activityStyleElement, 'style', 'font-size: 18px');
        userProfilePage.openUserProfile();
        userProfilePage.textSizePicker(`${globalConfig.textSize.auto}`);
    },

    'Step 15: [Test - Text Color - Dark]': () => {
        userProfilePage.textColorPicker(`${globalConfig.textColor.dark}`);
        userProfilePage.saveUserProfile();
    },

    'Step 16: [Verify - Text Color - Dark]': (browser) => {
        browser.useCss();
        browser.assert.attributeContains(activityStyleElement, 'style', `color: rgb(${darkColor})`);
    },

    'Step 17: [Test - Text Color - Light]': () => {
        userProfilePage.openUserProfile();
        userProfilePage.textColorPicker(`${globalConfig.textColor.light}`);
        userProfilePage.saveUserProfile();
    },

    'Step 18: [Verify - Text Color - Light]': (browser) => {
        browser.useCss();
        browser.assert.attributeContains(activityStyleElement, 'style', `color: rgb(${lightColor})`);
    },

    'Step 19: [Test - Periodic Email Report - Daily]': () => {
        userProfilePage.openUserProfile();
        userProfilePage.periodicEmailReportPicker(`${globalConfig.periodicEmailReport.daily}`);
    },

    'Step 20: [Verify - Periodic Email Report - Daily]': () => {
        userProfilePage.assert.attributeContains('@reportDaily', 'class', `option selected`);
    },

    'Step 21: [Test - Periodic Email Report - Monthly]': () => {
        userProfilePage.periodicEmailReportPicker(`${globalConfig.periodicEmailReport.monthly}`);
    },

    'Step 22: [Verify - Periodic Email Report - Monthly]': () => {
        userProfilePage.assert.attributeContains('@reportMonthly', 'class', `option selected`);
    },

    'Step 23: [Test - Periodic Email Report - Never]': () => {
        userProfilePage.periodicEmailReportPicker(`${globalConfig.periodicEmailReport.never}`);
    },

    'Step 24: [Verify - Periodic Email Report - Never]': () => {
        userProfilePage.assert.attributeContains('@reportNever', 'class', `option selected`);
    },

    'Step 25: [Test - Periodic Email Report - Weekly]': () => {
        userProfilePage.periodicEmailReportPicker(`${globalConfig.periodicEmailReport.weekly}`);
    },

    'Step 26: [Verify - Periodic Email Report - Weekly]': () => {
        userProfilePage.assert.attributeContains('@reportWeekly', 'class', `option selected`);
    },

    'Step 27: [Test - Add Tags]': () => {
        userProfilePage.addTag(`${genericName}`);
    },

    'Step 28: [Verify - Add Tags]': () => {
        const removeTagElement = userProfilePage.getTagElement(`${genericName}`);
        userProfilePage.expect.element(removeTagElement).to.be.present.before(globalConfig.timeOut);
    },

    'Step 29: Remove Tag': () => {
        userProfilePage.removeTag(`${genericName}`);
    },

    'Step 30: [Test - Add Groups]': () => {
        userProfilePage.addGroupTag(`${genericName}`);
    },

    'Step 31: [Verify - Add Groups]': () => {
        const removeTagElement = userProfilePage.getGroupTagElement(`${genericName}`);
        userProfilePage.expect.element(removeTagElement).to.be.present.before(globalConfig.timeOut);
    },

    'Step 32: Remove Group Tags': () => {
        userProfilePage.removeGroupTag(`Test-Group`);
    },

    'Step 33: [Test - Change Full Name]': () => {
        userProfilePage.changeFullName(`${genericName}`);
        userProfilePage.saveUserProfile();
    },

    'Step 34: [Verify - Change Full Name]': () => {
        userProfilePage.openUserProfile();
        const fullNameElement = userProfilePage.getFullNameValue(`${genericName}`);
        userProfilePage.expect.element(fullNameElement).to.be.present.before(globalConfig.timeOut);
    },

    'Step 35: Change Full Name To Original': () => {
        userProfilePage.changeFullName(`${fullName}`);
        userProfilePage.saveUserProfile();
    },

    'Step 36: [Test - Change Username]': () => {
        userProfilePage.openUserProfile();
        userProfilePage.changeUserName(`${genericName}`);
        userProfilePage.saveUserProfile();
    },

    'Step 37: [Verify - Change Username]': () => {
        userProfilePage.openUserProfile();
        const usernameElement = userProfilePage.getUserNameValue(`${genericName}`);
        userProfilePage.expect.element(usernameElement).to.be.present.before(globalConfig.timeOut);
    },

    'Step 38: Change Username To Original': () => {
        userProfilePage.changeUserName(`${fullName}`);
    },

    'Step 39: [Test - Change User Color]': () => {
        userProfilePage.colorPicker('#b71c1c');
    },

    'Step 40: [Verify - Change User Color]': () => {
        const colorValue = userProfilePage.getColorValueElement('#b71c1c');
        userProfilePage.expect.element(colorValue).to.be.present.before(globalConfig.timeOut);
    },

    'Step 41: Return To Original Color': () => {
        userProfilePage.colorPicker('#880e4f');
        userProfilePage.saveUserProfile();
    }

    // 'Step : [Test - Receive Emails From Proggio]': () => {},
    // 'Step : [Test - Update Email]': () => {},
    // 'Step : [Test - Change Password]': () => {},
};
