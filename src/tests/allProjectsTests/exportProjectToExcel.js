const xlsxFile = require('read-excel-file/node');
const expect = require('chai').expect;
const fs = require('fs');
const { excelDateFormat } = require('../../main/utilities/customDate');
let listViewPage, proggioMainPage, toolsPage;
const allProjectsView = 'All Projects';
const globalConfig = require('../../main/utilities/globals');
module.exports = {
    '@disabled': true,
    '@tags': ['fullRun', 'allProjectsTests', 'exportProjectToExcel'],
    before(browser) {
        console.log('Loading element pages...');
        proggioMainPage = browser.page.proggioMainPage();
        listViewPage = browser.page.listViewPage();
        toolsPage = browser.page.toolsPage();
        browser.login(globalConfig.list_view_account, globalConfig.generic_password);
    },

    'Step 1: [Test - Export Project To Excel From List View] - Click On List View': () => {
        proggioMainPage.allProjectsViewPicker(globalConfig.allProjects.views.listView);
    },

    'Step 2: Click On "All Projects" View': () => {
        listViewPage.changeView(`${allProjectsView}`);
    },

    'Step 3: Export Project To Excel': (browser) => {
        toolsPage.exportProjectToExcel('all projects list view');
        browser.pause(3000);
    },

    'Step 4: [Verify - Export Project To Excel From List View]': () => {
        xlsxFile(`C:/Users/vladi/Downloads/Projects Data ${excelDateFormat}.xlsx`).then((rows) => {
            for (let i = 0; i < rows[0].length; i++) {
                expect(rows[1][0]).to.not.be.equal(null);
            }
        });
    },

    'Step 5: Delete Excel File From OS': () => {
        fs.unlinkSync(`C:/Users/vladi/Downloads/Projects Data ${excelDateFormat}.xlsx`);
    }
};
