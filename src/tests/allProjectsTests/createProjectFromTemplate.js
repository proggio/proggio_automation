const globalConfig = require('../../main/utilities/globals');
let workStreamPage, tilesViewPage;
let project = 'Create Project From Template Test';
module.exports = {
    '@tags': ['fullRun', 'allProjectsTests', 'createProjectFromTemplate'],
    before(browser) {
        console.log('Loading element pages...');
        tilesViewPage = browser.page.tilesViewPage();
        workStreamPage = browser.page.workStreamPage();
        browser.login(globalConfig.automation_testing_account, globalConfig.generic_password);
    },

    "Step 1: Click 'Create from template or import' Collapse ": async() => {
        await tilesViewPage.deleteAllProjects();
        await tilesViewPage.clickOnElement('@createTemplateCollapse');
    },

    'Step 2: Pick Project Template': () => {
        tilesViewPage.clickOnElement('@applicationDevelopment');
    },

    'Step 3: Create Project From Template': () => {
        tilesViewPage.clearValue('css selector', '@textInput');
        tilesViewPage.setValueInElement('@textInput', project);
        tilesViewPage.clickOnElement('@createProjectTemplate');
    },

    "Step 4: Verify New Project's Creation": () => {
        workStreamPage.expect.element('@addNewWorkStreamButton').to.be.visible.before(globalConfig.timeOut);
    },

    'Step 5: Verify Existing WorkStreams': () => {
        workStreamPage.waitForElementVisible('@workStreamList');
        workStreamPage.expect.elements('@workStreamList').count.to.equal(6);
    }

};
