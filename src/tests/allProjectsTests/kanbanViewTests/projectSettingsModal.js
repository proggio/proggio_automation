let kanbanViewPage, proggioMainPage, projectModalPage;
const globalConfig = require('../../../main/utilities/globals');
const vendorSelection = 'Vendor selection';
const customProjectName = 'New Project';
const originalUser = 'vladi.b+kanban_View';
const assigneeUser = 'P-Bot';
const newTag = 'My New Tag';
module.exports = {
    '@tags': ['fullRun', 'allProjectsTests', 'listViewTests', 'kanbanProjectSettingsModal'],
    before(browser) {
        console.log('Loading element pages...');
        proggioMainPage = browser.page.proggioMainPage();
        kanbanViewPage = browser.page.kanbanViewPage();
        projectModalPage = browser.page.projectModalPage();
        browser.login(globalConfig.kanban_view_account, globalConfig.generic_password);
    },

    'Step 1: [Test - Project Settings] - Click On Kanban View': () => {
        proggioMainPage.allProjectsViewPicker(globalConfig.allProjects.views.kanbanView);
    },

    'Step 2: Open Project Settings': () => {
        kanbanViewPage.openProjectSettings(`${vendorSelection}`, globalConfig.contextMenu.settings);
    },

    'Step 3: [Test - Change Project Name] - Change Project Name': () => {
        projectModalPage.changeProjectName(`${customProjectName}`);
    },

    'Step 4: [Verify - Change Project Name]': () => {
        projectModalPage.expect.element('@projectNameInput').to.have.attribute('value', `${customProjectName}`);
    },

    'Step 5: Return Original Project Name': () => {
        projectModalPage.changeProjectName(`${vendorSelection}`);
    },

    'Step 6: [Test - Change Project Priority] - Change Project Priority ': () => {
        const priorities = globalConfig.priorityOptions;
        for (let i in priorities) {
            projectModalPage.changeProjectPriority(priorities[i]);
        }
    },

    'Step 7: [Verify - Change Project Priority]': () => {
        const projectPriorityElement = projectModalPage.getProjectPriorityElement(globalConfig.priorityOptions.critical);
        projectModalPage.expect.element(projectPriorityElement).to.have.attribute('class', 'option selected');
    },

    'Step 8: Return Original Project Priority': () => {
        projectModalPage.changeProjectPriority(`${globalConfig.priorityOptions.medium}`);
    },

    'Step 9: [Test - Change Project Life Cycle] - Change Project Life Cycle Status': () => {
        projectModalPage.changeLifeCycleStage(`${globalConfig.lifeCycleStages.planning}`);
    },

    'Step 10: [Verify - Change Project Life Cycle]': () => {
        const lifeCycleElement = projectModalPage.getLifeCycleElement(globalConfig.lifeCycleStages.planning);
        projectModalPage.expect.element(lifeCycleElement).text.to.contain(`${globalConfig.lifeCycleStages.planning}`);
    },

    'Step 11: Return Original Project Name': () => {
        projectModalPage.changeLifeCycleStage(`${globalConfig.lifeCycleStages.created}`);
    },

    'Step 12: [Test - Assign Project] - Reassign Project To Other User': () => {
        projectModalPage.assignProject(assigneeUser);
    },

    'Step 13: [Verify - Assign Project]': () => {
        projectModalPage.expect.element(kanbanViewPage.elements.assigneeContainerText).text.to.contain(assigneeUser);
    },

    'Step 14: Reassign Project To Original User': () => {
        projectModalPage.assignProject(originalUser);
        projectModalPage.expect.element(kanbanViewPage.elements.assigneeContainerText).text.to.contain(originalUser);
    },

    'Step 15: [Test - Add Project Tag] - Add Project Tag': () => {
        projectModalPage.addProjectTag(`${newTag}`);
    },

    'Step 16: [Verify - Add Project Tag]': () => {
        const projectTagElement = projectModalPage.getProjectTagElement(`${newTag}`);
        projectModalPage.expect.element(projectTagElement).to.be.present.before(globalConfig.timeOut);
    },

    'Step 17: Delete Project Tag': () => {
        projectModalPage.deleteProjectTag(`${newTag}`);
    },

    'Step 18: [Test - Change Project Life Cycle] - Change Project Life Cycle Status': () => {
        projectModalPage.changeLifeCycleStage(`${globalConfig.lifeCycleStages.planning}`);
    },

    'Step 19: [Verify - Change Project Life Cycle]': () => {
        const lifeCycleElement = projectModalPage.getLifeCycleElement(globalConfig.lifeCycleStages.planning);
        projectModalPage.expect.element(lifeCycleElement).text.to.contain(`${globalConfig.lifeCycleStages.planning}`);
    },

    'Step 20: Return Original Project Stage': () => {
        projectModalPage.changeLifeCycleStage(`${globalConfig.lifeCycleStages.created}`);
    },

    'Step 21: [Test - Open Project Data] - Open Project Data': () => {
        projectModalPage.clickMoreProjectData();
    },

    'Step 22: [Verify - Open Project Data]': () => {
        projectModalPage.expect.element('@moreProjectDataModal').to.be.present.before(globalConfig.timeOut);
    }
};
