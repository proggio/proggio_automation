let kanbanViewPage, proggioMainPage, taskManagementPage,
    activityPage, projectModalPage, budgetPage;
const globalConfig = require('../../../main/utilities/globals');
const vendorSelection = 'Vendor selection';
let projectTitle;
module.exports = {
    '@tags': ['fullRun', 'allProjectsTests', 'listViewTests', 'kanbanProjectMenuModal'],
    before(browser) {
        console.log('Loading element pages...');
        proggioMainPage = browser.page.proggioMainPage();
        kanbanViewPage = browser.page.kanbanViewPage();
        projectModalPage = browser.page.projectModalPage();
        activityPage = browser.page.activityPage();
        taskManagementPage = browser.page.taskManagementPage();
        budgetPage = browser.page.budgetPage();
        browser.login(globalConfig.kanban_view_account, globalConfig.generic_password);
    },

    'Step 1: [Test - Project Settings] - Click On Kanban View': () => {
        proggioMainPage.allProjectsViewPicker(globalConfig.allProjects.views.kanbanView);
    },

    'Step 2: Open Project Settings': () => {
        kanbanViewPage.openProjectSettings(`${vendorSelection}`, globalConfig.contextMenu.settings);
    },

    'Step 3: [Verify - Project Settings]': (browser) => {
        projectTitle = projectModalPage.getProjectTitle();
        browser.assert.attributeContains(projectTitle, 'value', vendorSelection);
    },

    'Step 4: Close Project Settings': (browser) => {
        projectModalPage.closeProjectSettings();
        browser.expect.element(projectTitle).to.not.be.present.before(globalConfig.timeOut);
    },

    'Step 5: [Test - Project Time Line] - Click On "Project Time Line"': () => {
        kanbanViewPage.openProjectSettings(`${vendorSelection}`, `${globalConfig.contextMenu.timeline}`);
    },

    'Step 6: [Verify - Project Time Line] - Open Project Time Line': () => {
        const title = activityPage.getProjectTitleElement(`${vendorSelection}`);
        activityPage.expect.element(title).to.be.visible.before(globalConfig.timeOut);
    },

    'Step 7: Back To All Projects Kanban View': () => {
        proggioMainPage.allProjectsViewPicker(globalConfig.allProjects.views.kanbanView);
    },

    'Step 8: [Test - Project Task List] - Click On "Project Task List"': () => {
        kanbanViewPage.openProjectSettings(`${vendorSelection}`, `${globalConfig.contextMenu.taskList}`);
    },

    'Step 9: [Verify - Project Task List] - Open Project Task List': () => {
        taskManagementPage.expect.element('@allTasksTab').to.be.visible.before(globalConfig.timeOut);
        taskManagementPage.expect.element('@listViewSelected').to.be.visible.before(globalConfig.timeOut);
    },

    'Step 10: Back To All Projects Kanban View': () => {
        proggioMainPage.allProjectsViewPicker(globalConfig.allProjects.views.kanbanView);
    },

    'Step 11: [Test - Project Task Board] - Click On "Project Task Board"': () => {
        kanbanViewPage.openProjectSettings(`${vendorSelection}`, `${globalConfig.contextMenu.taskBoard}`);
    },

    'Step 12: [Verify - Project Task List] - Open Project Task Board': () => {
        taskManagementPage.expect.element('@allTasksTab').to.be.visible.before(globalConfig.timeOut);
        taskManagementPage.expect.element('@boardViewSelected').to.be.visible.before(globalConfig.timeOut);
    },

    'Step 13: Back To All Projects Kanban View': () => {
        proggioMainPage.allProjectsViewPicker(globalConfig.allProjects.views.kanbanView);
    },

    'Step 14: [Test - Project Budget] - Click On "Project Budget"': () => {
        kanbanViewPage.openProjectSettings(`${vendorSelection}`, `${globalConfig.contextMenu.budget}`);
    },

    'Step 15: [Verify - Project Budget] - Open Project Budget': () => {
        budgetPage.expect.element('@projectBudgetButton').to.be.visible.before(globalConfig.timeOut);
    },

    'Step 16: [Test - Project Duplication] - Click On "Duplication" Option': () => {
        proggioMainPage.allProjectsViewPicker(globalConfig.allProjects.views.kanbanView);
    },

    'Step 17: Click Duplication Button': () => {
        kanbanViewPage.openProjectSettings(`${vendorSelection}`, `${globalConfig.contextMenu.duplication}`);
        kanbanViewPage.clickOnElement('@duplicateButton');
        proggioMainPage.expect.element('@spinner').to.not.be.present.before(globalConfig.timeOut);
    },

    'Step 18: [Verify - Project Duplication]': (browser) => {
        proggioMainPage.allProjectsViewPicker(globalConfig.allProjects.views.kanbanView);
        const projectElement = kanbanViewPage.getProjectName(`${vendorSelection} copy`);
        browser.expect.element(projectElement).to.be.present.before(globalConfig.timeOut);
    },

    'Step 19: Delete Duplicated Project': () => {
        kanbanViewPage.projectMenuAction(`${vendorSelection} copy`, 'delete');
    },

    'Step 20: [Test - Project Archive]': () => {
        kanbanViewPage.projectMenuAction(`${vendorSelection}`, 'archive');
    },

    'Step 21: [Verify - Project Archive]': () => {
        proggioMainPage.allProjectsViewPicker(globalConfig.allProjects.views.listView);
        kanbanViewPage.expect.element('@archivedProjectsCollapse').to.be.present.before(globalConfig.timeOut);
        kanbanViewPage.clickOnElement('@archivedProjectsCollapse');
        kanbanViewPage.projectMenuAction(`${vendorSelection}`, 'unArchive');
        kanbanViewPage.expect.element('@archivedProjectsCollapse').to.not.be.present.before(globalConfig.timeOut);
    }
};
