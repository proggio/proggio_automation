let listViewPage, proggioMainPage;
const allProjectsView = 'All Projects';
const globalConfig = require('../../../main/utilities/globals');
module.exports = {
    '@tags': ['fullRun', 'allProjectsTests', 'listViewTests', 'addAllColumns'],
    before(browser) {
        console.log('Loading element pages...');
        proggioMainPage = browser.page.proggioMainPage();
        listViewPage = browser.page.listViewPage();
        browser.login(globalConfig.list_view_account, globalConfig.generic_password);
    },

    'Step 1: Click On List View': () => {
        proggioMainPage.allProjectsViewPicker(globalConfig.allProjects.views.listView);
    },

    'Step 1.1: Click On "All Projects" View': () => {
        listViewPage.changeView(`${allProjectsView}`);
    },

    'Step 2: Add All Columns': async() => {
        listViewPage.toggleAddAllColumns('check');
    },

    'Step 3: Verify All Columns': async(browser) => {
        const list = await listViewPage.getColumnsNameTableList();
        for (let i = 0; i < list.length; i++) {
            const tableColumns = `div[class = "ReactVirtualized__Table__headerRow"] div[aria-label]:nth-child(${i + 2})`;
            browser.expect.element(tableColumns).text.to.contain(list[i]).before(globalConfig.timeOut);
        }
    },

    'Step 4: Remove Columns': () => {
        listViewPage.toggleAddAllColumns('uncheck');
    }
};
