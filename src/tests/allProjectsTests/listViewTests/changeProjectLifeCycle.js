let listViewPage, proggioMainPage;
const globalConfig = require('../../../main/utilities/globals');
const electricDevice = 'Electronic Device';
module.exports = {
    '@tags': ['fullRun', 'allProjectsTests', 'listViewTests', 'changeProjectLifeCycle'],
    before(browser) {
        console.log('Loading element pages...');
        proggioMainPage = browser.page.proggioMainPage();
        listViewPage = browser.page.listViewPage();
        browser.login(globalConfig.list_view_account, globalConfig.generic_password);
    },

    'Step 1: Click On List View': () => {
        proggioMainPage.allProjectsViewPicker(globalConfig.allProjects.views.listView);
    },

    'Step 2: Change Project Life Cycle Stage Status': () => {
        listViewPage.changeLifeCycleStage(
            `${electricDevice}`,
            `${globalConfig.lifeCycleStages.prioritization}`
        );
    },

    'Step 3: Verify Project Life Cycle Stage Changed': () => {
        const lifeCycleStatusElement = listViewPage.getProjectLifeCycleStatusElement(`${electricDevice}`);
        listViewPage.expect.element(lifeCycleStatusElement).text.to.contain(globalConfig.lifeCycleStages.prioritization);
    },

    'Step 4: Change Back Project Life Cycle Stage Status': () => {
        listViewPage.changeLifeCycleStage(
            `${electricDevice}`,
            `${globalConfig.lifeCycleStages.created}`
        );
    }
};
