let proggioMainPage, projectModalPage, listViewPage;
const globalConfig = require('../../../main/utilities/globals');
const electronicDeviceProject = 'Electronic Device';
const customProjectName = 'New Project';
const originalUser = 'vladi.b+list_View';
const assigneeUser = 'P-Bot';
const newTag = 'Electronic';
module.exports = {
    '@tags': ['fullRun', 'allProjectsTests', 'tilesViewTests', 'listProjectSettingsModal'],
    before(browser) {
        console.log('Loading element pages...');
        proggioMainPage = browser.page.proggioMainPage();
        projectModalPage = browser.page.projectModalPage();
        listViewPage = browser.page.listViewPage();
        browser.login(globalConfig.list_view_account, globalConfig.generic_password);
    },

    'Step 1: [Test - Project Settings] - Open Project Settings By Menu': () => {
        proggioMainPage.allProjectsViewPicker(globalConfig.allProjects.views.listView);
    },

    'Step 2: Open Project Settings': () => {
        listViewPage.openProjectSettings(`${electronicDeviceProject}`, `${globalConfig.contextMenu.settings}`);
    },

    'Step 3: [Test - Change Project Name] - Change Project Name': () => {
        projectModalPage.changeProjectName(`${customProjectName}`);
    },

    'Step 4: [Verify - Change Project Name]': () => {
        projectModalPage.expect.element('@projectNameInput').to.have.attribute('value', `${customProjectName}`);
    },

    'Step 5: Return Original Project Name': () => {
        projectModalPage.changeProjectName(`${electronicDeviceProject}`);
    },

    'Step 6: [Test - Change Project Priority] - Change Project Priority ': () => {
        const priorities = globalConfig.priorityOptions;
        for (let i in priorities) {
            projectModalPage.changeProjectPriority(priorities[i]);
        }
    },

    'Step 7: [Verify - Change Project Priority]': () => {
        const projectPriorityElement = projectModalPage.getProjectPriorityElement(globalConfig.priorityOptions.critical);
        projectModalPage.expect.element(projectPriorityElement).to.have.attribute('class', 'option selected');
    },

    'Step 8: Return Original Project Priority': () => {
        projectModalPage.changeProjectPriority(`${globalConfig.priorityOptions.medium}`);
    },

    'Step 9: [Test - Change Project Life Cycle] - Change Project Life Cycle Status': () => {
        projectModalPage.changeLifeCycleStage(`${globalConfig.lifeCycleStages.planning}`);
    },

    'Step 10: [Verify - Change Project Life Cycle]': () => {
        const lifeCycleElement = projectModalPage.getLifeCycleElement(globalConfig.lifeCycleStages.planning);
        projectModalPage.expect.element(lifeCycleElement).text.to.contain(`${globalConfig.lifeCycleStages.planning}`);
    },

    'Step 11: Return Original Project Stage': () => {
        projectModalPage.changeLifeCycleStage(`${globalConfig.lifeCycleStages.created}`);
    },

    'Step 12: [Test - Assign Project] - Reassign Project To Other User': () => {
        projectModalPage.assignProject(assigneeUser);
    },

    'Step 13: [Verify - Assign Project]': () => {
        projectModalPage.expect.element(listViewPage.elements.assigneeContainerText).text.to.contain(assigneeUser);
    },

    'Step 14: Reassign Project To Original User': () => {
        projectModalPage.assignProject(originalUser);
        projectModalPage.expect.element(listViewPage.elements.assigneeContainerText).text.to.contain(originalUser);
    },

    'Step 15: [Test - Add Project Tag] - Add Project Tag': () => {
        projectModalPage.addProjectTag(`${newTag}`);
    },

    'Step 16: [Verify - Add Project Tag]': () => {
        const projectTagElement = projectModalPage.getProjectTagElement(`${newTag}`);
        projectModalPage.expect.element(projectTagElement).to.be.present.before(globalConfig.timeOut);
    },

    'Step 17: Delete Project Tag': () => {
        projectModalPage.deleteProjectTag(`${newTag}`);
    },

    'Step 18: [Test - Open Project Data] - Open Project Data': () => {
        projectModalPage.clickMoreProjectData();
    },

    'Step 19: [Verify - Open Project Data]': () => {
        projectModalPage.expect.element('@moreProjectDataModal').to.be.present.before(globalConfig.timeOut);
    }
};
