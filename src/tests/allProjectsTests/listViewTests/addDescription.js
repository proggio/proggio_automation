let listViewPage, proggioMainPage, projectModalPage;
const allProjectsView = 'All Projects';
const globalConfig = require('../../../main/utilities/globals');
const HRProject = 'HR: Company event';
const InputText = 'This is a test';
module.exports = {
    '@tags': ['fullRun', 'allProjectsTests', 'listViewTests', 'addDescription'],
    before(browser) {
        console.log('Loading element pages...');
        proggioMainPage = browser.page.proggioMainPage();
        listViewPage = browser.page.listViewPage();
        projectModalPage = browser.page.projectModalPage();
        browser.login(globalConfig.list_view_account, globalConfig.generic_password);
    },

    'Step 1: Click On List View': () => {
        proggioMainPage.allProjectsViewPicker(globalConfig.allProjects.views.listView);
    },

    'Step 2: Click On "All Projects" View': () => {
        listViewPage.changeView(`${allProjectsView}`);
    },

    'Step 3: Add Project Description': () => {
        listViewPage.toggleAddColumn(`Description`);
        projectModalPage.addDescription(
            `${HRProject}`,
            `${InputText}`,
            false
        );
    },

    'Step 4: Verify Project Description': () => {
        const descriptionElement = projectModalPage.getProjectDescription(`${HRProject}`);
        projectModalPage.expect.element(descriptionElement).text.to.contain(`${InputText}`);
    },

    'Step 5: Delete Project Description': () => {
        projectModalPage.addDescription(
            `${HRProject}`,
            ``,
            true
        );
    }
};
