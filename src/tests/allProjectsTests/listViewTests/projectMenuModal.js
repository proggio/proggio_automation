let listViewPage, proggioMainPage, taskManagementPage,
    activityPage, projectModalPage, budgetPage;
const globalConfig = require('../../../main/utilities/globals');
const hrCompanyEvent = 'HR: Company event';
module.exports = {
    '@tags': ['fullRun', 'allProjectsTests', 'listViewTests', 'listProjectMenuModal'],
    before(browser) {
        console.log('Loading element pages...');
        proggioMainPage = browser.page.proggioMainPage();
        listViewPage = browser.page.listViewPage();
        projectModalPage = browser.page.projectModalPage();
        activityPage = browser.page.activityPage();
        taskManagementPage = browser.page.taskManagementPage();
        budgetPage = browser.page.budgetPage();
        browser.login(globalConfig.list_view_account, globalConfig.generic_password);
    },

    'Step 1: [Test - Project Settings - Button Click] - Click On List View': () => {
        proggioMainPage.allProjectsViewPicker(globalConfig.allProjects.views.listView);
    },

    'Step 2: Open Project Settings By Button': () => {
        listViewPage.openProjectSettingsByButton(`${hrCompanyEvent}`);
    },

    'Step 3: [Verify - Project Settings - By Button] - Open Project Settings By Button': (browser) => {
        const projectTitle = projectModalPage.getProjectTitle();
        browser.assert.attributeContains(projectTitle, 'value', hrCompanyEvent);
    },

    'Step 4: Close Project Settings': () => {
        projectModalPage.closeProjectSettings();
    },

    'Step 5: [Test - Project Settings - Mouse Right Click] - Open Project Settings By Menu': () => {
        listViewPage.openProjectSettings(`${hrCompanyEvent}`, globalConfig.contextMenu.settings);
    },

    'Step 6: [Verify - Project Settings - Mouse Right Click] - Open Project Settings By Menu': (browser) => {
        const projectTitle = projectModalPage.getProjectTitle();
        browser.assert.attributeContains(projectTitle, 'value', hrCompanyEvent);
    },

    'Step 7: Close Project Settings': () => {
        projectModalPage.closeProjectSettings();
    },

    'Step 8: Click On List View': () => {
        proggioMainPage.allProjectsViewPicker(globalConfig.allProjects.views.listView);
    },

    'Step 9: [Test - Project Time Line] - Click On "Project Time Line"': () => {
        listViewPage.openProjectSettings(`${hrCompanyEvent}`, `${globalConfig.contextMenu.timeline}`);
    },

    'Step 10: [Verify - Project Time Line] - Open Project Time Line': () => {
        const title = activityPage.getProjectTitleElement(`${hrCompanyEvent}`);
        activityPage.expect.element(title).to.be.visible.before(globalConfig.timeOut);
    },

    'Step 11: Back To All Projects List View': () => {
        proggioMainPage.allProjectsViewPicker(globalConfig.allProjects.views.listView);
    },

    'Step 12: [Test - Project Task List] - Click On "Project Task List"': () => {
        listViewPage.openProjectSettings(`${hrCompanyEvent}`, `${globalConfig.contextMenu.taskList}`);
    },

    'Step 13: [Verify - Project Task List] - Open Project Task List': () => {
        taskManagementPage.expect.element('@allTasksTab').to.be.visible.before(globalConfig.timeOut);
        taskManagementPage.expect.element('@listViewSelected').to.be.visible.before(globalConfig.timeOut);
    },

    'Step 14: Back To All Projects List View': () => {
        proggioMainPage.allProjectsViewPicker(globalConfig.allProjects.views.listView);
    },

    'Step 15: [Test - Project Task Board] - Click On "Project Task Board"': () => {
        listViewPage.openProjectSettings(`${hrCompanyEvent}`, `${globalConfig.contextMenu.taskBoard}`);
    },

    'Step 16: [Verify - Project Task List] - Open Project Task Board': () => {
        taskManagementPage.expect.element('@allTasksTab').to.be.visible.before(globalConfig.timeOut);
        taskManagementPage.expect.element('@boardViewSelected').to.be.visible.before(globalConfig.timeOut);
    },

    'Step 17: Back To All Projects List View': () => {
        proggioMainPage.allProjectsViewPicker(globalConfig.allProjects.views.listView);
    },

    'Step 18: [Test - Project Budget] - Click On "Project Budget"': () => {
        listViewPage.openProjectSettings(`${hrCompanyEvent}`, `${globalConfig.contextMenu.budget}`);
    },

    'Step 19: [Verify - Project Budget] - Open Project Budget': () => {
        budgetPage.expect.element('@projectBudgetButton').to.be.visible.before(globalConfig.timeOut);
    },

    'Step 20: [Test - Project Duplication] - Click On "Duplication" Option': () => {
        proggioMainPage.allProjectsViewPicker(globalConfig.allProjects.views.listView);
    },

    'Step 21: Click Duplication Button': () => {
        listViewPage.projectMenuAction(`${hrCompanyEvent}`, 'duplication');
        proggioMainPage.expect.element('@spinner').to.not.be.present.before(globalConfig.timeOut);
    },

    'Step 22: [Verify - Project Duplication]': (browser) => {
        proggioMainPage.allProjectsViewPicker(globalConfig.allProjects.views.listView);
        browser.useXpath();
        const projectElement = listViewPage.getProjectName(`${hrCompanyEvent} copy`);
        browser.expect.element(projectElement).to.be.present.before(globalConfig.timeOut);
    },

    'Step 23: Delete Duplicated Project': () => {
        listViewPage.projectMenuAction(`${hrCompanyEvent} copy`, 'delete');
    },

    'Step 24: [Test - Project Archive]': () => {
        listViewPage.projectMenuAction(`${hrCompanyEvent}`, 'archive');
    },

    'Step 25: [Verify - Project Archive]': () => {
        listViewPage.expect.element('@archivedProjectsCollapse').to.be.present.before(globalConfig.timeOut);
        listViewPage.clickOnElement('@archivedProjectsCollapse');
        listViewPage.projectMenuAction(`${hrCompanyEvent}`, 'unArchive');
        listViewPage.expect.element('@archivedProjectsCollapse').to.not.be.present.before(globalConfig.timeOut);
    },

    'Step 26: [Test - Create Template]': () => {
        listViewPage.projectMenuAction(`${hrCompanyEvent}`, 'createTemplate');
    },

    'Step 27: [Verify - Create Template]': () => {
        listViewPage.deleteProjectTemplate(`${hrCompanyEvent}`);
    }
};
