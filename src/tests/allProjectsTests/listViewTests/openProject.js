let listViewPage, proggioMainPage, activityPage;
const globalConfig = require('../../../main/utilities/globals');
const electricDevice = 'Electronic Device';
module.exports = {
    '@tags': ['fullRun', 'allProjectsTests', 'listViewTests', 'openProject'],
    before(browser) {
        console.log('Loading element pages...');
        proggioMainPage = browser.page.proggioMainPage();
        listViewPage = browser.page.listViewPage();
        activityPage = browser.page.activityPage();
        browser.login(globalConfig.list_view_account, globalConfig.generic_password);
    },

    'Step 1: Click On List View': () => {
        proggioMainPage.allProjectsViewPicker(globalConfig.allProjects.views.listView);
    },

    'Step 2: Open Project By Clicking Name': () => {
        listViewPage.openProjectByName(`${electricDevice}`);
    },

    'Step 3: Verify Project Opened': (browser) => {
        const titleElement = activityPage.getProjectTitleElement(electricDevice);
        browser.expect.element(titleElement).text.to.contain(electricDevice.toUpperCase());
    },

    'Step 4: Return To List View': () => {
        proggioMainPage.allProjectsViewPicker(globalConfig.allProjects.views.listView);
    },

    'Step 5: Open Project By Clicking Button': () => {
        listViewPage.openProjectByButton(`${electricDevice}`);
    },

    'Step 6: Verify Project Opened': (browser) => {
        const titleElement = activityPage.getProjectTitleElement(electricDevice);
        browser.expect.element(titleElement).text.to.contains(electricDevice.toUpperCase());
    }
};
