let listViewPage, kanbanViewPage, activityPage, tilesViewPage, proggioMainPage;
const globalConfig = require('../../main/utilities/globals');
let projectName = globalConfig.automation_testing_account.split('@', 1);
module.exports = {
    '@tags': ['fullRun', 'allProjectsTests', 'moveBetweenViews'],
    before(browser) {
        console.log('Loading element pages...');
        tilesViewPage = browser.page.tilesViewPage();
        activityPage = browser.page.activityPage();
        listViewPage = browser.page.listViewPage();
        kanbanViewPage = browser.page.kanbanViewPage();
        proggioMainPage = browser.page.proggioMainPage();
        browser.login(globalConfig.automation_testing_account, globalConfig.generic_password);
    },

    'Step 1: Delete All Projects': async() => {
        await tilesViewPage.deleteAllProjects();
    },

    'Step 1.1: Create New Project': async() => {
        await tilesViewPage.createNewProject(`${projectName}`, false);
    },

    'Step 2: Click On List View': () => {
        proggioMainPage.allProjectsViewPicker(globalConfig.allProjects.views.listView);
    },

    'Step 3: Verify Project In List View': () => {
        listViewPage.assert.containsTextInElement('@lastProjectText', `${projectName}`);
    },

    'Step 4: Click On Kanban View': () => {
        proggioMainPage.allProjectsViewPicker(globalConfig.allProjects.views.kanbanView);
    },

    'Step 5: Verify Project In Kanban View': () => {
        kanbanViewPage.assert.containsTextInElement('@lastProjectCardText', `${projectName}`);
    },

    'Step 6: Open Project in Kanban View': (browser) => {
        kanbanViewPage.waitForElementVisible('@lastProjectCard');
        browser.moveToElement('css selector', kanbanViewPage.elements.lastProjectCard, 1, 1, function() {
            browser.doubleClick();
        });
    },

    'Step 7: Verify Project Opened': () => {
        activityPage.assert.containsTextInElement('@projectGridToday', 'Today');
    }

};
