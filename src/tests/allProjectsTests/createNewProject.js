let workStreamPage, proggioMainPage, tilesViewPage;
const globalConfig = require('../../main/utilities/globals');
const projectName = globalConfig.automation_testing_account.split('@', 1);
const project = `div[id = "${projectName}"]`;
let size, height, width;

module.exports = {
    '@tags': ['fullRun', 'allProjectsTests', 'createNewProject'],
    before(browser) {
        console.log('Loading element pages...');
        workStreamPage = browser.page.workStreamPage();
        proggioMainPage = browser.page.proggioMainPage();
        tilesViewPage = browser.page.tilesViewPage();
        browser.login(globalConfig.automation_testing_account, globalConfig.generic_password);
    },

    'Step 1: Delete All Projects': async() => {
        await tilesViewPage.deleteAllProjects();
    },

    'Step 1.1: Create New Project': async() => {
        await tilesViewPage.createNewProject(`${projectName}`, true);
    },

    "Step 2: Verify New Project's Creation": () => {
        workStreamPage.expect.element('@addNewWorkStreamButton').to.be.visible.before(globalConfig.timeOut);
    },

    "Step 3: Cancel Project Deletion by 'Undo' Button": async(browser) => {
        proggioMainPage.clickOnElement('@allProjects');
        size = await browser.getElementSize('css selector', project);
        height = size.value.height;
        width = size.value.width;
        await browser.moveToElement('css selector', project, width / 2, height / 2);
        await browser.mouseButtonClick(2);
        tilesViewPage.clickOnElement('@delete');
        tilesViewPage.clickOnElement('@confirmDelete');
        proggioMainPage.expect.element('@alertDark').to.be.present.before(globalConfig.timeOut);
        proggioMainPage.clickOnElement('@undoButton');
        tilesViewPage.expect.element(tilesViewPage.elements.projectCardByName).to.be.visible.before(globalConfig.timeOut);
    },

    'Step 4: Cancel Project Deletion by Refresh': async(browser) => {
        browser.pause(2000);
        proggioMainPage.clickOnElement('@allProjects');
        size = await browser.getElementSize('css selector', project);
        height = size.value.height;
        width = size.value.width;
        await browser.moveToElement('css selector', project, width / 2, height / 2);
        await browser.mouseButtonClick(2);
        tilesViewPage.clickOnElement('@delete');
        tilesViewPage.clickOnElement('@confirmDelete');
        proggioMainPage.expect.element('@alertDark').to.be.present.before(globalConfig.timeOut);
        browser.refresh();
        tilesViewPage.expect.element(tilesViewPage.elements.projectCardByName).to.be.visible.before(globalConfig.timeOut);
    }

};
