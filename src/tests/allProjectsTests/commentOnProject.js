let tilesViewPage, proggioMainPage,
    activityPage, chatModalPage;
const globalConfig = require('../../main/utilities/globals');
const projectTitle = 'Assignation Project';
module.exports = {
    '@tags': ['fullRun', 'allProjectsTests', 'commentOnProject'],
    before: function(browser) {
        console.log('Loading element pages...');
        tilesViewPage = browser.page.tilesViewPage();
        activityPage = browser.page.activityPage();
        proggioMainPage = browser.page.proggioMainPage();
        chatModalPage = browser.page.chatModalPage();
        browser.login(globalConfig.account_add_user, globalConfig.generic_password);
    },

    'Step 1: Click On Project': () => {
        proggioMainPage.allProjectsViewPicker('tiles');
        tilesViewPage.enterToProject(`${projectTitle}`);
    },

    'Step 2: Open Project Chat': () => {
        activityPage.openProjectChat();
    },

    'Step 3: Send Message': () => {
        chatModalPage.sendMessage('Hello! Sending a message to a project');
    },

    'Step 4: Verify Message Sent': async() => {
        const lastMessage = await chatModalPage.getLastMessageContent();
        chatModalPage.expect.element('@lastMessage').text.to.contain(lastMessage.value);
    },

    'Step 5: Delete Message': () => {
        chatModalPage.deleteMessage('project');
        chatModalPage.expect.element('@lastMessage').to.not.be.present.before(globalConfig.timeOut);
        chatModalPage.closeChatBox();
    }
};
