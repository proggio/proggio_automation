let proggioMainPage, splitPage,
    activityPage, subProjectPage, generalPopoverPage,
    tilesViewPage, workStreamPage;
const globalConfig = require('../../main/utilities/globals');
let projectName = globalConfig.automation_testing_account.split('@', 1);
let splitGripLines, workStreamCard;
let messagingIcon = 'span[class = "cc-7doi cc-1ada"]';
let activity = 'Activity Test';
module.exports = {
    '@tags': ['fullRun', 'allProjectsTests', 'createSubProject'],
    before: function(browser) {
        console.log('Loading element pages...');
        generalPopoverPage = browser.page.generalPopoverPage();
        tilesViewPage = browser.page.tilesViewPage();
        activityPage = browser.page.activityPage();
        proggioMainPage = browser.page.proggioMainPage();
        splitPage = browser.page.splitPage();
        subProjectPage = browser.page.subProjectPage();
        workStreamPage = browser.page.workStreamPage();
        splitGripLines = splitPage.elements.splitGripLines;
        workStreamCard = workStreamPage.elements.firstWorkStreamCard;
        browser.login(globalConfig.automation_testing_account, globalConfig.generic_password);
    },

    'Step 1: Delete All Projects': async() => {
        await tilesViewPage.deleteAllProjects();
    },

    'Step 1.1: Create New Project': async() => {
        await tilesViewPage.createNewProject(`${projectName}`, false);
    },

    'Step 1.2: Create Another Project': async() => {
        await tilesViewPage.createNewProject(`From Existing Project`, true);
    },

    'Step 2: Create Activity In Project': async() => {
        await activityPage.createActivity();
        await proggioMainPage.expect.element('@activitySpinner').to.not.be.present.before(globalConfig.timeOut);
    },

    'Step 3: Move To Main Project': () => {
        proggioMainPage.allProjectsViewPicker(globalConfig.allProjects.views.tilesView);
        tilesViewPage.openProject(`${projectName}`);
    },

    'Step 4: Create Activity In Main Project': async() => {
        await activityPage.createActivity();
        proggioMainPage.expect.element('@activitySpinner').to.not.be.present.before(globalConfig.timeOut);
    },

    'Step 5: Rename Activity': (browser) => {
        activityPage.clickOnElement('@activityCard');
        activityPage.setValueInElement('@activityCardInput', activity);
        browser.keys([browser.Keys.ENTER]);
        splitPage.expect.element('@addTaskButton').to.be.visible.before(globalConfig.timeOut);
    },

    'Step 6: Open Activity Split': async(browser) => {
        await browser.dragAndDrop('css selector', splitGripLines, workStreamCard);
    },

    "Step 7: Click 'Sub Project' Tab": () => {
        splitPage.clickOnElement('@subProjectTab');
    },

    "Step 8: Click 'Blank' Option": () => {
        subProjectPage.clickOnElement('@blankButton');
    },

    'Step 9: Name Your Sub Project': () => {
        subProjectPage.clearValue('css selector', '@blankInput');
        subProjectPage.setValueInElement('@blankInput', 'Create Sub Project Test');
    },

    'Step 10: Create Sub Project': () => {
        subProjectPage.clickOnElement('@createProject');
        subProjectPage.expect.element('@gridSplitProject').to.be.visible.before(globalConfig.timeOut);
    },

    'Step 11: Delete Sub Project From Activity': async(browser) => {
        const splitGripLines = splitPage.elements.splitGripLines;
        await browser.dragAndDrop('css selector', splitGripLines, messagingIcon);
        const id = await activityPage.getActivityId(activity);
        await activityPage.openActivitySettingsById(activityPage, `${id}`, '@deleteSubProject');
        await generalPopoverPage.clickOnElement('@deleteButton');
        await proggioMainPage.clickOnElement('@alertDarkCloseButton');
    },

    'Step 12: Click On Activity': async() => {
        activityPage.clickOnElement('@activityCard');
    },

    "Step 13: Click 'From Existing Project' Option": async(browser) => {
        await browser.dragAndDrop('css selector', splitGripLines, workStreamCard);
        splitPage.clickOnElement('@subProjectTab');
        subProjectPage.clickOnElement('@fromExistingProjectButton');
    },

    'Step 14: Create Sub Project': async() => {
        await subProjectPage.selectProjectFromExisting('From Existing Project');
        subProjectPage.expect.element('@gridSplitProject').to.be.visible.before(globalConfig.timeOut);
    },

    'Step 15: Delete Sub Project From Activity': async(browser) => {
        const splitGripLines = splitPage.elements.splitGripLines;
        await browser.dragAndDrop('css selector', splitGripLines, messagingIcon);
        const id = await activityPage.getActivityId(activity);
        await activityPage.openActivitySettingsById(activityPage, `${id}`, '@deleteSubProject');
        await generalPopoverPage.clickOnElement('@deleteButton');
        await proggioMainPage.clickOnElement('@alertDarkCloseButton');
    }
};
