let tilesViewPage;
const globalConfig = require('../../../main/utilities/globals');
let projectName = globalConfig.automation_testing_account.split('@', 1);
module.exports = {
    '@disabled': true,
    '@tags': ['fullRun', 'allProjectsTests', 'tilesViewTests', 'archiveProject'],
    before(browser) {
        console.log('Loading element pages...');
        tilesViewPage = browser.page.tilesViewPage();
        browser.login(globalConfig.automation_testing_account, globalConfig.generic_password);
    },

    'Step 1: Delete All Projects': async() => {
        await tilesViewPage.deleteAllProjects();
    },

    'Step 1.1: Create New Project': async() => {
        await tilesViewPage.createNewProject(`${projectName}`, false);
    },

    "Step 2: Click On Project Settings and 'Archive' Option": () => {
        tilesViewPage.openProjectSettings(projectName, globalConfig.contextMenu.archive);
    },

    'Step 3: Verify Project Archived Collapse Appeared': () => {
        tilesViewPage.expect.element('@archivedProjectsCollapse').to.be.visible;
    },

    'Step 4: Click Project Archived Collapse': () => {
        tilesViewPage.clickOnElement('@archivedProjectsCollapse');
    },

    "Step 5: Verify Project Card 'Archived' Text": () => {
        let expected = 'Archived';
        tilesViewPage.assert.containsTextInElement('@projectCardByNameArchivedText', expected);
    },

    "Step 6: Click On Project Settings and 'Unarchive' Option": () => {
        tilesViewPage.openProjectSettings(projectName, globalConfig.contextMenu.unArchive);
    },

    'Step 7: Verify Project UnArchived': () => {
        tilesViewPage.assert.containsTextInElement('@projectCardByName', `${projectName}`);
    }
};
