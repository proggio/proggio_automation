const globalConfig = require('../../../main/utilities/globals');
let tilesViewPage, proggioMainPage;
let project = 'Duplicate Project Test';
module.exports = {
    '@tags': ['fullRun', 'allProjectsTests', 'tilesViewTests', 'duplicateProject'],
    before: function(browser) {
        console.log('Loading element pages...');

        proggioMainPage = browser.page.proggioMainPage();
        tilesViewPage = browser.page.tilesViewPage();
        browser.login(globalConfig.automation_testing_account, globalConfig.generic_password);
    },

    'Step 1: Delete All Projects': async() => {
        await tilesViewPage.deleteAllProjects();
    },

    'Step 1.1: Create New Project': async() => {
        await tilesViewPage.createNewProject(project, false);
    },

    "Step 2: Click On Project Settings and 'Duplicate' Option": () => {
        proggioMainPage.clickOnElement('@allProjects');
        tilesViewPage.openProjectSettings(project, globalConfig.contextMenu.duplication);
    },

    'Step 3: Duplicate Project': () => {
        tilesViewPage.clickOnElement('@openProjectCheckBox');
        tilesViewPage.clickOnElement('@duplicateButton');
    },

    'Step 4: Verify Duplication Success': () => {
        proggioMainPage.expect.element('@spinner').to.not.be.present.before(globalConfig.timeOut);
        const projectText = tilesViewPage.getProjectName(project + ' copy');
        tilesViewPage.assert.containsTextInElement(projectText, project + ' copy');
    }
};
