let proggioMainPage, taskManagementPage, budgetPage,
    activityPage, projectModalPage, tilesViewPage;
const globalConfig = require('../../../main/utilities/globals');
const eventManagementProject = 'Event Management';
module.exports = {
    '@tags': ['fullRun', 'allProjectsTests', 'tilesViewTests', 'tilesProjectMenuModal'],
    before(browser) {
        console.log('Loading element pages...');
        proggioMainPage = browser.page.proggioMainPage();
        projectModalPage = browser.page.projectModalPage();
        activityPage = browser.page.activityPage();
        taskManagementPage = browser.page.taskManagementPage();
        tilesViewPage = browser.page.tilesViewPage();
        budgetPage = browser.page.budgetPage();
        browser.login(globalConfig.tiles_view_account, globalConfig.generic_password);
    },

    'Step 1: [Test - Project Settings] - Open Project Settings By Menu': () => {
        proggioMainPage.allProjectsViewPicker(globalConfig.allProjects.views.tilesView);
        tilesViewPage.openProjectSettings(`${eventManagementProject}`, `${globalConfig.contextMenu.settings}`);
    },

    'Step 2: [Verify - Project Settings]': () => {
        projectModalPage.expect.element('@projectSettingsText').to.be.present.before(globalConfig.timeOut);
    },

    'Step 3: Close Project Settings': () => {
        projectModalPage.closeProjectSettings();
    },

    'Step 4: Click On Tiles View': () => {
        proggioMainPage.allProjectsViewPicker(globalConfig.allProjects.views.tilesView);
    },

    'Step 5: [Test - Project Time Line] - Click On "Project Time Line"': () => {
        tilesViewPage.openProjectSettings(`${eventManagementProject}`, `${globalConfig.contextMenu.timeline}`);
    },

    'Step 6: [Verify - Project Time Line] - Open Project Time Line': () => {
        const title = activityPage.getProjectTitleElement(`${eventManagementProject}`);
        activityPage.expect.element(title).to.be.visible.before(globalConfig.timeOut);
    },

    'Step 7: Back To All Projects Tiles View': () => {
        proggioMainPage.allProjectsViewPicker(globalConfig.allProjects.views.tilesView);
    },

    'Step 8: [Test - Project Task List] - Click On "Project Task List"': () => {
        tilesViewPage.openProjectSettings(`${eventManagementProject}`, `${globalConfig.contextMenu.taskList}`);
    },

    'Step 9: [Verify - Project Task List] - Open Project Task List': () => {
        taskManagementPage.expect.element('@allTasksTab').to.be.visible.before(globalConfig.timeOut);
        taskManagementPage.expect.element('@listViewSelected').to.be.visible.before(globalConfig.timeOut);
    },

    'Step 10: Back To All Projects List View': () => {
        proggioMainPage.allProjectsViewPicker(globalConfig.allProjects.views.tilesView);
    },

    'Step 11: [Test - Project Task Board] - Click On "Project Task Board"': () => {
        tilesViewPage.openProjectSettings(`${eventManagementProject}`, `${globalConfig.contextMenu.taskBoard}`);
    },

    'Step 12: [Verify - Project Task List] - Open Project Task Board': () => {
        taskManagementPage.expect.element('@allTasksTab').to.be.visible.before(globalConfig.timeOut);
        taskManagementPage.expect.element('@boardViewSelected').to.be.visible.before(globalConfig.timeOut);
    },

    'Step 13: Back To All Projects List View': () => {
        proggioMainPage.allProjectsViewPicker(globalConfig.allProjects.views.tilesView);
    },

    'Step 14: [Test - Project Budget] - Click On "Project Budget"': () => {
        tilesViewPage.openProjectSettings(`${eventManagementProject}`, `${globalConfig.contextMenu.budget}`);
    },

    'Step 15: [Verify - Project Budget] - Open Project Budget': () => {
        budgetPage.expect.element('@projectBudgetButton').to.be.visible.before(globalConfig.timeOut);
    }
};
