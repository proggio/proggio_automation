let tilesViewPage, proggioMainPage,
    activityPage, bufferPage;
const globalConfig = require('../../../main/utilities/globals');
const assert = require('assert');
const projectTitle = 'Move Items Project';
let bufferId = 'map_activity_id_338913';
let activityStartingWidth, activityEndingWidth;
const xOffsetNum = 20;

module.exports = {
    '@tags': ['projectMapTests', 'bufferTests', 'sanity', 'fullRun'],
    before: function(browser) {
        console.log('Loading element pages...');
        tilesViewPage = browser.page.tilesViewPage();
        bufferPage = browser.page.bufferPage();
        activityPage = browser.page.activityPage();
        proggioMainPage = browser.page.proggioMainPage();
        browser.login(globalConfig.account_add_user, globalConfig.generic_password);
    },

    'Step 1: Click On Project': () => {
        proggioMainPage.clickOnElement('@allProjects');
        tilesViewPage.enterToProject(`${projectTitle}`);
    },

    'Step 2: Move Buffer and Verify': async() => {
        activityStartingWidth = await activityPage.getActivityXAxisLocation(bufferId);
        await activityPage.moveActivityByOffset(bufferId, xOffsetNum);
        activityEndingWidth = await activityPage.getActivityXAxisLocation(bufferId);
        assert.strict.notEqual(activityStartingWidth, activityEndingWidth);
    },

    'Step 3: Return Buffer Original Location ': async() => {
        await activityPage.moveActivityByOffset(bufferId, -xOffsetNum);
        activityEndingWidth = await activityPage.getActivityXAxisLocation(bufferId);
    },

    'Step 4: Resize Buffer - Dragging': async() => {
        activityStartingWidth = await activityPage.getActivityWidth(bufferId);
        await activityPage.resizeActivity(bufferId, 'right', xOffsetNum);
        activityEndingWidth = await activityPage.getActivityWidth(bufferId);
        assert.strict.notEqual(activityStartingWidth, activityEndingWidth);
    },

    'Step 5: Return Buffer Original Width - Dragging': async() => {
        await activityPage.resizeActivity(bufferId, 'right', -xOffsetNum);
        activityEndingWidth = await activityPage.getActivityWidth(bufferId);
    },

    'Step 6: Resize Buffer - Settings - Dates': async() => {
        await bufferPage.openBufferSettings(bufferPage, bufferId, '@info');
        bufferPage.bufferModalDatePicker(
            'end date',
            2021,
            0,
            19
        );
        bufferPage.expect.element('@endDateContainerText').text.to.contain('Jun 19');
    },

    'Step 7: Return Buffer Original Date - Settings - Dates': async() => {
        bufferPage.bufferModalDatePicker(
            'end date',
            2021,
            0,
            20
        );
        bufferPage.expect.element('@endDateContainerText').text.to.contain('Jun 20');
        bufferPage.saveBufferSettings();
    }
};
