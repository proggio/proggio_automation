let tilesViewPage, proggioMainPage,
    activityPage;
let activity = 'Edit Me!';
const globalConfig = require('../../../main/utilities/globals');
let projectName = globalConfig.automation_testing_account.split('@', 1);
module.exports = {
    '@tags': ['projectMapTests', 'bufferTests', 'sanity', 'fullRun', 'createBuffer'],
    before: function(browser) {
        console.log('Loading element pages...');
        tilesViewPage = browser.page.tilesViewPage();
        activityPage = browser.page.activityPage();
        proggioMainPage = browser.page.proggioMainPage();
        browser.login(globalConfig.automation_testing_account, globalConfig.generic_password);
    },

    'Step 1: Delete All Projects': async() => {
        await tilesViewPage.deleteAllProjects();
    },

    'Step 1.1: Create New Project': async() => {
        await tilesViewPage.createNewProject(`${projectName}`, true);
    },

    'Step 2: Create Activity': async function(browser) {
        await activityPage.createActivity();
        await browser.keys([browser.Keys.ENTER]);
        await proggioMainPage.expect.element('@activitySpinner').to.not.be.present.before(globalConfig.timeOut);
    },

    "Step 3: Click On Activity Settings and 'To buffer' Option": async() => {
        const id = await activityPage.getActivityId(activity);
        await activityPage.openActivitySettingsById(activityPage, `${id}`, '@toBuffer');
    },

    'Step 4: Verify Activity In Buffer Mode': async function(browser) {
        const activityId = await browser.getAttribute('xpath', activityPage.elements.activityCard, 'id');
        const activityCard = `//*[name() = "g" and contains(@id, "${activityId.value}")]//*[name() = "rect" and contains(@style, "stripes")]`;
        await browser.useXpath();
        await browser.expect.element(activityCard).to.be.present.before(globalConfig.timeOut);
    },

    'Step 5: Delete Buffer': async() => {
        const id = await activityPage.getActivityId(activity);
        await activityPage.openActivitySettingsById(activityPage, `${id}`, '@delete');
    }

};
