let tilesViewPage, proggioMainPage,
    activityPage, bufferPage;
const globalConfig = require('../../../main/utilities/globals');
const projectTitle = 'Copy Paste Project';
const buffer1 = 'map_activity_id_347555';
module.exports = {
    '@tags': ['projectMapTests', 'bufferTests', 'sanity', 'fullRun'],
    before: function(browser) {
        console.log('Loading element pages...');
        tilesViewPage = browser.page.tilesViewPage();
        activityPage = browser.page.activityPage();
        proggioMainPage = browser.page.proggioMainPage();
        bufferPage = browser.page.bufferPage();
        browser.login(globalConfig.account_add_user, globalConfig.generic_password);
    },

    'Step 1: Click On Project': () => {
        proggioMainPage.clickOnElement('@allProjects');
        tilesViewPage.enterToProject(`${projectTitle}`);
    },

    'Step 2: Copy Buffer': async() => {
        await bufferPage.openBufferSettings(bufferPage,
            `${buffer1}`,
            `@copy`
        );
    },

    'Step 3: Paste Buffer': async() => {
        await activityPage.pasteActivityAndOpen(buffer1, 50);
        proggioMainPage.expect.element('@spinner').to.not.be.present.before(globalConfig.timeOut);
    },

    'Step 4: Verify Pasted Buffer': async(browser) => {
        bufferPage.assert.visible('@bufferNameInput');
        bufferPage.deleteBuffer();
        browser.logout();
    }
};
