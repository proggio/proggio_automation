let bufferStartWidth, bufferEndWidth;
let tilesViewPage, proggioMainPage,
    activityPage;
const globalConfig = require('../../../main/utilities/globals');
const assert = require('assert');
const projectTitle = 'Connectors Project';
let mainActivity = 'map_activity_id_346113';
let bufferActivity = 'map_activity_id_346158';
const xOffsetNum = 50;
module.exports = {
    '@tags': ['projectMapTests', 'bufferTests', 'sanity', 'fullRun'],
    before: function(browser) {
        console.log('Loading element pages...');
        tilesViewPage = browser.page.tilesViewPage();
        activityPage = browser.page.activityPage();
        proggioMainPage = browser.page.proggioMainPage();
        browser.login(globalConfig.account_add_user, globalConfig.generic_password);
    },

    'Step 1: Click On Project': () => {
        proggioMainPage.clickOnElement('@allProjects');
        tilesViewPage.enterToProject(`${projectTitle}`);
    },

    'Step 2: Move Activity': async() => {
        activityPage.zoomIntoProject();
        bufferStartWidth = await activityPage.getActivityWidth(bufferActivity);
        await activityPage.moveActivityByOffset(
            `${mainActivity}`,
            bufferStartWidth - xOffsetNum
        );
    },
    'Step 3: Verify Buffer Consumed ': async() => {
        bufferEndWidth = await activityPage.getActivityWidth(bufferActivity);
        assert.strict.notEqual(bufferStartWidth, bufferEndWidth);
    },

    'Step 4: Return Activity To Original Location': async() => {
        await activityPage.moveActivityByOffset(
            `${mainActivity}`,
            -bufferStartWidth + xOffsetNum
        );
    },

    'Step 5: Return Buffer To Original Width': async() => {
        const originalWidth = bufferStartWidth - bufferEndWidth;
        await activityPage.resizeActivity(
            `${bufferActivity}`,
            `left`,
            -originalWidth
        );
    }
};
