const assert = require('assert');
const globalConfig = require('../../../main/utilities/globals');
let tilesViewPage, proggioMainPage, workStreamPage;
const projectTitle = 'Move Items Project';
const dragWorkStreamName = 'Menu on the left';
let startYAxisLocation, endYAxisLocation;
module.exports = {
    '@disabled': true,
    '@tags': ['projectMapTests', 'workStreamTests', 'sanity', 'fullRun'],
    before: function(browser) {
        console.log('Loading element pages...');
        tilesViewPage = browser.page.tilesViewPage();
        proggioMainPage = browser.page.proggioMainPage();
        workStreamPage = browser.page.workStreamPage();
        browser.login(globalConfig.account_add_user, globalConfig.generic_password);
    },

    'Step 1: Click On Project': () => {
        proggioMainPage.clickOnElement('@allProjects');
        tilesViewPage.enterToProject(`${projectTitle}`);
    },

    'Step 2: Resize WorkStream': async() => {
        startYAxisLocation = await workStreamPage.getWorkStreamYAxisByName(dragWorkStreamName);
        await workStreamPage.resizeWorkStream(`${dragWorkStreamName}`, 50);
        endYAxisLocation = await workStreamPage.getWorkStreamYAxisByName(dragWorkStreamName);
    },

    'Step 3: Verify WorkStream Resized': () => {
        assert.strict.notEqual(startYAxisLocation, endYAxisLocation);
    },

    'Step 4: Return WorkStream To Original Location': async() => {
        await workStreamPage.resizeWorkStream(`${dragWorkStreamName}`, -3);
        endYAxisLocation = await workStreamPage.getWorkStreamYAxisByName(dragWorkStreamName);
        assert.strict.equal(startYAxisLocation, endYAxisLocation);
    }
};
