const assert = require('assert');
const globalConfig = require('../../../main/utilities/globals');
let tilesViewPage, proggioMainPage, workStreamPage;
const projectTitle = 'Move Items Project';
const dragWorkStreamName = 'Menu on the left';
const dropWorkStreamName = 'E.g. Purchasing Dep.';
let startYAxisLocation, endYAxisLocation;
module.exports = {
    '@tags': ['projectMapTests', 'workStreamTests', 'sanity', 'fullRun'],
    before: function(browser) {
        console.log('Loading element pages...');
        tilesViewPage = browser.page.tilesViewPage();
        proggioMainPage = browser.page.proggioMainPage();
        workStreamPage = browser.page.workStreamPage();
        browser.login(globalConfig.account_add_user, globalConfig.generic_password);
    },

    'Step 1: Click On Project': () => {
        proggioMainPage.clickOnElement('@allProjects');
        tilesViewPage.enterToProject(`${projectTitle}`);
    },

    'Step 2: Move WorkStream': async() => {
        startYAxisLocation = await workStreamPage.getWorkStreamYAxisByName(dragWorkStreamName);
        await workStreamPage.moveWorkStreamByName(
            `${dragWorkStreamName}`,
            `${dropWorkStreamName}`
        );
        endYAxisLocation = await workStreamPage.getWorkStreamYAxisByName(dragWorkStreamName);
        assert.strict.notEqual(startYAxisLocation, endYAxisLocation);
    },

    'Step 3: Return WorkStream To Original Location': async() => {
        await workStreamPage.moveWorkStreamByName(
            `${dragWorkStreamName}`,
            `${dropWorkStreamName}`
        );
        endYAxisLocation = await workStreamPage.getWorkStreamYAxisByName(dragWorkStreamName);
        assert.strict.equal(startYAxisLocation, endYAxisLocation);
    }
};
