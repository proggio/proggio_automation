let tilesViewPage, proggioMainPage, workStreamPage;
const globalConfig = require('../../../main/utilities/globals');
const projectTitle = 'Copy Paste Project';
const workStream4 = 'Workstream 4';
module.exports = {
    '@tags': ['projectMapTests', 'workStreamTests', 'sanity', 'fullRun'],
    before: function(browser) {
        console.log('Loading element pages...');
        tilesViewPage = browser.page.tilesViewPage();
        proggioMainPage = browser.page.proggioMainPage();
        workStreamPage = browser.page.workStreamPage();
        browser.login(globalConfig.account_add_user, globalConfig.generic_password);
        // browser.waitForElementVisible('a[class = "cc-unoo"]').execute(function() {
        //     document.querySelector('.cc-unoo').remove();
        // });
    },

    'Step 1: Click On Project': () => {
        proggioMainPage.clickOnElement('@allProjects');
        tilesViewPage.enterToProject(`${projectTitle}`);
    },

    'Step 2: Copy WorkStream': () => {
        workStreamPage.openWorkStreamSettings(workStreamPage,
            `${workStream4}`,
            `@copy`
        );
    },

    'Step 3: Paste WorkStream': () => {
        workStreamPage.openWorkStreamSettings(workStreamPage,
            `${workStream4}`,
            `@paste`
        );
        proggioMainPage.expect.element('@spinner').to.not.be.present.before(globalConfig.timeOut);
        workStreamPage.clickOnElement('@copyWorkStreamButton');
    },

    'Step 4: Verify Pasted WorkStream': () => {
        workStreamPage.waitForElementVisible('@workStreamList');
        workStreamPage.expect.elements('@workStreamList').count.to.equal(5);
    },

    'Step 5: Delete Pasted WorkStream': () => {
        workStreamPage.deleteWorkStream(`${workStream4}`);
    },

    'Step 6: Duplicate WorkStream': () => {
        workStreamPage.openWorkStreamSettings(workStreamPage,
            `${workStream4}`,
            `@duplicate`
        );
        proggioMainPage.expect.element('@spinner').to.not.be.present.before(globalConfig.timeOut);
        workStreamPage.clickOnElement('@duplicateWorkStreamButton');
    },

    'Step 7: Verify Pasted WorkStream': () => {
        workStreamPage.waitForElementVisible('@workStreamList');
        workStreamPage.expect.elements('@workStreamList').count.to.equal(5);
    },

    'Step 8: Delete Pasted WorkStream': () => {
        workStreamPage.deleteWorkStream(`${workStream4}`);
    }
};
