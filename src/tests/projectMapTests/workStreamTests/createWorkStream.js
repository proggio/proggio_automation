let tilesViewPage, workStreamPage, proggioMainPage;
let activityName = 'Test Activity';
const globalConfig = require('../../../main/utilities/globals');
module.exports = {
    '@tags': ['projectMapTests', 'workStreamTests', 'sanity', 'fullRun'],
    before: function(browser) {
        console.log('Loading element pages...');
        proggioMainPage = browser.page.proggioMainPage();

        tilesViewPage = browser.page.tilesViewPage();
        workStreamPage = browser.page.workStreamPage();
        browser.login(globalConfig.automation_testing_account, globalConfig.generic_password);
    },

    'Step 1: Delete All Projects': async() => {
        await tilesViewPage.deleteAllProjects();
    },

    'Step 1.1: Create New Project': async() => {
        let projectName = globalConfig.automation_testing_account.split('@', 1);
        await tilesViewPage.createNewProject(`${projectName}`, true);
    },

    'Step 2: Click add New WorkStream': () => {
        workStreamPage.clickOnElement('@addNewWorkStreamButton');
        workStreamPage.expect.element('@defaultWorkStreamCard').to.be.visible.before(globalConfig.timeOut);
    },

    'Step 3: Rename The New WorkStream': (browser) => {
        workStreamPage.getLocationInView('css selector', '@addNewWorkStreamBottom');
        workStreamPage.setValueInElement('@lastWorkStreamCardText', activityName);
        browser.keys([browser.Keys.ENTER]);
    },

    'Step 4: Verify Name Changed': () => {
        workStreamPage.clickOnElement('@lastWorkStreamCard');
        workStreamPage.assert.attributeEquals('@lastWorkStreamCardText', 'value', activityName);
    },

    'Step 5: Open Menu and Delete WorkStream': () => {
        workStreamPage.openWorkStreamSettings(workStreamPage, activityName, '@delete');
        workStreamPage.clickOnElement('@confirmDeleteButton');
        workStreamPage.expect.element('@confirmDeleteButton').to.not.be.present.before(globalConfig.timeOut);
    },

    'Step 6: Verify WorkStream Deleted': () => {
        let activityCard = `//div[contains(text(), "${activityName}")]`;
        proggioMainPage.expect.element('@spinner').to.not.be.present.before(globalConfig.timeOut);
        workStreamPage.expect.element(activityCard).to.not.be.present.before(globalConfig.timeOut);
    }
};
