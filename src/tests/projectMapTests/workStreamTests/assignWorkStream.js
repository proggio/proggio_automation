let tilesViewPage, proggioMainPage, workStreamPage;
const globalConfig = require('../../../main/utilities/globals');
const projectTitle = 'Assignation Project';
const workStreamName = 'Workstream 3';
const addUserAccountName = 'Add_user';
const automationTestingAccountName = 'Automation Testing';

module.exports = {
    '@tags': ['projectMapTests', 'workStreamTests', 'sanity', 'acceptance'],
    before: function(browser) {
        console.log('Loading element pages...');
        tilesViewPage = browser.page.tilesViewPage();
        proggioMainPage = browser.page.proggioMainPage();
        workStreamPage = browser.page.workStreamPage();
        browser.login(globalConfig.account_add_user, globalConfig.generic_password);
    },

    'Step 1: Click On Project': () => {
        proggioMainPage.clickOnElement('@allProjects');
        tilesViewPage.enterToProject(`${projectTitle}`);
    },

    'Step 2: Open WorkStream Settings': () => {
        workStreamPage.openWorkStreamSettings(workStreamPage, workStreamName, '@settings');
    },

    'Step 3: Reassign WorkStream To Other User': () => {
        workStreamPage.assignWorkStream(automationTestingAccountName);
    },

    'Step 4: Verify WorkStream New Assignee': () => {
        workStreamPage.expect.element(workStreamPage.elements.assigneeContainerText).text.to.contain(automationTestingAccountName);
    },

    'Step 5: Reassign WorkStream To Original User': () => {
        workStreamPage.assignWorkStream(addUserAccountName);
        workStreamPage.expect.element(workStreamPage.elements.assigneeContainerText).text.to.contain(addUserAccountName);
        workStreamPage.closeWorkStreamSettings();
    }
};
