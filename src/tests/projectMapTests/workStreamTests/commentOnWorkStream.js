let tilesViewPage, proggioMainPage, chatModalPage, workStreamPage;
const globalConfig = require('../../../main/utilities/globals');
const projectTitle = 'Assignation Project';
const workStream1 = 'Workstream 1';
module.exports = {
    '@tags': ['projectMapTests', 'workStreamTests', 'fullRun', 'commentOnWorkStream'],
    before: function(browser) {
        console.log('Loading element pages...');
        tilesViewPage = browser.page.tilesViewPage();
        proggioMainPage = browser.page.proggioMainPage();
        chatModalPage = browser.page.chatModalPage();
        workStreamPage = browser.page.workStreamPage();
        browser.login(globalConfig.account_add_user, globalConfig.generic_password);
        browser.waitForElementVisible('a[class = "cc-unoo"]').execute(function() {
            document.querySelector('.cc-unoo').remove();
        });
    },

    'Step 1: Click On Project': () => {
        proggioMainPage.clickOnElement('@allProjects');
        tilesViewPage.enterToProject(`${projectTitle}`);
    },

    'Step 2: Open WorkStream Chat': async() => {
        await workStreamPage.openWorkStreamSettings(workStreamPage,
            `${workStream1}`,
            `@collaboration`
        );
    },

    'Step 3: Send Message': () => {
        chatModalPage.sendMessage('Hello! Sending a message to a workStream');
    },

    'Step 4: Verify Message Sent': async() => {
        const lastMessage = await chatModalPage.getLastMessageContent();
        chatModalPage.expect.element('@lastMessage').text.to.contain(lastMessage.value);
    },

    'Step 5: Delete Message': () => {
        chatModalPage.deleteMessage('workStream');
        chatModalPage.expect.element('@lastMessage').to.not.be.present.before(globalConfig.timeOut);
        chatModalPage.closeChatBox();
    }
};
