let tilesViewPage, proggioMainPage, splitPage,
    activityPage,
    workStreamPage, projectBacklogPage;
const globalConfig = require('../../../main/utilities/globals');
const projectName = globalConfig.automation_testing_account.split('@', 1);
const testName = 'Create Unscheduled Task Test';
module.exports = {
    '@tags': ['projectMapTests', 'taskTests', 'sanity', 'fullRun'],
    before: function(browser) {
        console.log('Loading element pages...');
        tilesViewPage = browser.page.tilesViewPage();
        activityPage = browser.page.activityPage();
        proggioMainPage = browser.page.proggioMainPage();
        splitPage = browser.page.splitPage();
        projectBacklogPage = browser.page.projectBacklogPage();
        workStreamPage = browser.page.workStreamPage();
        browser.login(globalConfig.automation_testing_account, globalConfig.generic_password);
    },

    'Step 1: Delete All Projects': async() => {
        await tilesViewPage.deleteAllProjects();
    },

    'Step 1.1: Create New Project': async() => {
        await tilesViewPage.createNewProject(`${projectName}`, true);
    },

    'Step 2: Create Activity In Project': async() => {
        await activityPage.createActivity();
        await proggioMainPage.expect.element('@activitySpinner').to.not.be.present.before(globalConfig.timeOut);
        await activityPage.clickOnElement('@activityCardText');
        await splitPage.expect.element('@addTaskButton').to.be.visible.before(globalConfig.timeOut);
    },

    'Step 3: Open Activity Split': async(browser) => {
        const splitGripLines = splitPage.elements.splitGripLines;
        const workStreamCard = workStreamPage.elements.firstWorkStreamCard;
        await browser.dragAndDrop('css selector', splitGripLines, workStreamCard);
    },

    "Step 4: Click 'Project Backlog' Tab": () => {
        splitPage.clickOnElement('@projectBacklogTab');
    },

    "Step 5: Click 'Add Task To This Project": () => {
        projectBacklogPage.clickOnElement('@addTaskButton');
    },

    'Step 6: Name Your Task': (browser) => {
        projectBacklogPage.clearValue('css selector', '@newTaskText');
        projectBacklogPage.setValueInElement('@newTaskText', testName);
        browser.keys([browser.Keys.ENTER]);
    },

    'Step 7: Verify New Task': (browser) => {
        projectBacklogPage.expect.element('@lastTaskText').text.to.contain(testName);
        browser.keys([browser.Keys.ESCAPE, browser.Keys.ESCAPE]);
    },

    'Step 8: Delete New Task': async() => {
        await splitPage.deleteTaskByName(testName);
        projectBacklogPage.expect.element('@lastTaskCell').to.not.be.present.before(globalConfig.timeOut);
    }
};
