let tilesViewPage, proggioMainPage,
    activityPage, chatModalPage, activityModalPage;
const globalConfig = require('../../../main/utilities/globals');
const projectTitle = 'Assignation Project';
const activity1 = 'map_activity_id_346314';
const taskName = 'Task1';
module.exports = {
    // TODO - Step 6 - Element of chat box
    '@tags': ['projectMapTests', 'taskTests', 'fullRun', 'commentOnTask'],
    before: function(browser) {
        console.log('Loading element pages...');
        tilesViewPage = browser.page.tilesViewPage();
        activityPage = browser.page.activityPage();
        proggioMainPage = browser.page.proggioMainPage();
        chatModalPage = browser.page.chatModalPage();
        activityModalPage = browser.page.activityModalPage();
        browser.login(globalConfig.account_add_user, globalConfig.generic_password);
        // browser.waitForElementVisible('a[class = "cc-unoo"]').execute(function() {
        //     document.querySelector('.cc-unoo').remove();
        // });
    },

    'Step 1: Click On Project': () => {
        proggioMainPage.clickOnElement('@allProjects');
        tilesViewPage.enterToProject(`${projectTitle}`);
    },

    'Step 2: Open Activity Settings': async() => {
        await activityPage.openActivitySettingsById(activityPage,
            `${activity1}`,
            `@details`
        );
    },

    'Step 3: Select Task': () => {
        activityModalPage.selectTaskOrActivityInModal(`${taskName}`);
    },

    'Step 4: Navigate To "Comments" Tab': () => {
        activityModalPage.activityModalChooseTab(activityModalPage, '@commentsTab');
    },

    'Step 5: Send Message': () => {
        chatModalPage.sendMessage('Hello! Sending a message to a task');
    },

    'Step 6: Verify Message Sent': async() => {
        const lastMessage = await chatModalPage.getLastMessageContent();
        chatModalPage.expect.element('@lastMessage').text.to.contain(lastMessage.value);
    },

    'Step 7: Delete Message': () => {
        chatModalPage.deleteMessage('task');
        chatModalPage.expect.element('@lastMessage').to.not.be.present.before(globalConfig.timeOut);
        activityModalPage.closeActivitySettings();
    }
};
