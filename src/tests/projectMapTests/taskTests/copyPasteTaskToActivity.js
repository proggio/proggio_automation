let tilesViewPage, proggioMainPage,
    activityPage, activityModalPage;
const globalConfig = require('../../../main/utilities/globals');
const projectTitle = 'Copy Paste Project';
const activity1 = 'map_activity_id_346890';
const activity2 = 'map_activity_id_346891';
const task1 = 'Task1';
module.exports = {
    '@tags': ['projectMapTests', 'taskTests', 'sanity', 'fullRun', 'copyPasteTaskToActivity'],
    before: function(browser) {
        console.log('Loading element pages...');
        tilesViewPage = browser.page.tilesViewPage();
        activityPage = browser.page.activityPage();
        proggioMainPage = browser.page.proggioMainPage();
        activityModalPage = browser.page.activityModalPage();
        browser.login(globalConfig.account_add_user, globalConfig.generic_password);
    },

    'Step 1: Click On Project': () => {
        proggioMainPage.clickOnElement('@allProjects');
        tilesViewPage.enterToProject(`${projectTitle}`);
    },

    'Step 2: Open Activity Settings': async() => {
        await activityPage.openActivitySettingsById(activityPage,
            `${activity1}`,
            `@details`
        );
    },

    'Step 3: Copy Task - Same Activity': async() => {
        await activityModalPage.selectTaskOrActivityInModal(`${task1}`);
        activityModalPage.openTaskOrActivitySettings(activityModalPage,
            `${task1}`,
            '@copy'
        );
    },

    'Step 4: Paste Task - Same Activity': () => {
        activityModalPage.openTaskOrActivitySettings(activityModalPage,
            `${task1}`,
            '@paste'
        );
        proggioMainPage.expect.element('@spinner').to.not.be.present.before(globalConfig.timeOut);
    },

    'Step 5: Verify Pasted Task - Same Activity': async() => {
        activityModalPage.openTaskOrActivitySettings(activityModalPage,
            `${task1}`,
            '@delete'
        );
        activityModalPage.assertActivityOrTask(`${task1}`);
        activityModalPage.openTaskOrActivitySettings(activityModalPage,
            `${task1}`,
            '@copy'
        );
        activityModalPage.closeActivitySettings();
    },

    'Step 6: Open Other Activity Settings': async() => {
        await activityPage.openActivitySettingsById(activityPage,
            `${activity2}`,
            `@details`
        );
    },

    'Step 7: Paste Task - Other Activity': async() => {
        const task2 = 'Task2';
        activityModalPage.selectTaskOrActivityInModal(`${task2}`);
        activityModalPage.openTaskOrActivitySettings(activityModalPage,
            `${task2}`,
            '@paste'
        );
        proggioMainPage.expect.element('@spinner').to.not.be.present.before(globalConfig.timeOut);
    },

    'Step 8: Verify Pasted Task - Other Activity': async() => {
        activityModalPage.openTaskOrActivitySettings(activityModalPage,
            `${task1}`,
            '@delete'
        );
        activityModalPage.assertActivityOrTask(`${task1}`, 'not');
        activityModalPage.closeActivitySettings();
    }
};
