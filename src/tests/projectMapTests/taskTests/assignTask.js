let tilesViewPage, proggioMainPage,
    activityPage, activityModalPage;
const globalConfig = require('../../../main/utilities/globals');
const projectTitle = 'Assignation Project';
const activity1 = 'map_activity_id_346314';
const taskName = 'Task1';
const addUserAccountName = 'Add_user';
const automationTestingAccountName = 'Automation Testing';

module.exports = {
    '@tags': ['projectMapTests', 'taskTests', 'sanity', 'fullRun'],
    before: function(browser) {
        console.log('Loading element pages...');
        tilesViewPage = browser.page.tilesViewPage();
        activityPage = browser.page.activityPage();
        proggioMainPage = browser.page.proggioMainPage();
        activityModalPage = browser.page.activityModalPage();
        browser.login(globalConfig.account_add_user, globalConfig.generic_password);
    },

    'Step 1: Click On Project': () => {
        proggioMainPage.clickOnElement('@allProjects');
        tilesViewPage.enterToProject(`${projectTitle}`);
    },

    'Step 2: Open Activity Settings': async() => {
        await activityPage.openActivitySettingsById(activityPage, activity1, '@details');
    },

    'Step 3: Reassign Task To Other User': () => {
        activityModalPage.assignActivityTask(taskName, automationTestingAccountName);
    },

    'Step 4: Verify Task New Assignee': () => {
        activityModalPage.expect.element(activityModalPage.elements.assigneeContainerText).text.to.contain(automationTestingAccountName);
    },

    'Step 5: Reassign Task To Original User': () => {
        activityModalPage.assignActivityTask(taskName, addUserAccountName);
        activityModalPage.expect.element(activityModalPage.elements.assigneeContainerText).text.to.contain(addUserAccountName);
        activityModalPage.closeActivitySettings();
    }
};
