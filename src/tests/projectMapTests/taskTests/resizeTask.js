let tilesViewPage, proggioMainPage,
    activityPage, splitPage, activityModalPage;
const globalConfig = require('../../../main/utilities/globals');
const assert = require('assert');
const projectTitle = 'Move Items Project';
let taskName = 'Resize Task';
let resizeActivityId = 'map_activity_id_343867';
let taskStartDate, taskEndDate;
module.exports = {
    '@tags': ['projectMapTests', 'taskTests', 'sanity', 'fullRun'],
    before: function(browser) {
        console.log('Loading element pages...');
        tilesViewPage = browser.page.tilesViewPage();
        splitPage = browser.page.splitPage();
        activityPage = browser.page.activityPage();
        activityModalPage = browser.page.activityModalPage();
        proggioMainPage = browser.page.proggioMainPage();
        browser.login(globalConfig.account_add_user, globalConfig.generic_password);
        browser.waitForElementVisible('a[class = "cc-unoo"]').execute(function() {
            document.querySelector('.cc-unoo').remove();
        });
    },

    'Step 1: Click On Project': () => {
        proggioMainPage.clickOnElement('@allProjects');
        tilesViewPage.enterToProject(`${projectTitle}`);
    },

    'Step 2: Go To Activity Settings': async() => {
        await activityPage.openActivitySettingsById(activityPage,
            `${resizeActivityId}`,
            '@details'
        );
    },

    'Step 3: Select Task': async() => {
        activityModalPage.selectTaskOrActivityInModal(`${taskName}`);
    },

    'Step 4: Resize Task - Settings - Dates': async() => {
        taskStartDate = await activityModalPage.getActivityDateInModal();
        activityModalPage.activityModalDatePicker(
            'end date',
            0,
            28
        );
        taskEndDate = await activityModalPage.getActivityDateInModal();
        assert.strict.notEqual(taskStartDate.value, taskEndDate.value);
        // Return Original Date
        activityModalPage.activityModalDatePicker(
            'end date',
            0,
            29
        );
        activityModalPage.clickOnElement('@saveButton');
    },

    'Step 5: Resize Activity - Split - Dates': async() => {
        splitPage.openSplit();
        taskStartDate = await splitPage.getTaskEndDateInSplit(taskName);
        splitPage.activitySplitDatePicker(
            `${taskName}`,
            'end date',
            0,
            28
        );
        taskEndDate = await splitPage.getTaskEndDateInSplit(taskName);
        assert.strict.notEqual(taskStartDate.value, taskEndDate.value);
        // Return Original Date
        splitPage.activitySplitDatePicker(
            `${taskName}`,
            'end date',
            0,
            29
        );
    }
};
