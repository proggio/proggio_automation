let tilesViewPage,
    activityPage, milestonePage;
const globalConfig = require('../../../main/utilities/globals');
let projectName = globalConfig.automation_testing_account.split('@', 1);
const milestoneName = 'Milestone Test';

module.exports = {
    '@tags': ['projectMapTests', 'milestoneTests', 'sanity', 'fullRun', 'createMilestone'],
    before: (browser) => {
        console.log('Loading element pages...');
        tilesViewPage = browser.page.tilesViewPage();
        activityPage = browser.page.activityPage();
        milestonePage = browser.page.milestonePage();
        browser.login(globalConfig.automation_testing_account, globalConfig.generic_password);
    },

    'Step 1: Delete All Projects': async() => {
        await tilesViewPage.deleteAllProjects();
    },

    'Step 1.1: Create New Project': async() => {
        await tilesViewPage.createNewProject(`${projectName}`, true);
    },

    'Step 2: Create Milestone': (browser) => {
        browser.moveToElement('xpath', activityPage.elements.projectGridToday, 96, 0, function() {
            browser.doubleClick();
        });
    },

    'Step 3: Rename Milestone': () => {
        milestonePage.waitForElementVisible('@milestoneInput');
        milestonePage.clearValue('css selector', '@milestoneInput');
        milestonePage.setValueInElement('@milestoneInput', milestoneName);
        milestonePage.clickOnElement('@milestoneSaveButton');
        milestonePage.expect.element('@milestoneSettingsPopup').to.not.be.present.before(globalConfig.timeOut);
    },

    'Step 4: Verify Milestone ': () => {
        milestonePage.verifyMilestoneText(milestoneName);
    },

    'Step 5: Delete Milestone ': () => {
        milestonePage.deleteMilestone(milestoneName);
    }
};
