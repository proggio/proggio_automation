let tilesViewPage, proggioMainPage, milestonePage;
const globalConfig = require('../../../main/utilities/globals');
const projectTitle = 'Move Items Project';
module.exports = {
    '@tags': ['projectMapTests', 'milestoneTests', 'sanity', 'fullRun', 'moveMilestone'],
    before: function(browser) {
        console.log('Loading element pages...');
        tilesViewPage = browser.page.tilesViewPage();
        proggioMainPage = browser.page.proggioMainPage();
        milestonePage = browser.page.milestonePage();
        browser.login(globalConfig.account_add_user, globalConfig.generic_password);
    },

    'Step 1: Click On Project': () => {
        proggioMainPage.clickOnElement('@allProjects');
        tilesViewPage.enterToProject(`${projectTitle}`);
    },

    'Step 2: Go To Milestone Settings': async() => {
        await milestonePage.openMilestoneSettings('Milestone Test');
    },

    'Step 3: Change Milestone Date': () => {
        milestonePage.milestoneDatePicker(6);
    },

    'Step 4: Verify Milestone Date Change': () => {
        milestonePage.expect.element('@dateContainerText').text.to.contain('May 06');
    },

    'Step 5: Return Milestone Original Date': async() => {
        milestonePage.milestoneDatePicker(7);
        milestonePage.expect.element('@dateContainerText').text.to.contain('May 07');
        milestonePage.clickOnElement('@milestoneSaveButton');
    }
};
