let tilesViewPage, proggioMainPage, splitPage,
    activityPage;
const globalConfig = require('../../../main/utilities/globals');
let projectName = globalConfig.automation_testing_account.split('@', 1);
let taskName = 'Add task to activity test';
module.exports = {
    '@tags': ['projectMapTests', 'splitTests', 'sanity', 'fullRun'],
    before(browser) {
        console.log('Loading element pages...');
        tilesViewPage = browser.page.tilesViewPage();
        activityPage = browser.page.activityPage();
        proggioMainPage = browser.page.proggioMainPage();
        splitPage = browser.page.splitPage();
        browser.login(globalConfig.automation_testing_account, globalConfig.generic_password);
    },

    'Step 1: Delete All Projects': async() => {
        await tilesViewPage.deleteAllProjects();
    },

    'Step 1.1: Create New Project': async() => {
        await tilesViewPage.createNewProject(`${projectName}`, true);
    },

    'Step 2: Create Activity': async() => {
        await activityPage.createActivity();
        await proggioMainPage.expect.element('@activitySpinner').to.not.be.present.before(globalConfig.timeOut);
        await activityPage.clickOnElement('@activityCardText');
        await splitPage.expect.element('@addTaskButton').to.be.visible.before(globalConfig.timeOut);
    },

    'Step 3: Open Activity Split': async(browser) => {
        let splitGripLines = splitPage.elements.splitGripLines;
        let todayFlag = activityPage.elements.projectGridToday;
        await browser.dragAndDrop('css selector', splitGripLines, todayFlag);
    },

    'Step 4: Add New Task To Activity': () => {
        splitPage.addTask(taskName);
    },

    'Step 5: Verify New Task': (browser) => {
        splitPage.expect.element('@lastTaskText').text.to.contain(taskName);
        browser.keys([browser.Keys.ESCAPE]);
    },

    'Step 6: Delete New Task': async() => {
        let taskName = 'Edit Me!';
        await splitPage.deleteTaskByName(taskName);
        splitPage.expect.element('@lastTaskCell').to.not.be.present.before(globalConfig.timeOut);
    }
};
