let tilesViewPage, proggioMainPage,
    activityPage;
const globalConfig = require('../../../main/utilities/globals');
const projectTitle = 'Assignation Project';
const activity2 = 'map_activity_id_346315';
module.exports = {
    // TODO - Activity Progress
    '@tags': ['projectMapTests', 'activityTests', 'fullRun'],
    before: function(browser) {
        console.log('Loading element pages...');
        tilesViewPage = browser.page.tilesViewPage();
        activityPage = browser.page.activityPage();
        proggioMainPage = browser.page.proggioMainPage();
        browser.login(globalConfig.account_add_user, globalConfig.generic_password);
    },

    'Step 1: Click On Project': () => {
        proggioMainPage.clickOnElement('@allProjects');
        tilesViewPage.enterToProject(`${projectTitle}`);
    },

    'Step 2: Open Activity Progress Bar': async() => {
        activityPage.openActivityProgressBar(`${activity2}`);
    },

    'Step 3: Drag Activity Progress Bar': async() => {
        activityPage.moveActivityProgressBar(50);
    },

    'Step 4: Verify Activity Progress Bar Changed': async() => {
        activityPage.expect.element('@progressBarNumText').text.to.not.equal(0);
    },

    'Step 5: Return Progress Bar': async() => {
        activityPage.openActivityProgressBar(`${activity2}`);
        activityPage.moveActivityProgressBar(-50);
        activityPage.openActivityProgressBar(`${activity2}`);
        activityPage.expect.element('@progressBarNumText').text.to.contain(0);
    }

};
