let tilesViewPage, proggioMainPage,
    activityPage, checkListModalPage,
    activityModalPage, filesModalPage;
const globalConfig = require('../../../main/utilities/globals');
const projectTitle = 'Copy Paste Project';
const activity1 = 'map_activity_id_346890';
const activity2 = 'map_activity_id_346891';
const checkListItem = 'Activity1 Tests';
const activity3Name = 'Activity3';
let activity3;
const checkListItem3 = 'Activity3 Tests';
module.exports = {
    '@tags': ['projectMapTests', 'activityTests', 'sanity', 'fullRun'],
    before: function(browser) {
        console.log('Loading element pages...');
        tilesViewPage = browser.page.tilesViewPage();
        activityPage = browser.page.activityPage();
        proggioMainPage = browser.page.proggioMainPage();
        filesModalPage = browser.page.filesModalPage();
        activityModalPage = browser.page.activityModalPage();
        checkListModalPage = browser.page.checkListModalPage();
        browser.login(globalConfig.account_add_user, globalConfig.generic_password);
    },

    'Step 1: [Test - Copy/Paste Activity] - Click On Project': () => {
        proggioMainPage.clickOnElement('@allProjects');
        tilesViewPage.enterToProject(`${projectTitle}`);
    },

    'Step 2: Copy Activity': async() => {
        await activityPage.openActivitySettingsById(activityPage,
            `${activity1}`,
            `@copy`
        );
    },

    'Step 3: Paste Activity - Same WorkStream': async() => {
        await activityPage.pasteActivityAndOpen(activity1, 50);
    },

    'Step 4: [Verify - Copy/Paste Activity] - Same WorkStream': async() => {
        activityModalPage.activityModalChooseTab(activityModalPage, '@fileTab');
        filesModalPage.expect.element('@emptyAttachments').to.not.be.present;
        activityModalPage.activityModalChooseTab(activityModalPage, '@checkListTab');
        const checkList = await checkListModalPage.getCheckListText(`${checkListItem}`);
        const checkListElement = `div[title = "${checkList.value}"]`;
        checkListModalPage.expect.element(checkListElement).to.be.present;
    },

    'Step 5: Delete Pasted Activity - Same WorkStream': () => {
        activityModalPage.activityModalChooseTab(activityModalPage, '@infoTab');
        activityModalPage.clickOnElement('@deleteButton');
        activityModalPage.clickOnElement('@confirmDelete');
    },

    'Step 6: [Test - Copy/Paste Activity] - Different WorkStream': async() => {
        await activityPage.pasteActivityAndOpen(activity2, 50);
    },

    'Step 7: [Verify - Copy/Paste Activity] - Different WorkStream': async() => {
        activityModalPage.activityModalChooseTab(activityModalPage, '@fileTab');
        filesModalPage.expect.element('@emptyAttachments').to.not.be.present;
        activityModalPage.activityModalChooseTab(activityModalPage, '@checkListTab');
        const checkList = await checkListModalPage.getCheckListText(`${checkListItem}`);
        const checkListElement = `div[title = "${checkList.value}"]`;
        checkListModalPage.expect.element(checkListElement).to.be.present;
    },

    'Step 8: Delete Pasted Activity - Different WorkStream': () => {
        activityModalPage.activityModalChooseTab(activityModalPage, '@infoTab');
        activityModalPage.clickOnElement('@deleteButton');
        activityModalPage.clickOnElement('@confirmDelete');
    },

    'Step 9: [Test - Cut/Paste Activity] - Cut Activity': async() => {
        activity3 = await activityPage.getActivityId(activity3Name);
        await activityPage.openActivitySettingsById(activityPage,
            `${activity3}`,
            `@cut`
        );
    },

    'Step 10: Paste Activity': async(browser) => {
        await activityPage.pasteActivity(activity3,
            'left',
            -50
        );
        proggioMainPage.expect.element('@spinner').to.not.be.present.before(globalConfig.timeOut);
        browser.pause(2000);
    },

    'Step 11: [Verify - Cut/Paste Activity]': async() => {
        activity3 = await activityPage.getActivityId(activity3Name);
        await activityPage.openActivitySettingsById(activityPage,
            `${activity3}`,
            '@details'
        );
        activityModalPage.activityModalChooseTab(activityModalPage, '@fileTab');
        filesModalPage.expect.element('@emptyAttachments').to.not.be.present;
        activityModalPage.activityModalChooseTab(activityModalPage, '@checkListTab');
        const checkList = await checkListModalPage.getCheckListText(`${checkListItem3}`);
        const checkListElement = `div[title = "${checkList.value}"]`;
        checkListModalPage.expect.element(checkListElement).to.be.present;
        activityModalPage.closeActivitySettings();
    },

    'Step 12: Return Activity To Original Place': async() => {
        await activityPage.openActivitySettingsById(activityPage,
            `${activity3}`,
            `@cut`
        );
        await activityPage.pasteActivity(activity3,
            'right',
            50
        );
        proggioMainPage.expect.element('@spinner').to.not.be.present.before(globalConfig.timeOut);
    }
};
