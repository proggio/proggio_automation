let tilesViewPage, proggioMainPage,
    activityPage, activityModalPage;
const globalConfig = require('../../../../main/utilities/globals');
const projectTitle = 'Assignation Project';
const activity1 = 'map_activity_id_346314';
const activityName = 'Activity 1';
const addUserAccountName = 'Add_user';
const automationTestingAccountName = 'Automation Testing';

module.exports = {
    '@tags': ['projectMapTests', 'activityTests', 'activitySettings', 'fullRun'],
    before: function(browser) {
        console.log('Loading element pages...');
        tilesViewPage = browser.page.tilesViewPage();
        activityPage = browser.page.activityPage();
        proggioMainPage = browser.page.proggioMainPage();
        activityModalPage = browser.page.activityModalPage();
        browser.login(globalConfig.account_add_user, globalConfig.generic_password);
    },

    'Step 1: [Test - Assign Activity] - Click On Project': () => {
        proggioMainPage.clickOnElement('@allProjects');
        tilesViewPage.enterToProject(`${projectTitle}`);
    },

    'Step 2: Open Activity Settings': async() => {
        await activityPage.openActivitySettingsById(activityPage, activity1, '@details');
    },

    'Step 3: Reassign Activity To Other User': () => {
        activityModalPage.assignActivityTask(activityName, automationTestingAccountName);
    },

    'Step 4: [Verify - Assign Activity]': () => {
        activityModalPage.expect.element(activityModalPage.elements.assigneeContainerText).text.to.contain(automationTestingAccountName);
    },

    'Step 5: Reassign Activity To Original User': () => {
        activityModalPage.assignActivityTask(activityName, addUserAccountName);
        activityModalPage.expect.element(activityModalPage.elements.assigneeContainerText).text.to.contain(addUserAccountName);
        activityModalPage.closeActivitySettings();
    }
};
