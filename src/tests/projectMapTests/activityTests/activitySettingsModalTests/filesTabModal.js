let tilesViewPage, proggioMainPage,
    activityPage, projectModalPage, filesModalPage;
const globalConfig = require('../../../../main/utilities/globals');
const projectTitle = 'Copy Paste Project';
module.exports = {
    '@tags': ['projectMapTests', 'activityTests', 'activitySettings', 'fullRun', 'filesTabModal'],
    before: function(browser) {
        console.log('Loading element pages...');
        tilesViewPage = browser.page.tilesViewPage();
        activityPage = browser.page.activityPage();
        proggioMainPage = browser.page.proggioMainPage();
        projectModalPage = browser.page.projectModalPage();
        filesModalPage = browser.page.filesModalPage();
        browser.login(globalConfig.account_add_user, globalConfig.generic_password);
    },

    'Step 1: [Test - Add File To Project] - Click On Project': () => {
        proggioMainPage.clickOnElement('@allProjects');
        tilesViewPage.enterToProject(`${projectTitle}`);
    },

    'Step 2: Add File To Project': async() => {
        activityPage.openProjectSettings(`${projectTitle}`);
        filesModalPage.addExternalLink(`${projectTitle}`);
    },

    'Step 3: [Verify - Add File To Project]': async() => {
        filesModalPage.verifyFileVisible(`${projectTitle}`);
    },

    'Step 4: Delete Added File': async() => {
        filesModalPage.removeExternalLink(`${projectTitle}`);
        projectModalPage.closeProjectSettings();
    }
};
