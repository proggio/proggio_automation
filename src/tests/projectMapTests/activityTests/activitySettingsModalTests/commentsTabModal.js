let tilesViewPage, proggioMainPage,
    activityPage, chatModalPage, activityModalPage;
const globalConfig = require('../../../../main/utilities/globals');
const projectTitle = 'Assignation Project';
const activity1 = 'map_activity_id_346314';
module.exports = {
    '@tags': ['projectMapTests', 'activityTests', 'activitySettings', 'sanity'],
    before: function(browser) {
        console.log('Loading element pages...');
        tilesViewPage = browser.page.tilesViewPage();
        activityPage = browser.page.activityPage();
        proggioMainPage = browser.page.proggioMainPage();
        chatModalPage = browser.page.chatModalPage();
        activityModalPage = browser.page.activityModalPage();
        browser.login(globalConfig.account_add_user, globalConfig.generic_password);
    },

    'Step 1: [Test - Comment On Activity] - Click On Project': () => {
        proggioMainPage.clickOnElement('@allProjects');
        tilesViewPage.enterToProject(`${projectTitle}`);
    },

    'Step 2: Open Activity Settings': async() => {
        await activityPage.openActivitySettingsById(activityPage,
            `${activity1}`,
            `@details`
        );
    },

    'Step 3: Navigate To "Comments" Tab': () => {
        activityModalPage.activityModalChooseTab(activityModalPage, '@commentsTab');
    },

    'Step 4: Send Message': () => {
        chatModalPage.sendMessage('Hello! Sending a message to an activity');
    },

    'Step 5: [Verify - Comment On Activity]': async() => {
        const lastMessage = await chatModalPage.getLastMessageContent();
        chatModalPage.expect.element('@lastMessage').text.to.contain(lastMessage.value);
    },

    'Step 6: Delete Message': () => {
        chatModalPage.deleteMessage('activity');
        chatModalPage.expect.element('@lastMessage').to.not.be.present.before(globalConfig.timeOut);
        activityModalPage.closeActivitySettings();
    }
};
