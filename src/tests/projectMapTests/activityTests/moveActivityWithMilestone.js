let milestoneStartWidth, milestoneEndWidth, activityStartWidth;
let tilesViewPage, proggioMainPage,
    activityPage, milestonePage;
const globalConfig = require('../../../main/utilities/globals');
const assert = require('assert');
const projectTitle = 'Connectors Project';
let mainActivity = 'map_activity_id_346160';
let milestone = 'milestone-346161';
let milestoneName = 'Connected Milestone';
module.exports = {
    '@tags': ['projectMapTests', 'activityTests', 'sanity', 'fullRun'],
    before: function(browser) {
        console.log('Loading element pages...');
        tilesViewPage = browser.page.tilesViewPage();
        activityPage = browser.page.activityPage();
        proggioMainPage = browser.page.proggioMainPage();
        milestonePage = browser.page.milestonePage();
        browser.login(globalConfig.account_add_user, globalConfig.generic_password);
    },

    'Step 1: Click On Project': () => {
        proggioMainPage.clickOnElement('@allProjects');
        tilesViewPage.enterToProject(`${projectTitle}`);
    },

    'Step 2: Move Activity': async() => {
        activityPage.zoomIntoProject();
        activityStartWidth = await activityPage.getActivityWidth(mainActivity);
        milestoneStartWidth = await activityPage.getActivityXAxisLocation(milestone);
        await activityPage.moveActivityByOffset(
            `${mainActivity}`,
            activityStartWidth
        );
    },
    'Step 3: Verify Milestone Moved ': async() => {
        milestoneEndWidth = await activityPage.getActivityWidth(milestone);
        assert.strict.notEqual(milestoneStartWidth, milestoneEndWidth);
    },

    'Step 4: Return Activity To Original Location': async() => {
        await activityPage.moveActivityByOffset(
            `${mainActivity}`,
            -activityStartWidth
        );
    },

    'Step 5: Return Milestone To Original Location': async() => {
        await milestonePage.openMilestoneSettings(milestoneName);
        milestonePage.milestoneDatePicker(12);
        milestonePage.clickOnElement('@milestoneSaveButton');
    }
};
