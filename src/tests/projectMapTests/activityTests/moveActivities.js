let activity1StartLocation, activity1EndLocation, activity2StartLocation, activity2EndLocation;
let tilesViewPage, proggioMainPage,
    activityPage;
const globalConfig = require('../../../main/utilities/globals');
const assert = require('assert');
const projectTitle = 'Move Items Project';
let resizeActivityId = 'map_activity_id_343867';
const xOffsetNum = 50;
module.exports = {
    '@tags': ['projectMapTests', 'activityTests', 'sanity', 'fullRun'],
    before: function(browser) {
        console.log('Loading element pages...');
        tilesViewPage = browser.page.tilesViewPage();
        activityPage = browser.page.activityPage();
        proggioMainPage = browser.page.proggioMainPage();
        browser.login(globalConfig.account_add_user, globalConfig.generic_password);
    },

    'Step 1: Click On Project': () => {
        proggioMainPage.clickOnElement('@allProjects');
        tilesViewPage.enterToProject(`${projectTitle}`);
    },

    'Step 2: Move Activities': async() => {
        activityPage.zoomIntoProject();
        activity1StartLocation = await activityPage.getActivityXAxisLocation(globalConfig.mainActivity);
        activity2StartLocation = await activityPage.getActivityXAxisLocation(resizeActivityId);
        await activityPage.moveActivitiesByOffset(
            `${globalConfig.mainActivity}`,
            `${resizeActivityId}`,
            xOffsetNum
        );
    },
    'Step 3: Verify Activities Moved ': async() => {
        activity1EndLocation = await activityPage.getActivityXAxisLocation(globalConfig.mainActivity);
        activity2EndLocation = await activityPage.getActivityXAxisLocation(resizeActivityId);
        assert.strict.notEqual(activity1StartLocation, activity1EndLocation);
        assert.strict.notEqual(activity2StartLocation, activity2EndLocation);
    },

    'Step 4: Return Activity To Original Location': async() => {
        await activityPage.moveActivitiesByOffset(
            `${globalConfig.mainActivity}`,
            `${resizeActivityId}`,
            -xOffsetNum
        );
    }
};
