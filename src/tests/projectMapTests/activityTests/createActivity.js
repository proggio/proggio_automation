let tilesViewPage, proggioMainPage,
    activityPage;
const globalConfig = require('../../../main/utilities/globals');
let projectName = globalConfig.automation_testing_account.split('@', 1);
let activity = 'Activity Test';
module.exports = {
    '@tags': ['projectMapTests', 'activityTests', 'sanity', 'fullRun', 'createActivity'],
    before: function(browser) {
        console.log('Loading element pages...');

        tilesViewPage = browser.page.tilesViewPage();
        activityPage = browser.page.activityPage();
        proggioMainPage = browser.page.proggioMainPage();
        browser.login(globalConfig.automation_testing_account, globalConfig.generic_password);
    },

    'Step 1: Delete All Projects': async() => {
        await tilesViewPage.deleteAllProjects();
    },

    'Step 1.1: Create New Project': async() => {
        await tilesViewPage.createNewProject(`${projectName}`, true);
    },

    'Step 2: Create Activity': async() => {
        await activityPage.createActivity();
        proggioMainPage.expect.element('@activitySpinner').to.not.be.present.before(globalConfig.timeOut);
    },

    'Step 3: Rename Activity': (browser) => {
        activityPage.clickOnElement('@activityCardText');
        activityPage.setValueInElement('@activityCardInput', activity);
        browser.keys([browser.Keys.ENTER]);
    },

    'Step 4: Verify Activity Name ': () => {
        activityPage.assert.containsTextInElement('@activityCardText', activity);
    },

    'Step 5: Delete Activity': async() => {
        const id = await activityPage.getActivityId(activity);
        await activityPage.openActivitySettingsById(activityPage, `${id}`, '@delete');
        activityPage.expect.element('@activityCard').to.not.be.present.before(globalConfig.timeOut);
    }
};
