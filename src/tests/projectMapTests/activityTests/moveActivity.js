let activityOriginalX, activityMovedX;
let tilesViewPage, proggioMainPage,
    activityPage, splitPage, activityModalPage;
const globalConfig = require('../../../main/utilities/globals');
const assert = require('assert');
const projectTitle = 'Move Items Project';
let ActivityName = 'Resize Activity';
let resizeActivityId = 'map_activity_id_343867';
let activityStartingWidth, activityEndingWidth;
module.exports = {
    '@tags': ['projectMapTests', 'activityTests', 'sanity', 'fullRun'],
    before: function(browser) {
        console.log('Loading element pages...');
        tilesViewPage = browser.page.tilesViewPage();
        splitPage = browser.page.splitPage();
        activityPage = browser.page.activityPage();
        proggioMainPage = browser.page.proggioMainPage();
        activityModalPage = browser.page.activityModalPage();
        browser.login(globalConfig.account_add_user, globalConfig.generic_password);
    },

    'Step 1: Click On Project': () => {
        proggioMainPage.clickOnElement('@allProjects');
        tilesViewPage.enterToProject(`${projectTitle}`);
    },

    'Step 2: Move Activity': async() => {
        activityPage.zoomIntoProject();
        activityOriginalX = await activityPage.getActivityXAxisLocation(globalConfig.mainActivity);
        await activityPage.moveActivityById(
            `${globalConfig.mainActivity}`,
            `${globalConfig.endActivity}`,
            1
        );
    },
    'Step 3: Verify Activity Moved ': async() => {
        activityMovedX = await activityPage.getActivityXAxisLocation(globalConfig.mainActivity);
        assert.strict.notEqual(activityOriginalX, activityMovedX);
    },

    'Step 4: Return Activity To Original Location': async() => {
        await activityPage.moveActivityById(
            `${globalConfig.mainActivity}`,
            `${globalConfig.startActivity}`,
            1
        );
    },

    'Step 5: Resize Activity - Dragging': async() => {
        const xOffsetNum = 20;
        activityStartingWidth = await activityPage.getActivityWidth(resizeActivityId);
        await activityPage.resizeActivity(resizeActivityId, 'right', xOffsetNum);
        activityEndingWidth = await activityPage.getActivityWidth(resizeActivityId);
        assert.strict.notEqual(activityStartingWidth, activityEndingWidth);
        // Resize Activity Back
        await activityPage.resizeActivity(resizeActivityId, 'right', -xOffsetNum);
    },

    'Step 6: Resize Activity - Settings - Dates': async(browser) => {
        await activityPage.openActivitySettingsById(activityPage, resizeActivityId, '@details');
        activityModalPage.activityModalDatePicker(
            'end date',
            0,
            30
        );

        browser.executeAsync(function(done) {
            document.querySelector('#alert').remove();
            setTimeout(function() {
                done(true);
            }, 500);
        });

        activityStartingWidth = await activityPage.getActivityWidth(resizeActivityId);
        activityModalPage.activityModalDatePicker(
            'end date',
            0,
            29
        );
        activityModalPage.clickOnElement('@saveButton');
        activityEndingWidth = await activityPage.getActivityWidth(resizeActivityId);
        assert.strict.notEqual(activityStartingWidth, activityEndingWidth);
        // Return Original Date
        await activityPage.openActivitySettingsById(activityPage, resizeActivityId, '@details');
        activityModalPage.activityModalDatePicker(
            'end date',
            0,
            30
        );
        activityModalPage.clickOnElement('@saveButton');
    },

    'Step 7: Resize Activity - Split - Dates': async() => {
        activityStartingWidth = await activityPage.getActivityWidth(resizeActivityId);
        splitPage.openSplit();
        splitPage.openSplit();
        splitPage.activitySplitDatePicker(
            `${ActivityName}`,
            'end date',
            0,
            29
        );
        splitPage.closeSplit(splitPage);
        activityEndingWidth = await activityPage.getActivityWidth(resizeActivityId);
        assert.strict.notEqual(activityStartingWidth, activityEndingWidth);
        // Return Original Date
        splitPage.openSplit(splitPage);
        splitPage.activitySplitDatePicker(
            `${ActivityName}`,
            'end date',
            0,
            30
        );
    }
};
