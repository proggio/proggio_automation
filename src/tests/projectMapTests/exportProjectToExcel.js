const xlsxFile = require('read-excel-file/node');
const expect = require('chai').expect;
const fs = require('fs');
const { excelDateFormat } = require('../../main/utilities/customDate');
let proggioMainPage, toolsPage, tilesViewPage;
const electronicDevice = 'Electronic Device';
const globalConfig = require('../../main/utilities/globals');
module.exports = {
    '@disabled': true,
    '@tags': ['projectMapTests', 'fullRun'],
    before(browser) {
        console.log('Loading element pages...');
        proggioMainPage = browser.page.proggioMainPage();
        toolsPage = browser.page.toolsPage();
        tilesViewPage = browser.page.tilesViewPage();
        browser.login(globalConfig.list_view_account, globalConfig.generic_password);
    },

    'Step 1: [Test - Export Project To Excel From Project Map] - Go To All Projects Tiles View': () => {
        proggioMainPage.allProjectsViewPicker(globalConfig.allProjects.views.tilesView);
    },

    'Step 2: Open "Electronic Device" Project': () => {
        tilesViewPage.openProject(`${electronicDevice}`);
    },

    'Step 3: Export "Electronic Device" Project To Excel': (browser) => {
        toolsPage.exportProjectToExcel('project map');
        browser.pause(3000);
    },

    'Step 4: [Verify - Export Project To Excel From Project Map]': () => {
        xlsxFile(`C:/Users/vladi/Downloads/Project ${electronicDevice} ${excelDateFormat}.xlsx`).then((rows) => {
            for (let i = 0; i < rows[0].length; i++) {
                expect(rows[1][0]).to.not.be.equal(null);
            }
        });
    },

    'Step 5: Delete Excel File From OS': () => {
        fs.unlinkSync(`C:/Users/vladi/Downloads/Project ${electronicDevice} ${excelDateFormat}.xlsx`);
    }
};
