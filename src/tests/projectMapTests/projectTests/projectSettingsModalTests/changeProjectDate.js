let tilesViewPage, proggioMainPage,
    activityPage, projectModalPage;
const globalConfig = require('../../../../main/utilities/globals');
const projectTitle = 'Copy Paste Project';
module.exports = {
    '@tags': ['projectMapTests', 'projectTests', 'projectSettingsModalTests', 'sanity', 'fullRun'],
    before: function(browser) {
        console.log('Loading element pages...');
        tilesViewPage = browser.page.tilesViewPage();
        activityPage = browser.page.activityPage();
        proggioMainPage = browser.page.proggioMainPage();
        projectModalPage = browser.page.projectModalPage();
        browser.login(globalConfig.account_add_user, globalConfig.generic_password);
    },

    'Step 1: Click On Project': () => {
        proggioMainPage.clickOnElement('@allProjects');
        tilesViewPage.enterToProject(`${projectTitle}`);
    },

    'Step 2: Open Project Settings': async() => {
        activityPage.openProjectSettings(projectTitle);
    },

    'Step 3: Change Project Date': async() => {
        projectModalPage.projectDatePicker(24);
    },

    'Step 4: Verify Changed Date': async() => {
        projectModalPage.expect.element('@datePickerText').text.to.contain(24);
    },

    'Step 5: Return Original Date ': async() => {
        projectModalPage.projectDatePicker(25);
        projectModalPage.closeProjectSettings();
    }
};
