const assert = require('assert');
const globalConfig = require('../../../main/utilities/globals');
let tilesViewPage, proggioMainPage,
    activityPage, workStreamPage;
const projectTitle = 'Zoom Project';
const activity1Id = 'map_activity_id_348583';
const workStreamName = 'My WorkStream';
let workStream;
let xStartLocation, xEndLocation;
module.exports = {
    '@tags': ['projectMapTests', 'milestoneTests', 'sanity', 'fullRun', 'projectTopControls'],
    before: function(browser) {
        console.log('Loading element pages...');
        tilesViewPage = browser.page.tilesViewPage();
        activityPage = browser.page.activityPage();
        proggioMainPage = browser.page.proggioMainPage();
        workStreamPage = browser.page.workStreamPage();
        browser.login(globalConfig.account_add_user, globalConfig.generic_password);
    },

    'Step 1: [Test - Zoom To Project] - Click On "Zoom Project"': () => {
        proggioMainPage.clickOnElement('@allProjects');
        tilesViewPage.enterToProject(`${projectTitle}`);
    },

    'Step 2: Zoom Out Project': async() => {
        activityPage.zoomIntoProject();
        activityPage.zoomOutMyProject(3);
        xStartLocation = await activityPage.getActivityXAxisLocation(activity1Id);
    },

    'Step 3: Zoom To Project': () => {
        activityPage.zoomIntoProject();
    },

    'Step 4: [Verify - Zoom To Project]': async() => {
        xEndLocation = await activityPage.getActivityXAxisLocation(activity1Id);
        assert.strict.notEqual(xStartLocation, xEndLocation);
    },

    'Step 5: [Test - Zoom To Today] - Zoom In Project': async() => {
        activityPage.zoomInMyProject(10);
        activityPage.zoomIntoProject();
        xStartLocation = await activityPage.getActivityXAxisLocation(activity1Id);
    },

    'Step 6: Zoom To "Today"': () => {
        activityPage.zoomToTodayProject();
    },

    'Step 7: [Verify - Zoom To Today]': async() => {
        activityPage.expect.element('@todayFlag').to.be.present.before(globalConfig.timeOut);
    },

    'Step 8: [Test - Zoom Out Project] - Zoom Out Project': async() => {
        activityPage.zoomIntoProject();
        xStartLocation = await activityPage.getActivityWidth(activity1Id);
        activityPage.zoomOutMyProject(5);
    },

    'Step 9: [Verify - Zoom Out Project]': async() => {
        xEndLocation = await activityPage.getActivityWidth(activity1Id);
        assert.strict.notEqual(xStartLocation, xEndLocation);
    },

    'Step 10: [Test - Zoom In Project] - Zoom In Project': async() => {
        xStartLocation = await activityPage.getActivityWidth(activity1Id);
        activityPage.zoomInMyProject(5);
    },

    'Step 11: [Verify - Zoom In Project]': async() => {
        xEndLocation = await activityPage.getActivityWidth(activity1Id);
        assert.strict.notEqual(xStartLocation, xEndLocation);
    },

    'Step 12: Return Original Zoom': () => {
        activityPage.zoomOutMyProject(1);
        activityPage.zoomIntoProject();
    },

    'Step 13: [Test - Redo/Undo Actions] - Add New WorkStream': () => {
        workStreamPage.addNewWorkStream(`${workStreamName}`);
        workStream = workStreamPage.getWorkStreamTitle(`${workStreamName}`);
    },

    'Step 14: Click Undo': () => {
        activityPage.undoAction();
    },

    'Step 15: [Verify - Undo Button]': (browser) => {
        browser.useXpath();
        browser.expect.element(workStream).to.not.be.present.before(globalConfig.timeOut);
    },

    'Step 16: Click Redo': () => {
        activityPage.redoAction();
    },

    'Step 17: [Verify - Redo Button]': (browser) => {
        browser.useXpath();
        browser.expect.element(workStream).to.be.present.before(globalConfig.timeOut);
    },

    'Step 18: Delete WorkStream': () => {
        workStreamPage.deleteWorkStream(`${workStreamName}`);
    }
};
