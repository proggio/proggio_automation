let listViewPage, proggioMainPage, taskManagementPage;
const globalConfig = require('../../../main/utilities/globals');
module.exports = {
    '@tags': ['taskManagementTests', 'listViewTests', 'fullRun'],
    before(browser) {
        console.log('Loading element pages...');
        proggioMainPage = browser.page.proggioMainPage();
        listViewPage = browser.page.listViewPage();
        taskManagementPage = browser.page.taskManagementPage();
        browser.login(globalConfig.list_view_account, globalConfig.generic_password);
    },

    'Step 1: Click On List View': () => {
        proggioMainPage.clickOnElement('@taskManagement');
        taskManagementPage.taskManagementViewPicker(globalConfig.allProjects.views.listView);
    },

    'Step 2: Add All Columns': async() => {
        listViewPage.toggleAddAllColumns('check');
    },

    'Step 3: Verify All Columns': async(browser) => {
        const list = await taskManagementPage.getColumnsNameTableList();
        for (let i = 0; i < list.length; i++) {
            let startingColumnNum = i + 4;
            let j = startingColumnNum > 4 ? i + 1 : i;
            const tableColumns = `div[class = "ReactVirtualized__Table__headerRow"] div[aria-label]:nth-child(${j + 4})`;
            browser.expect.element(tableColumns).text.to.contain(list[i]).before(globalConfig.timeOut);
        }
    },

    'Step 4: Remove Columns': () => {
        listViewPage.toggleAddAllColumns('uncheck');
    }
};
