const xlsxFile = require('read-excel-file/node');
const expect = require('chai').expect;
const fs = require('fs');
const { excelDateFormat } = require('../../../main/utilities/customDate');
let proggioMainPage, taskManagementPage;
const globalConfig = require('../../../main/utilities/globals');
module.exports = {
    '@disabled': true,
    '@tags': ['taskManagementTests', 'listViewTests', 'fullRun'],
    before(browser) {
        console.log('Loading element pages...');
        proggioMainPage = browser.page.proggioMainPage();
        taskManagementPage = browser.page.taskManagementPage();
        browser.login(globalConfig.list_view_account, globalConfig.generic_password);
    },

    'Step 1: [Test - Export Project To Excel From Task Management - My Tasks] - Go To Task Management': () => {
        proggioMainPage.clickOnElement('@taskManagement');
    },

    'Step 2: Click On "My Tasks" View': () => {
        taskManagementPage.taskManagementViewPicker('list');
        taskManagementPage.taskManagementSubViewPicker('my tasks');
    },

    'Step 3: Export Project To Excel': (browser) => {
        taskManagementPage.exportProjectToExcel();
        browser.pause(5000);
    },

    'Step 4: [Verify - Export Project To Excel From Task Management - My Tasks]': () => {
        xlsxFile(`C:/Users/vladi/Downloads/Task View ${excelDateFormat}.xlsx`).then((rows) => {
            for (let i = 0; i < rows[0].length; i++) {
                expect(rows[1][0]).to.not.be.equal(null);
            }
        });
    },

    'Step 5: Delete Excel File From OS': () => {
        fs.unlinkSync(`C:/Users/vladi/Downloads/Task View ${excelDateFormat}.xlsx`);
    }
};
