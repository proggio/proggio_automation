const globalConfig = require('../../main/utilities/globals');
let userProfilePage;

module.exports = {
    '@tags': ['login', 'fullRun', 'generalTests'],
    before(browser) {
        console.log('Loading element pages...');
        userProfilePage = browser.page.userProfilePage();
        browser.login(globalConfig.proggio_username, globalConfig.proggio_password);
    },

    'Step 1: Verify Account Name': () => {
        userProfilePage.clickOnElement('@userProfileButton');
        userProfilePage.assert.containsTextInElement('@profileEmailName', globalConfig.proggio_username);
        userProfilePage.clickOnElement('@userProfileButton');
    }
};
