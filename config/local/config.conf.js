const Services = {};
loadServices();
const globalConfig = require('../../src/main/utilities/globals');

const nightwatchConfig = {
    // skip_testcases_on_fail: false,
    src_folders: [
        'src/tests'

        /** General **/
        // 'src/tests/generalTests/login.js',
        // 'src/tests/generalTests/sideMenu.js',

        /** Register **/
        // 'src/tests/registerTests/signUp.js',
        // 'src/tests/registerTests/resetPassword.js',

        /** All Projects **/
        // 'src/tests/allProjectsTests/moveBetweenViews.js',
        // 'src/tests/allProjectsTests/exportProjectToExcel.js',
        // 'src/tests/allProjectsTests/listViewTests/duplicateProject.js'
        // 'src/tests/allProjectsTests/listViewTests/assignProject.js',
        // 'src/tests/allProjectsTests/tilesViewTests/projectMenuModal.js'
        // 'src/tests/allProjectsTests/tilesViewTests/changeProjectName.js'
        // 'src/tests/allProjectsTests/createProjectFromTemplate.js'
        // 'src/tests/allProjectsTests/createNewProject.js',
        // 'src/tests/allProjectsTests/listViewTests/archiveProject.js',
        // 'src/tests/allProjectsTests/commentOnProject.js',
        // 'src/tests/allProjectsTests/createSubProject.js'
        // 'src/tests/allProjectsTests/listViewTests/addAllColumns.js'
        // 'src/tests/allProjectsTests/listViewTests/addDescription.js',
        // 'src/tests/allProjectsTests/listViewTests/changeProjectLifeCycle.js',
        // 'src/tests/allProjectsTests/listViewTests/openProject.js',
        // 'src/tests/allProjectsTests/listViewTests/projectMenuModal.js'

        /** Project Map **/
        // 'src/tests/projectMapTests/activityTests/activityContextMenu.js'
        // 'src/tests/projectMapTests/activityTests/activitySettingsModalTests/filesTabModal.js',
        // 'src/tests/projectMapTests/activityTests/activitySettingsModalTests/infoTabModal.js',
        // 'src/tests/projectMapTests/activityTests/activitySettingsModalTests/commentsTabModal.js',
        // 'src/tests/projectMapTests/activityTests/addTaskToActivity.js',
        // 'src/tests/projectMapTests/activityTests/changeActivityProgress.js',
        // 'src/tests/projectMapTests/taskTests/copyPasteTaskToActivity.js',
        // 'src/tests/projectMapTests/workStreamTests/copyPasteWorkStream.js',
        // 'src/tests/projectMapTests/projectTests/openProjectSettings.js',
        // 'src/tests/projectMapTests/projectTests/projectTopControls.js',
        // 'src/tests/projectMapTests/projectTests/undoRedoAction.js'
        // 'src/tests/projectMapTests/projectTests/projectSettingsModalTests/changeProjectDate.js',
        // 'src/tests/projectMapTests/projectTests/zoomInOutProject.js'
        // 'src/tests/projectMapTests/projectTests/zoomToProject.js'
        // 'src/tests/projectMapTests/projectTests/zoomToToday.js',
        // 'src/tests/projectMapTests/bufferTests/copyPasteBuffer.js',
        // 'src/tests/projectMapTests/taskTests/assignTask.js',
        // 'src/tests/projectMapTests/workStreamTests/assignWorkStream.js',
        // 'src/tests/projectMapTests/taskTests/commentOnTask.js',
        // 'src/tests/projectMapTests/workStreamTests/commentOnWorkStream.js'
        // 'src/tests/projectMapTests/bufferTests/consumeBuffer.js',
        // 'src/tests/projectMapTests/activityTests/createActivity.js',
        // 'src/tests/projectMapTests/bufferTests/createBuffer.js',
        // 'src/tests/projectMapTests/milestoneTests/createMilestone.js',
        // 'src/tests/projectMapTests/taskTests/createUnscheduledTask.js',
        // 'src/tests/projectMapTests/workStreamTests/createWorkStream.js',
        // 'src/tests/projectMapTests/activityTests/moveActivities.js',
        // 'src/tests/projectMapTests/activityTests/moveActivity.js',
        // 'src/tests/projectMapTests/activityTests/moveActivityWithMilestone.js'
        // 'src/tests/projectMapTests/bufferTests/moveBuffer.js'
        // 'src/tests/projectMapTests/milestoneTests/moveMilestone.js',
        // 'src/tests/projectMapTests/workStreamTests/moveWorkStream.js',
        // 'src/tests/projectMapTests/workStreamTests/resizeWorkStream.js'
        // 'src/tests/projectMapTests/taskTests/resizeTask.js'
        // 'src/tests/projectMapTests/exportProjectToExcel.js',

        /** Task Management **/
        // 'src/tests/taskManagementTests/listViewTests/addAllColumns.js',
        // 'src/tests/taskManagementTests/listViewTests/exportProjectToExcel.js',

        /** Settings **/
        // 'src/tests/settingsTests/addUser.js',
        // 'src/tests/settingsTests/accessRoleTests/viewerTests/verifyViewerGuest.js',
        // 'src/tests/settingsTests/accessRoleTests/viewerTests/verifyViewerTaskOwner.js',
        // 'src/tests/settingsTests/accessRoleTests/viewerTests/verifyViewerProjectManager.js',
        // 'src/tests/settingsTests/accessRoleTests/viewerTests/verifyViewerExecutiveManager.js',
        // 'src/tests/settingsTests/accessRoleTests/editorTests/verifyEditorGuest.js',
        // 'src/tests/settingsTests/accessRoleTests/editorTests/verifyEditorTaskOwner.js'
        // 'src/tests/settingsTests/accessRoleTests/editorTests/verifyEditorProjectManager.js',
        // 'src/tests/settingsTests/accessRoleTests/editorTests/verifyEditorExecutiveManager.js',
        // 'src/tests/settingsTests/accessRoleTests/adminTests/verifyAdminProjectManager.js',
        // 'src/tests/settingsTests/accessRoleTests/adminTests/verifyAdminExecutiveManager.js',

        /** Jira **/
        // 'src/tests/jiraTests/connectToJira.js',
        // 'src/tests/jiraTests/dragJiraTicketToProjectMap.js'
    ],

    page_objects_path: [
        /** Proggio */
        'src/main/pageObjects/proggioWeb',
        'src/main/pageObjects/proggioWeb/proggioLoginPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/userProfilePage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/allProjectsPage/projectMapPage/activityPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/allProjectsPage/projectMapPage/activityPage/activityModalPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/allProjectsPage/projectMapPage/workStreamPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/allProjectsPage/projectMapPage/milestonePage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/allProjectsPage/projectMapPage/bufferPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/allProjectsPage/projectMapPage/allProjectsSplitPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/allProjectsPage/tilesViewPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/listViewPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/allProjectsPage/kanbanViewPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/budgetPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/checkListModalPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/projectModalPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/chatModalPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/filesModalPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/dashboardPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/portfolioPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/resourcesManagementPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/settingsPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/settingsPage/jiraConnectPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/taskManagementPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/topControlsPage',

        // Register/ResetPassword
        'src/main/pageObjects/proggioWeb/proggioLoginPage/createAccountPage',
        'src/main/pageObjects/proggioWeb/proggioLoginPage/createAccountPage/signUpPage',
        'src/main/pageObjects/proggioWeb/proggioLoginPage/createAccountPage/signUpPage/signedUpPage',
        'src/main/pageObjects/proggioWeb/proggioLoginPage/resetPasswordPage',
        'src/main/pageObjects/proggioWeb/proggioLoginPage/resetPasswordPage/resetSubmittedPage',

        /** Gmail */
        'src/main/pageObjects/gmail/gmailLoginPage',
        'src/main/pageObjects/gmail/gmailMainPage'
    ],

    custom_commands_path: [
        'src/main/extensions/uiActions',
        'src/main/workFlows/webFlows'
    ],

    custom_assertions_path: 'src/main/extensions/verifications',

    globals_path: '../../src/main/utilities/globals',

    test_settings: {
        chrome: {
            disable_error_log: false,
            launch_url: 'https://app-staging.proggio.com',

            desiredCapabilities: {
                browserName: 'chrome',
                javascriptEnabled: true,
                acceptSslCerts: true,
                elementScrollBehavior: 1
            },

            webdriver: {
                start_process: true,
                port: 9515,
                server_path: (Services.chromedriver ? Services.chromedriver.path : '')
            },

            globals: globalConfig.globals

        },

        firefox: {
            disable_error_log: false,
            launch_url: 'https://nightwatchjs.org',

            desiredCapabilities: {
                browserName: 'firefox',
                alwaysMatch: {
                    // Enable this if you encounter unexpected SSL certificate errors in Firefox
                    // acceptInsecureCerts: true,
                    'moz:firefoxOptions': {
                        args: [
                            // '-headless',
                            // '-verbose'
                        ],
                        binary: `C:/Program Files/Mozilla Firefox/firefox.exe`
                    }
                }
            },

            webdriver: {
                start_process: true,
                port: 4444,
                server_path: require('geckodriver').path,
                cli_args: [
                    // very verbose geckodriver logs
                    // '-vv'
                ]
            },

            globals: globalConfig.globals
        }
    }
};

function loadServices() {
    try {
        Services.seleniumServer = require('selenium-server');
    } catch (err) {
    }

    try {
        Services.chromedriver = require('chromedriver');
    } catch (err) {
    }

    try {
        Services.geckodriver = require('geckodriver');
    } catch (err) {
    }
}

module.exports = nightwatchConfig;
