const Services = {};
loadServices();
const globalConfig = require('../../src/main/utilities/globals');

const nightwatchConfig = {
    src_folders: ['src/tests/projectMapTests/taskTests/createUnscheduledTask.js'],

    page_objects_path: [
        /** Proggio */
        'src/main/pageObjects/proggioWeb',
        'src/main/pageObjects/proggioWeb/proggioLoginPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/userProfilePage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/allProjectsPage/projectMapPage/activityPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/allProjectsPage/projectMapPage/activityPage/activityModalPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/allProjectsPage/projectMapPage/workStreamPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/allProjectsPage/projectMapPage/milestonePage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/allProjectsPage/projectMapPage/bufferPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/allProjectsPage/projectMapPage/allProjectsSplitPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/allProjectsPage/tilesViewPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/allProjectsPage/listViewPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/allProjectsPage/kanbanViewPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/budgetPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/checkListModalPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/projectModalPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/chatModalPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/filesModalPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/dashboardPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/portfolioPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/resourcesManagementPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/settingsPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/taskManagementPage',

        // Register/ResetPassword
        'src/main/pageObjects/proggioWeb/proggioLoginPage/createAccountPage',
        'src/main/pageObjects/proggioWeb/proggioLoginPage/createAccountPage/signUpPage',
        'src/main/pageObjects/proggioWeb/proggioLoginPage/createAccountPage/signUpPage/signedUpPage',
        'src/main/pageObjects/proggioWeb/proggioLoginPage/resetPasswordPage',
        'src/main/pageObjects/proggioWeb/proggioLoginPage/resetPasswordPage/resetSubmittedPage',

        /** Gmail */
        'src/main/pageObjects/gmail/gmailLoginPage',
        'src/main/pageObjects/gmail/gmailMainPage'
    ],

    custom_commands_path: [
        'src/main/extensions/uiActions',
        'src/main/workFlows/webFlows'
    ],

    custom_assertions_path: 'src/main/extensions/verifications',

    globals_path: '../../src/main/utilities/globals',

    test_settings: {
        chrome: {
            disable_error_log: false,
            launch_url: 'https://app-staging.proggio.com',

            desiredCapabilities: {
                browserName: 'chrome',
                javascriptEnabled: true,
                acceptSslCerts: true,
                elementScrollBehavior: 1
            },

            webdriver: {
                start_process: true,
                port: 9515,
                server_path: (Services.chromedriver ? Services.chromedriver.path : '')
            },

            globals: globalConfig.globals

        },

        firefox: {
            disable_error_log: false,
            launch_url: 'https://nightwatchjs.org',

            desiredCapabilities: {
                browserName: 'firefox',
                alwaysMatch: {
                    // Enable this if you encounter unexpected SSL certificate errors in Firefox
                    // acceptInsecureCerts: true,
                    'moz:firefoxOptions': {
                        args: [
                            // '-headless',
                            // '-verbose'
                        ],
                        binary: `C:/Program Files/Mozilla Firefox/firefox.exe`
                    }
                }
            },

            webdriver: {
                start_process: true,
                port: 4444,
                server_path: require('geckodriver').path,
                cli_args: [
                    // very verbose geckodriver logs
                    // '-vv'
                ]
            },

            globals: globalConfig.globals
        }
    }
};

function loadServices() {
    try {
        Services.seleniumServer = require('selenium-server');
    } catch (err) {
    }

    try {
        Services.chromedriver = require('chromedriver');
    } catch (err) {
    }

    try {
        Services.geckodriver = require('geckodriver');
    } catch (err) {
    }
}

module.exports = nightwatchConfig;
