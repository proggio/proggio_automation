const globalConfig = require('../../src/main/utilities/globals');

const nightwatchConfig = {
    src_folders: [ 'src/tests' ],

    page_objects_path: [
        /** Proggio */
        'src/main/pageObjects/proggioWeb',
        'src/main/pageObjects/proggioWeb/proggioLoginPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/userProfilePage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/allProjectsPage/projectMapPage/activityPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/allProjectsPage/projectMapPage/activityPage/activityModalPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/allProjectsPage/projectMapPage/workStreamPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/allProjectsPage/projectMapPage/milestonePage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/allProjectsPage/projectMapPage/bufferPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/allProjectsPage/projectMapPage/allProjectsSplitPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/allProjectsPage/tilesViewPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/listViewPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/allProjectsPage/kanbanViewPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/budgetPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/checkListModalPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/projectModalPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/chatModalPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/filesModalPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/dashboardPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/portfolioPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/resourcesManagementPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/settingsPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/settingsPage/jiraConnectPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/taskManagementPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/topControlsPage',

        // Register/ResetPassword
        'src/main/pageObjects/proggioWeb/proggioLoginPage/createAccountPage',
        'src/main/pageObjects/proggioWeb/proggioLoginPage/createAccountPage/signUpPage',
        'src/main/pageObjects/proggioWeb/proggioLoginPage/createAccountPage/signUpPage/signedUpPage',
        'src/main/pageObjects/proggioWeb/proggioLoginPage/resetPasswordPage',
        'src/main/pageObjects/proggioWeb/proggioLoginPage/resetPasswordPage/resetSubmittedPage',

        /** Gmail */
        'src/main/pageObjects/gmail/gmailLoginPage',
        'src/main/pageObjects/gmail/gmailMainPage'
    ],

    custom_commands_path: [
        'src/main/extensions/uiActions',
        'src/main/workFlows/webFlows'
    ],

    custom_assertions_path: 'src/main/extensions/verifications',

    globals_path: '../../src/main/utilities/globals',

    selenium: {
        'start_process': false,
        'host': 'hub-cloud.browserstack.com',
        'port': 443

    },

    test_settings: {
        chrome: {
            desiredCapabilities: {
                'build': 'Single Test',
                'browserstack.user': process.env.BROWSERSTACK_USERNAME,
                'browserstack.key': process.env.BROWSERSTACK_ACCESS_KEY,
                'browserstack.debug': true,
                'browserstack.console': 'verbose',
                // browserName: 'firefox',
                // browser_version: '85.0',
                browserName: 'chrome',
                browser_version: '86.0',
                // browserName: 'internet explorer',
                // browser_version: '11',
                // browserName: 'Safari',
                // browser_version: '5',
                // browserName: 'opera',
                // browser_version: '63.0',
                os: 'Windows',
                os_version: '10',
                resolution: '1920x1080'
            },

            globals: globalConfig.globals
        }
    }
};

module.exports = nightwatchConfig;
