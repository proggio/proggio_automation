const globalConfig = require('../../src/main/utilities/globals');

const nightwatchConfig = {
    src_folders: ['src/tests/generalTests/login.js'],

    page_objects_path: [
        /** Proggio */
        'src/main/pageObjects/proggioWeb',
        'src/main/pageObjects/proggioWeb/proggioLoginPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/userProfilePage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/allProjectsPage/projectMapPage/activityPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/allProjectsPage/projectMapPage/workStreamPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/allProjectsPage/projectMapPage/milestonePage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/allProjectsPage/projectMapPage/bufferPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/allProjectsPage/projectMapPage/allProjectsSplitPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/allProjectsPage/tilesViewPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/allProjectsPage/listViewPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/allProjectsPage/kanbanViewPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/budgetPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/dashboardPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/portfolioPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/resourcesManagementPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/settingsPage',
        'src/main/pageObjects/proggioWeb/proggioMainPage/taskManagementPage',

        // Register/ResetPassword
        'src/main/pageObjects/proggioWeb/proggioLoginPage/createAccountPage',
        'src/main/pageObjects/proggioWeb/proggioLoginPage/createAccountPage/signUpPage',
        'src/main/pageObjects/proggioWeb/proggioLoginPage/createAccountPage/signUpPage/signedUpPage',
        'src/main/pageObjects/proggioWeb/proggioLoginPage/resetPasswordPage',
        'src/main/pageObjects/proggioWeb/proggioLoginPage/resetPasswordPage/resetSubmittedPage',

        /** Gmail */
        'src/main/pageObjects/gmail/gmailLoginPage',
        'src/main/pageObjects/gmail/gmailMainPage'
    ],

    custom_commands_path: [
        'src/main/extensions/uiActions',
        'src/main/workFlows/webFlows'
    ],

    custom_assertions_path: 'src/main/extensions/verifications',

    globals_path: '../../src/main/utilities/globals',

    selenium: {
        'start_process': false,
        'host': 'hub-cloud.browserstack.com',
        'port': 443
    },

    common_capabilities: {
        'build': 'nightwatch-browserstack',
        'browserstack.user': process.env.BROWSERSTACK_USERNAME || globalConfig.browserstack_user,
        'browserstack.key': process.env.BROWSERSTACK_ACCESS_KEY || globalConfig.browserstack_access_key,
        'browserstack.debug': true
    },

    test_settings: {
        default: {},
        chrome: {
            desiredCapabilities: {
                browserName: 'chrome',
                browser_version: '87.0',
                os: 'Windows',
                os_version: '10',
                resolution: '1920x1080'
            }
        },
        firefox: {
            desiredCapabilities: {
                browserName: 'firefox',
                browser_version: '81.0',
                os: 'Windows',
                os_version: '8',
                resolution: '1920x1080'
            }
        },
        safari: {
            desiredCapabilities: {
                browserName: 'safari',
                browser_version: '7.1',
                os: 'OS X',
                os_version: 'MAVERICKS',
                resolution: '1920x1080'
            }
        },
        edge: {
            desiredCapabilities: {
                browser: 'microsoftedge',
                // browser_version: '7.1',
                os: 'Windows',
                os_version: '7',
                resolution: '1920x1080'
            },

            globals: globalConfig.globals
        }
    }
};
// Code to support common capabilites
for (let i in nightwatchConfig.test_settings) {
    let config = nightwatchConfig.test_settings[i];
    config['selenium_host'] = nightwatchConfig.selenium.host;
    config['selenium_port'] = nightwatchConfig.selenium.port;
    config['desiredCapabilities'] = config['desiredCapabilities'] || {};
    for (let j in nightwatchConfig.common_capabilities) {
        config['desiredCapabilities'][j] = config['desiredCapabilities'][j] || nightwatchConfig.common_capabilities[j];
    }
}

module.exports = nightwatchConfig;
