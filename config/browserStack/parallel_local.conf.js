const globalConfig = require('../../src/main/utilities/globals');

const nightwatchConfig = {
    src_folders: [ 'tests/signUp' ],

    selenium: {
        'start_process': false,
        'host': 'hub-cloud.browserstack.com',
        'port': 443
    },

    common_capabilities: {
        'build': 'nightwatch-browserstack',
        'browserstack.user': process.env.BROWSERSTACK_USERNAME || globalConfig.browserstack_user,
        'browserstack.key': process.env.BROWSERSTACK_ACCESS_KEY || globalConfig.browserstack_access_key,
        'browserstack.debug': true,
        'browserstack.local': true
    },

    test_settings: {
        default: {},
        chrome: {
            desiredCapabilities: {
                browser: 'chrome'
            }
        },
        firefox: {
            desiredCapabilities: {
                browser: 'firefox'
            }
        },
        safari: {
            desiredCapabilities: {
                browser: 'safari'
            }
        }
    }
};

module.exports = nightwatchConfig;
